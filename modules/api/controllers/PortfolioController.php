<?php

namespace app\modules\api\controllers;

use app\models\AutoRate;
use app\models\AutoRateLog;
use app\models\PortfolioRateAuto;
use app\models\Portfolio;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\rest\ActiveController;
use yii\web\Response;
use app\models\User;
use app\models\PortfolioRate;

/**
 * Default controller for the `api` module
 */
class PortfolioController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionCheckStart()
    {
        /** @var Portfolio[] $portfolios */
        $portfolios = Portfolio::find()->where(['status' => Portfolio::STATUS_MODERATED])->andWhere(['<=', 'datetime_start', date('Y-m-d H:i:s')])->all();

        foreach ($portfolios as $portfolio){
            $portfolio->status = Portfolio::STATUS_RATING;
            $portfolio->detachBehavior('company_id');
            $portfolio->save(false);

            $portfolio->company->sendEmailMessage("Начало торгов по портфелю «{$portfolio->name}»", "
                <p>Доброго времени суток, {$portfolio->company->director}!</p>
                <p>Торги по вашему портфелю №{$portfolio->id} «{$portfolio->name}» начались</p>
            ");
        }
    }

    public function actionAutoRate()
    {
        /** @var AutoRate[] $rateAutos */
        $rateAutos = AutoRate::find()->all();

        sleep(rand(5, 20));

        shuffle($rateAutos);

        $portfolioPks = [];

        foreach ($rateAutos as $rateAuto)
        {
            if(in_array($rateAuto->portfolio_id, $portfolioPks)){
                continue;
            }

            /** @var Portfolio $portfolio */
            $portfolio = Portfolio::findOne($rateAuto->portfolio_id);

            if($portfolio->status != Portfolio::STATUS_RATING){
                continue;
            }

            /** @var PortfolioRate $lastRate */
            $lastRate = PortfolioRate::find()->where(['portfolio_id' => $portfolio->id])->orderBy('id desc')->one();

            if($lastRate == null){
                $lastRate = new PortfolioRate(['amount' => $portfolio->start_rate]);
            }

            if($lastRate->company_id != $rateAuto->company_id){


                if($rateAuto->rate <= $lastRate->amount){
                    if($rateAuto->status != AutoRate::STATUS_STOP_BY_LIMIT){
                        $rateAuto->status = AutoRate::STATUS_STOP_BY_LIMIT;
                        $rateAuto->save(false);

                        $od = $portfolio->sum_od;
                        $gsc = $portfolio->general_sum_credit;
                        $st = $rateAuto->rate;
                        if((!empty($st))&&(!empty($od))&&(!empty($gsc))) {
                            $res = $st/$od*100;
                            $res = round($res, 2);
                            $st = round($st, 0);
                            $st1 = number_format(intval($st), 0, '', ' ');
                            $res = number_format(floatval($res), 2, ',', ' ');
                            $resOZ = $st/$gsc*100;
                            $resOZ = round($resOZ, 2);
//            $resOZ = number_format(floatval($resOZ), 2, ',', ' ');
                            //$st = number_format(floatval($st), 2, ',', ' ');
                        } else {
                            $res = '';
                            $resOZ = '';
                        }

                        $log = new AutoRateLog([
                            'portfolio_id' => $portfolio->id,
                            'action' => AutoRateLog::ACTION_LIMIT,
                            'company_id' => $rateAuto->company_id,
                            'user_id' =>$rateAuto->user_id,
                            'amount' => $rateAuto->rate,
                            'od' => $resOZ,
                            'osz' => $resOZ,
                            'datetime' => date('Y-m-d H:i:s'),
                        ]);

                        $log->save(false);

                    }
                    // VarDumper::dump($rateAuto, 10, true);
                    continue;
                }

                if((time() - strtotime($lastRate->created_at)) < 10){
                    continue;
                }

                $portfolioPks[] = $portfolio->id;
                $rate = new PortfolioRate(['portfolio_id' => $portfolio->id, 'amount' => ($lastRate->amount + $portfolio->min_step), 'is_auto' => 1, 'user_id' => $rateAuto->user_id, 'company_id' => $rateAuto->company_id, 'created_at' => date('Y-m-d H:i:s')]);


                $g = new Portfolio;
                $modelRateAuto = new PortfolioRateAuto;
                $getAutoRate = $modelRateAuto->getAutoRateUser($rateAuto->portfolio_id, $rateAuto->user_id);
                $auctPerc = $rate->getPercentAuction($rateAuto->portfolio_id);
                $per = $rate->getPercentSetRate($rateAuto->portfolio_id);
                $rHistory = $rate->getPercentSetRateHistory($rateAuto->portfolio_id);
                $rHistoryItemL = $rate->getPercentSetRateHistoryLast($rateAuto->portfolio_id);
                $auctPerc = $rate->getPercentAuction($rateAuto->portfolio_id);

                if(!empty($auctPerc)) {
                    foreach($auctPerc as $auctPercItem => $auctPercValue) {
                        if($auctPercValue == 1) {
                            $keyPerc = $auctPercItem;
                            $keyPerc = substr($auctPercItem, 3);
                            if($keyPerc !== 'legal_percent') {
                                $keyPerc = $keyPerc.'_percent';
                            }
                        }
                    }
                    if($portfolio->is_legal_fix == 1) {
                        $fixPerc = $portfolio->legal_fix;
                    }


                }

                if(!empty($keyPerc)) {
                    if(isset($portfolio->min_step_percent)) { $step = $portfolio->min_step_percent; } else { $step = 1; }
                    if(isset($portfolio->min_step_fix)) { $stepFix = $portfolio->min_step_fix; } else { $stepFix = 1; }
                    $ms = $step;
                    $ma = preg_replace("/\s+/", "", $rate->amount);
                    $ma = str_replace(',','.',$ma);
                    $ma = (float) $ma;
                    if($portfolio->soft_30_days_percent) {
                        $rate->count_soft_30 = $portfolio->soft_30_days_percent-$ma;
                    }
                    if($portfolio->soft_90_days_percent) {
                        $rate->count_soft_90 = $portfolio->soft_90_days_percent-$ma;
                    }
                    if($portfolio->soft_180_days_percent) {
                        $rate->count_soft_180 = $portfolio->soft_180_days_percent-$ma;
                    }
                    if($portfolio->soft_365_days_percent) {
                        $rate->count_soft_365 = $portfolio->soft_365_days_percent-$ma;
                    }
                    if($portfolio->soft_730_days_percent) {
                        $rate->count_soft_730 = $portfolio->soft_730_days_percent-$ma;
                    }
                    if($portfolio->legal_fix) {
                        $rate->count_legal_fix = $portfolio->legal_fix-$stepFix;
                    }
                    if($portfolio->legal_percent) {
                        $maxP = $portfolio->legal_percent;
                        $rate->count_legal = $portfolio->legal_percent-$ma;
                    } else {
                        $maxP = $portfolio->$keyPerc;
                    }
                    if($ma>$maxP) {


                        \Yii::warning('Внимание. Ставка не должна быть больше текущего значения!');

                        continue;

                    }
                    if($rate->count_fix) {
                        $max = $portfolio->legal_fix;
                        $mfix = $rate->count_fix;
                        if($mfix < 0) {

                            \Yii::warning('Внимание. Ставка не должна быть меньше 0!');

                            continue;
                        }
                        if($mfix >= $max) {

                            \Yii::warning('Внимание. Ставка не должна быть больше текущего значения!');

                            continue;
                        }
                    } else {
                        $mfix = 2;
                        $stepFix = 0;
                    }

                    if($rate->save()) {
                        if($ma<$ms || $mfix<$stepFix) {
//                            return [
//                                'forceReload' => '#crud-datatable-pjax',
//                                'title' => $titleSet,
//                                'content' => '<span class="text-error">Внимание. Ставка не должна быть меньше минимального шага!</span>',
//                                'footer' => Html::button('ОК', ['class' => 'btn btn-default btn-block', 'data-dismiss' => "modal"])
//
//                            ];

                            \Yii::warning('Внимание. Ставка не должна быть меньше минимального шага!');

                            continue;
                        }
                        $endDate = $rate->created_at;
                        $g->setPortfolioSetRate($portfolio->id, $endDate);
                        $amountA = $rate->amountA;
                        $amount_fixA = $rate->amount_fixA;
                        $limit_percent = $rate->limit_percent;
                        $limit_fix = $rate->limit_fix;
                        if(empty($limit_fix)) {
                            $limit_fix = 0;
                        }
                        if(empty($amount_fixA)) {
                            $amount_fixA = 0;
                        }
                        $companyId = $portfolio->company_id;
                        $userId = \Yii::$app->user->identity->id;
                        if($limit_percent || $limit_fix) {
                            $modelRateAuto->setAutoRate($portfolio->id, $userId, $companyId, $amountA, $amount_fixA, $limit_percent, $limit_fix);
                        }
                        $g->setPercentAuct($portfolio->id, $ma);
                        if($rate->count_fix !== $portfolio->legal_fix) {
                            $g->setPercentFix($portfolio->id, $mfix);
                        }

//                        return [
//                            'forceReload' => '#crud-datatable-pjax',
//                            'title' => $titleSet,
//                            'content' => '<span class="text-success">Внимание. Вы сделали ставку</span>',
//                            'footer' => Html::button('ОК', ['class' => 'btn btn-default btn-block', 'data-dismiss' => "modal"])
//                        ];

                        \Yii::warning('Внимание. Вы сделали ставку');

                    }
                } else {
                    $ms = $per['min_step']+$portfolio->currentRateAmount;
                    \Yii::warning('ms='.$per['min_step']."+".$portfolio->currentRateAmount.'='.$ms);
                    $ma = preg_replace("/\s+/", "", $rate->amount);
                    $ma = (float)$ma;
                    \Yii::warning($ma);
                    if($ma<$ms) {

//                        return [
//                            'forceReload' => '#crud-datatable-pjax',
//                            'title' => $titleSet,
//                            'content' => '<span class="text-error">Внимание. Ставка не должна быть меньше минимального шага!</span>',
//                            'footer' => Html::button('ОК', ['class' => 'btn btn-default btn-block', 'data-dismiss' => "modal"])
//
//                        ];

                        \Yii::warning('Внимание. Ставка не должна быть меньше минимального шага!');
                    } else {
                        if($rate->save()) {
                            $endDate = $rate->created_at;
                            $g->setPortfolioSetRate($portfolio->id, $endDate);


//                            return [
//                                'forceReload' => '#crud-datatable-pjax',
//                                'title' => $titleSet,
//                                'content' => '<span class="text-success">Внимание. Вы сделали ставку</span>',
//                                'footer' => Html::button('ОК', ['class' => 'btn btn-default btn-block', 'data-dismiss' => "modal"])
//                            ];

                            \Yii::warning('Внимание. Ставка не должна быть меньше минимального шага!');
                        } else {
                            \Yii::warning($rate->errors, 'Model Errors');
                        }
                    }
                }

            }
        }
    }

    public function actionSpotWinner()
    {
        /** @var Portfolio[] $portfolios */
        $portfolios = Portfolio::find()->where(['status' => Portfolio::STATUS_RATING])->andWhere(['<=', 'datetime_end', date('Y-m-d H:i:s')])->all();

//        var_dump($portfolio);
//        exit;

        foreach ($portfolios as $portfolio) {
            $rate = PortfolioRate::find()->where(['portfolio_id' => $portfolio->id])->orderBy('amount desc')->one();

            if($rate == null){
                $portfolio->status = Portfolio::STATUS_DONE_NO_DEAL;
                $portfolio->stage = Portfolio::STAGE_START;
//                $portfolio->company->sendEmailMessage("Торги по портфелю №{$portfolio->id} «{$portfolio->name}» завершены", "
//                        <p>Доброго времени суток, {$portfolio->company->director}!</p>
//                        <p>Торги по портфелю №{$portfolio->id} «{$portfolio->name}». Победитель не определен в связи с отсутствием ставок</p>
//                    ");

                $portfolio->save(false);

                continue;
            }

            $winner = User::findOne($rate->user_id);

            if($winner != null){
                $portfolio->winner_id = $winner->company_id;

                $od = $portfolio->sum_od;
                $gsc = $portfolio->general_sum_credit;
                $st = $portfolio->currentRateAmount;
                if((!empty($st))&&(!empty($od))&&(!empty($gsc))) {
                    $res = $st/$od*100;
                    $res = round($res, 2);
                    $st = round($st, 0);
                    $st1 = number_format(intval($st), 0, '', ' ');
                    $res = number_format(floatval($res), 2, ',', ' ');
                    $resOZ = $st/$gsc*100;
                    $resOZ = round($resOZ, 2);
                    $resOZ = number_format(floatval($resOZ), 2, ',', ' ');
                    //$st = number_format(floatval($st), 2, ',', ' ');
                } else {
                    $res = '';
                    $resOZ = '';
                }

                $portfolio->status = Portfolio::STATUS_DONE;
                $portfolio->stage = Portfolio::STAGE_START;
                $portfolio->company->sendEmailMessage("Торги по портфелю №{$portfolio->id} «{$portfolio->name}» завершены", "
                        <p>Доброго времени суток, {$portfolio->company->director}!</p>
                        <p>Торги по портфелю №{$portfolio->id} «{$portfolio->name}» завершены. Победил участник со ставкой ".number_format($portfolio->currentRateAmount, 0, 0, ' ')." ₽ ({$res}% ОД, {$resOZ}% ОСЗ). В ближайшее время менеджер аукциона свяжется с Вами для помощи в оформлении сделки.</p>
                    ");

                $portfolio->save(false);



                $winner->sendEmailMessage("Вы выиграли аукцион по портфелю №{$portfolio->id} «{$portfolio->name}»", "
                      Поздравляем!
Вы победили в торгах на портфель №{$portfolio->id} {$portfolio->name} со ставкой ".number_format($portfolio->currentRateAmount, 0, 0, ' ')." ₽ (".$res."% ОД, ".$resOZ."% ОСЗ). В ближайшее время менеджер аукциона свяжется с Вами для помощи в оформлении сделки.
                    ");
            }

            $participantsPks = ArrayHelper::getColumn(PortfolioRate::find()->where(['!=', 'id', $winner->id])->andWhere(['portfolio_id' => $portfolio->id])->all(), 'user_id');
            /** @var User[] $participants */
            $participants = User::findAll($participantsPks);
            foreach ($participants as $participant){
//                $participant->sendEmailMessage('Окончание торгов', "Портфель №{$portfolio->id} {$portfolio->name} - победил участник со ставкой ".number_format($portfolio->currentRateAmount, 0, 0, ' ')." ₽ (".$portfolio->sum_od."% ОД, ".$portfolio->sum_percents."% ОСЗ).
//Благодарим Вас за участие в торгах!");
            }
        }
    }
}
