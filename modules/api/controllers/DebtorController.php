<?php

namespace app\modules\api\controllers;

use app\models\Company;
use app\models\Debtor;
use app\models\DebtorStatus;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

/**
 * Debtor controller for the `api` module
 */
class DebtorController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if ($action->id == 'add') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Добавляет должника и дело
     * @return array
     */
    public function actionAdd()
    {
        $raw_data = file_get_contents('php://input') ?? null;
        $data = json_decode($raw_data, true);

//        Yii::info($data, 'test');

        $company = (new Company())->getByToken($data['token']);

        if (!$company) {
            return ['success' => 0, 'error' => 'Токен доступа не опознан'];
        }

        $result = Debtor::addApi($data);

        return $result;

    }

    /**
     * Статусы должника
     * @param string $token Токен компании
     * @return array
     */
    public function actionStatuses($token)
    {
        $company = (new Company())->getByToken($token);

        if (!$company) {
            return ['success' => 0, 'error' => 'Не верный токен доступа'];
        }

        return ['success' => 1, 'data' => (new DebtorStatus())->getList()];
    }

    /**
     * Запрос для теста методов АПИ
     * @param string $type Тип запроса (add, statuses)
     * @param string $token Апи ключ компании
     * @return mixed|string
     */
    public function actionTestRequest($type,$token)
    {
        $url = Url::to('@web/api/' . $this->id . '/' . $type, true);
        Yii::info('URL: ' . $url, 'test');

        if ($type == 'add') {
            //Добавляем должника
            $result = $this->postRequest($url, $token);
        } else {
            //Получаем статусы
            $result = $this->getRequest($url . '?token=' . $token);
        }

        Yii::info($result, 'test');

        return $result;
    }

    /**
     * Пост запрос для тестирование
     * @param $url
     * @param $token
     * @return mixed|string
     */
    private function postRequest($url, $token)
    {
        Yii::info($token, 'test');

        $data = [
            'token' => $token,
            'debtor' => [
                'outer_id' => '56465465fdsf',
                'age' => 35,
                'gender' => 1,
                'marital_status' => 0,
                'birthday_date' => '1977-02-25',
                'birthplace' => 'г. Кострома',
                'education' => 'Высшее',
                'email' => 'test@mail.ru',
                'number_minor_children' => 1,
                'owner_property' => '',
                'fact_residence_postcode' => '',
                'fact_residence_region' => '',
                'fact_residence_city' => '',
                'fact_residence_address' => '',
                'register_region' => '',
                'register_city' => '',
                'register_address' => '',
                'status' => 2,
                'workplace' => '',
                'position' => '',
                'employer_city' => 'Кострома',
                'employer_address' => '',
                'period_last_job' => 4,
            ],
            'loan' => [
                'date' => '2015-02-02',
                'type' => 2,
                'currency_id' => 1,
                'name_collateral' => '',
                'assessment_collateral' => '',
                'primary_amount_debt' => '',
                'percent_amount_debt' => 4.2,
                'other_amount_debt' => '',
                'total_amount_debt' => '',
                'period' => 24,
                'expired_days' => '',
                'amount_last_payment' => 255.45,
                'total_payment' => '',
                'last_payment_date' => '2017-04-15',
                'last_contact_date' => '2018-05-28',
                'total_number_payment' => 4,
            ]
        ];

        $result = '';

        $json = json_encode($data);

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'accept: application/json'));
            $result = curl_exec($curl);
            curl_close($curl);
        }

        return $result;
    }

    /**
     * GET запрос для тестирования
     * @param $url
     * @return mixed|string
     */
    private function getRequest($url)
    {

        $result = '';

        Yii::info('URl: ' . $url, 'test');

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, false);
            $result = curl_exec($curl);
            curl_close($curl);
        }

        return $result;
    }

}
