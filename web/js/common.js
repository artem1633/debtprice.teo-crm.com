$('#btn-dropdown_header').click(function(){
    if($(".dropdown-menu").is(':visible')){
        $(".dropdown-menu ").hide();
    } else {
        $(".dropdown-menu ").show();
    }
});

var menuOpened = false;

$('#btn-user-dropdown').click(function(){
    $('#dropdown-user-menu').toggle();
});


$("[data-click='top-menu-toggled']").click(function(){
    if(menuOpened == false){
        $('#sidebar').css('left', 'unset');
        menuOpened = true;
    } else {
        $('#sidebar').css('left', '-220px');
        menuOpened = false;
    }
});

var socket = io('https://debtprice.teo-crm.com:3002?userId='+$('meta[name="user_id"]').attr('content'));

socket.on('update-rates', function(data){
    $.pjax.reload('#crud-datatable-pjax');
    setInterval(function(){
        $.pjax.reload('#report-messages-pjax');
    }, 300);
});

$('#generate-key').click(function(){
    $.get(
        '/company/change-api-key',
        function(response){
            $('#api-key').html(response);
        }
    )

});