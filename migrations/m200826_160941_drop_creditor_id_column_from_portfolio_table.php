<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%portfolio}}`.
 */
class m200826_160941_drop_creditor_id_column_from_portfolio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-portfolio-creditor_id',
            'portfolio'
        );

        $this->dropIndex(
            'idx-portfolio-creditor_id',
            'portfolio'
        );

        $this->dropColumn('portfolio', 'creditor_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('portfolio', 'creditor_id', $this->integer()->comment('Кредитор'));

        $this->createIndex(
            'idx-portfolio-creditor_id',
            'portfolio',
            'creditor_id'
        );

        $this->addForeignKey(
            'fk-portfolio-creditor_id',
            'portfolio',
            'creditor_id',
            'company',
            'id',
            'SET NULL'
        );
    }
}
