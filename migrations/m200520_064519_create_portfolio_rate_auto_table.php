<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%portfolio_rate_auto}}`.
 */
class m200520_064519_create_portfolio_rate_auto_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%portfolio_rate_auto}}', [
            'id' => $this->primaryKey(),
			'active' => $this->integer()->comment('Активность автоставки'),
			'portfolio_id' => $this->integer()->comment('Портфель'),
            'amount' => $this->float()->comment('Ставка процент'),
			'amount_fix' => $this->float()->comment('Ставка фикс'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'company_id' => $this->integer()->comment('Компания'),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%portfolio_rate_auto}}');
    }
}
