<?php

use yii\db\Migration;

/**
 * Class m200107_080555_add_relations_to_loan_table
 */
class m200107_080555_add_relations_to_loan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-loan-type',
            'loan',
            'type',
            'product_type',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-loan-currency',
            'loan',
            'currency',
            'currency',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200107_080555_add_relations_to_loan_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200107_080555_add_relations_to_loan_table cannot be reverted.\n";

        return false;
    }
    */
}
