<?php

use yii\db\Migration;

/**
 * Class m200107_080336_add_relations_to_debtor_table
 */
class m200107_080336_add_relations_to_debtor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-debtor-status',
            'debtor',
            'status',
            'debtor_status',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200107_080336_add_relations_to_debtor_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200107_080336_add_relations_to_debtor_table cannot be reverted.\n";

        return false;
    }
    */
}
