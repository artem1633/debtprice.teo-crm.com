<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%portfolio_rate}}`.
 */
class m200716_130547_add_last_rate_message_column_to_portfolio_rate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('portfolio_rate', 'last_rate_message', $this->boolean()->defaultValue(false)->comment('Уведомление о перебитии'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('portfolio_rate', 'last_rate_message');
    }
}
