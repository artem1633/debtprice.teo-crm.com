<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%portfolio_rate}}`.
 */
class m191116_110906_create_portfolio_rate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%portfolio_rate}}', [
            'id' => $this->primaryKey(),
            'portfolio_id' => $this->integer()->comment('Портфель'),
            'amount' => $this->float()->comment('Ставка'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'company_id' => $this->integer()->comment('Компания'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-portfolio_rate-portfolio_id',
            'portfolio_rate',
            'portfolio_id'
        );

        $this->addForeignKey(
            'fk-portfolio_rate-portfolio_id',
            'portfolio_rate',
            'portfolio_id',
            'portfolio',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-portfolio_rate-user_id',
            'portfolio_rate',
            'user_id'
        );

        $this->addForeignKey(
            'fk-portfolio_rate-user_id',
            'portfolio_rate',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );


        $this->createIndex(
            'idx-portfolio_rate-company_id',
            'portfolio_rate',
            'company_id'
        );

        $this->addForeignKey(
            'fk-portfolio_rate-company_id',
            'portfolio_rate',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-portfolio_rate-company_id',
            'portfolio_rate'
        );

        $this->dropIndex(
            'idx-portfolio_rate-company_id',
            'portfolio_rate'
        );

        $this->dropForeignKey(
            'fk-portfolio_rate-user_id',
            'portfolio_rate'
        );

        $this->dropIndex(
            'idx-portfolio_rate-user_id',
            'portfolio_rate'
        );

        $this->dropForeignKey(
            'fk-portfolio_rate-portfolio_id',
            'portfolio_rate'
        );

        $this->dropIndex(
            'idx-portfolio_rate-portfolio_id',
            'portfolio_rate'
        );

        $this->dropTable('{{%portfolio_rate}}');
    }
}
