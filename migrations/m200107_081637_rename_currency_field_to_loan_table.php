<?php

use yii\db\Migration;

/**
 * Class m200107_081637_rename_currency_field_to_loan_table
 */
class m200107_081637_rename_currency_field_to_loan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('loan', 'currency', 'currency_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200107_081637_rename_currency_field_to_loan_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200107_081637_rename_currency_field_to_loan_table cannot be reverted.\n";

        return false;
    }
    */
}
