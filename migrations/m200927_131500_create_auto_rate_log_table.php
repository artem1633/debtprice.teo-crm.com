<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%auto_rate_log}}`.
 */
class m200927_131500_create_auto_rate_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%auto_rate_log}}', [
            'id' => $this->primaryKey(),
            'action' => $this->integer()->comment('Действие'),
            'portfolio_id' => $this->integer()->comment('Портфель'),
            'company_id' => $this->integer()->comment('Компания'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'amount' => $this->float()->comment('Сумма'),
            'od' => $this->float()->comment('ОД'),
            'osz' => $this->float()->comment('ОСЗ'),
            'datetime' => $this->dateTime()->comment('Дата и время'),
        ]);

        $this->createIndex(
            'idx-auto_rate_log-portfolio_id',
            'auto_rate_log',
            'portfolio_id'
        );

        $this->addForeignKey(
            'fk-auto_rate_log-portfolio_id',
            'auto_rate_log',
            'portfolio_id',
            'portfolio',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-auto_rate_log-company_id',
            'auto_rate_log',
            'company_id'
        );

        $this->addForeignKey(
            'fk-auto_rate_log-company_id',
            'auto_rate_log',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-auto_rate_log-user_id',
            'auto_rate_log',
            'user_id'
        );

        $this->addForeignKey(
            'fk-auto_rate_log-user_id',
            'auto_rate_log',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-auto_rate_log-user_id',
            'auto_rate_log'
        );

        $this->dropIndex(
            'idx-auto_rate_log-user_id',
            'auto_rate_log'
        );

        $this->dropForeignKey(
            'fk-auto_rate_log-company_id',
            'auto_rate_log'
        );

        $this->dropIndex(
            'idx-auto_rate_log-company_id',
            'auto_rate_log'
        );

        $this->dropForeignKey(
            'fk-auto_rate_log-portfolio_id',
            'auto_rate_log'
        );

        $this->dropIndex(
            'idx-auto_rate_log-portfolio_id',
            'auto_rate_log'
        );

        $this->dropTable('{{%auto_rate_log}}');
    }
}
