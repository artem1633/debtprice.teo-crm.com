<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%company}}`.
 */
class m191115_210557_add_new_columns_to_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company', 'inn', $this->string()->comment('ИНН'));
        $this->addColumn('company', 'ogrn', $this->string()->comment('ОГРН'));
        $this->addColumn('company', 'kpp', $this->string()->comment('КПП'));
        $this->addColumn('company', 'official_address', $this->string()->comment('Юридический адрес'));
        $this->addColumn('company', 'address_equals', $this->boolean()->defaultValue(false)->comment('Фактический адрес совпадает с юридическим'));
        $this->addColumn('company', 'director', $this->string()->comment('Генеральный директор'));
        $this->addColumn('company', 'email', $this->string()->comment('Email'));
        $this->addColumn('company', 'phone', $this->string()->comment('Телефон'));
        $this->addColumn('company', 'site', $this->string()->comment('Сайт'));
        $this->addColumn('company', 'bank_bik', $this->string()->comment('БИК/SWIFT'));
        $this->addColumn('company', 'bank_name', $this->string()->comment('Наименование банка'));
        $this->addColumn('company', 'bank_address', $this->string()->comment('Адрес банка'));
        $this->addColumn('company', 'bank_correspondent_account', $this->string()->comment('Корреспондентский счёт'));
        $this->addColumn('company', 'bank_register_number', $this->string()->comment('Регистрационный номер'));
        $this->addColumn('company', 'bank_registration_date', $this->date()->comment('Дата регистрации'));
        $this->addColumn('company', 'bank_payment_account', $this->string()->comment('Расчетный счет'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company', 'inn');
        $this->dropColumn('company', 'ogrn');
        $this->dropColumn('company', 'kpp');
        $this->dropColumn('company', 'official_address');
        $this->dropColumn('company', 'address_equals');
        $this->dropColumn('company', 'director');
        $this->dropColumn('company', 'email');
        $this->dropColumn('company', 'phone');
        $this->dropColumn('company', 'site');
        $this->dropColumn('company', 'bank_bik');
        $this->dropColumn('company', 'bank_name');
        $this->dropColumn('company', 'bank_address');
        $this->dropColumn('company', 'bank_correspondent_account');
        $this->dropColumn('company', 'bank_register_number');
        $this->dropColumn('company', 'bank_registration_date');
        $this->dropColumn('company', 'bank_payment_account');
    }
}
