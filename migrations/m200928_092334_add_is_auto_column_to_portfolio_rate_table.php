<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%portfolio_rate}}`.
 */
class m200928_092334_add_is_auto_column_to_portfolio_rate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('portfolio_rate', 'is_auto', $this->boolean()->defaultValue(false)->comment('Авто'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('portfolio_rate', 'is_auto');
    }
}
