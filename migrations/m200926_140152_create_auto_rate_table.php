<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%auto_rate}}`.
 */
class m200926_140152_create_auto_rate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%auto_rate}}', [
            'id' => $this->primaryKey(),
            'portfolio_id' => $this->integer()->comment('Портфель'),
            'company_id' => $this->integer()->comment('Компания'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'rate' => $this->float()->comment('Ставка'),
        ]);

        $this->createIndex(
            'idx-auto_rate-portfolio_id',
            'auto_rate',
            'portfolio_id'
        );

        $this->addForeignKey(
            'fk-auto_rate-portfolio_id',
            'auto_rate',
            'portfolio_id',
            'portfolio',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-auto_rate-user_id',
            'auto_rate',
            'user_id'
        );

        $this->addForeignKey(
            'fk-auto_rate-user_id',
            'auto_rate',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-auto_rate-company_id',
            'auto_rate',
            'company_id'
        );

        $this->addForeignKey(
            'fk-auto_rate-company_id',
            'auto_rate',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-auto_rate-company_id',
            'auto_rate'
        );

        $this->dropIndex(
            'idx-auto_rate-company_id',
            'auto_rate'
        );

        $this->dropForeignKey(
            'fk-auto_rate-portfolio_id',
            'auto_rate'
        );

        $this->dropIndex(
            'idx-auto_rate-portfolio_id',
            'auto_rate'
        );

        $this->dropForeignKey(
            'fk-auto_rate-user_id',
            'auto_rate'
        );

        $this->dropIndex(
            'idx-auto_rate-user_id',
            'auto_rate'
        );

        $this->dropTable('{{%auto_rate}}');
    }
}
