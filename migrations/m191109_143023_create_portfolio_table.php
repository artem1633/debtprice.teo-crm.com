<?php

use yii\db\Migration;

/**
 * Handles the creation of table `portfolio`.
 */
class m191109_143023_create_portfolio_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('portfolio', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'count_actions' => $this->integer()->comment('Кол-во дел'),
            'sum_od' => $this->float()->comment('Сумма ОД'),
            'sum_percents' => $this->float()->comment('Сумма процентов'),
            'general_sum_credit' => $this->float()->comment('Общая сумма задолжности'),
            'count_ip' => $this->integer()->comment('Кол-во ИП'),
            'count_il' => $this->integer()->comment('Кол-во ИЛ'),
            'is_pledge' => $this->boolean()->comment('Залог'),
            'general_payments' => $this->float()->comment('Общая сумма платежей за все время'),
            'count_action_before_90_days' => $this->float()->comment('Кол-во дел до 90 дней'),
            'count_action_90_180_days' => $this->float()->comment('Кол-во дел 90-180 дней'),
            'count_action_181_365_days' => $this->float()->comment('Кол-во дел 181-365 дней'),
            'count_action_366_730_days' => $this->float()->comment('Кол-во дел 366-730 дней'),
            'count_action_731_1095_days' => $this->float()->comment('Кол-во дел 731-1095 дней'),
            'count_action_over_1096_days' => $this->float()->comment('Кол-во дел 1096+ дней'),
            'product' => $this->string()->comment('Продукт'),
            'location' => $this->string()->comment('Местоположение'),
            'pledge_name' => $this->string()->comment('Наименование залога'),
            'count_placements_ka' => $this->integer()->comment('Кол-во размещений в КА'),
            'status' => $this->integer()->comment('Статус'),
            'datetime_start' => $this->dateTime()->comment('Дата и время начала'),
            'datetime_end' => $this->dateTime()->comment('Дата и время конца'),
            'winner_id' => $this->integer()->comment('Победитель'),
            'start_rate' => $this->float()->comment('Начальная ставка'),
            'company_id' => $this->integer()->comment('Компания'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-portfolio-winner_id',
            'portfolio',
            'winner_id'
        );

        $this->addForeignKey(
            'fk-portfolio-winner_id',
            'portfolio',
            'winner_id',
            'company',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-portfolio-company_id',
            'portfolio',
            'company_id'
        );

        $this->addForeignKey(
            'fk-portfolio-company_id',
            'portfolio',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-portfolio-company_id',
            'portfolio'
        );

        $this->dropIndex(
            'idx-portfolio-company_id',
            'portfolio'
        );

        $this->dropForeignKey(
            'fk-portfolio-winner_id',
            'portfolio'
        );

        $this->dropIndex(
            'idx-portfolio-winner_id',
            'portfolio'
        );

        $this->dropTable('portfolio');
    }
}
