<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%auto_rate}}`.
 */
class m200930_115812_add_message_column_to_auto_rate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('auto_rate', 'message', $this->boolean()->defaultValue(false)->comment('Уведомление'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('auto_rate', 'message');
    }
}
