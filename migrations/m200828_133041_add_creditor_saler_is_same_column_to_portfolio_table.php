<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%portfolio}}`.
 */
class m200828_133041_add_creditor_saler_is_same_column_to_portfolio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('portfolio', 'creditor_saler_is_same', $this->boolean()->defaultValue(false)->comment('Кредитор и Продавец – одно лицо'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('portfolio', 'creditor_saler_is_same');
    }
}
