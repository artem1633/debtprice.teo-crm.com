<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%debtor_status}}`.
 */
class m200107_074249_create_debtor_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%debtor_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'description' => $this->text()->comment('Описание')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%debtor_status}}');
    }
}
