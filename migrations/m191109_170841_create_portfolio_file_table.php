<?php

use yii\db\Migration;

/**
 * Handles the creation of table `portfolio_file`.
 */
class m191109_170841_create_portfolio_file_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('portfolio_file', [
            'id' => $this->primaryKey(),
            'portfolio_id' => $this->integer()->comment('Проект'),
            'name' => $this->string()->comment('Наименование'),
            'path' => $this->string()->comment('Путь'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-portfolio_file-portfolio_id',
            'portfolio_file',
            'portfolio_id'
        );

        $this->addForeignKey(
            'fk-portfolio_file-portfolio_id',
            'portfolio_file',
            'portfolio_id',
            'portfolio',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-portfolio_file-portfolio_id',
            'portfolio_file'
        );

        $this->dropIndex(
            'idx-portfolio_file-portfolio_id',
            'portfolio_file'
        );

        $this->dropTable('portfolio_file');
    }
}
