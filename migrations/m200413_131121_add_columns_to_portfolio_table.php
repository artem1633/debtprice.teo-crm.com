<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%portfolio}}`.
 */
class m200413_131121_add_columns_to_portfolio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('portfolio', 'soft_30_days', $this->string()->comment('soft 30 дней'));
		$this->addColumn('portfolio', 'soft_90_days', $this->string()->comment('soft 90 дней'));
		$this->addColumn('portfolio', 'soft_180_days', $this->string()->comment('soft 180 дней'));
		$this->addColumn('portfolio', 'soft_365_days', $this->string()->comment('soft 365 дней'));
		$this->addColumn('portfolio', 'soft_730_days', $this->string()->comment('soft 730 дней'));
		$this->addColumn('portfolio', 'soft_30_days_percent', $this->float()->comment('soft 30 дней процент'));
		$this->addColumn('portfolio', 'soft_90_days_percent', $this->float()->comment('soft 90 дней процент'));
		$this->addColumn('portfolio', 'soft_180_days_percent', $this->float()->comment('soft 180 дней процент'));
		$this->addColumn('portfolio', 'soft_365_days_percent', $this->float()->comment('soft 365 дней процент'));
		$this->addColumn('portfolio', 'soft_730_days_percent', $this->float()->comment('soft 730 дней процент'));
		$this->addColumn('portfolio', 'legal_percent', $this->float()->comment('legal процент'));
		$this->addColumn('portfolio', 'legal_fix', $this->float()->comment('legal фикс'));
		$this->addColumn('portfolio', 'is_soft_30_days', $this->integer()->comment('soft 30 дней активный'));
		$this->addColumn('portfolio', 'is_soft_90_days', $this->integer()->comment('soft 90 дней активный'));
		$this->addColumn('portfolio', 'is_soft_180_days', $this->integer()->comment('soft 180 дней активный'));
		$this->addColumn('portfolio', 'is_soft_365_days', $this->integer()->comment('soft 365 дней активный'));
		$this->addColumn('portfolio', 'is_soft_730_days', $this->integer()->comment('soft 730 дней активный'));
		$this->addColumn('portfolio', 'is_legal_percent', $this->integer()->comment('legal процент активный'));
		$this->addColumn('portfolio', 'is_legal_fix', $this->integer()->comment('legal фикс активный'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropColumn('portfolio', 'soft_30_days');
		$this->dropColumn('portfolio', 'soft_90_days');
		$this->dropColumn('portfolio', 'soft_180_days');
		$this->dropColumn('portfolio', 'soft_365_days');
		$this->dropColumn('portfolio', 'soft_730_days');
		$this->dropColumn('portfolio', 'soft_30_days_percent');
		$this->dropColumn('portfolio', 'soft_90_days_percent');
		$this->dropColumn('portfolio', 'soft_180_days_percent');
		$this->dropColumn('portfolio', 'soft_365_days_percent');
		$this->dropColumn('portfolio', 'soft_730_days_percent');
		$this->dropColumn('portfolio', 'legal_percent');
		$this->dropColumn('portfolio', 'legal_fix');
		$this->dropColumn('portfolio', 'is_soft_30_days');
		$this->dropColumn('portfolio', 'is_soft_90_days');
		$this->dropColumn('portfolio', 'is_soft_180_days');
		$this->dropColumn('portfolio', 'is_soft_365_days');
		$this->dropColumn('portfolio', 'is_soft_730_days');
		$this->dropColumn('portfolio', 'is_legal_percent');
		$this->dropColumn('portfolio', 'is_legal_fix');
    }
}
