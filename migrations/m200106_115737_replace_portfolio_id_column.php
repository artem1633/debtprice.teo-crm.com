<?php

use yii\db\Migration;

/**
 * Class m200106_115737_replace_portfolio_id_column
 */
class m200106_115737_replace_portfolio_id_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-debtor-portfolio_id', 'debtor');
        $this->dropColumn('debtor', 'portfolio_id');

        $this->addColumn('loan', 'portfolio_id', $this->integer()->comment('Портфель'));

        $this->addForeignKey(
            'fk-loan-portfolio_id',
            'loan',
            'portfolio_id',
            'portfolio',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200106_115737_replace_portfolio_id_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200106_115737_replace_portfolio_id_column cannot be reverted.\n";

        return false;
    }
    */
}
