<?php

use yii\db\Migration;

/**
 * Class m200615_150234_add_type_to_portfolio_table
 */
class m200615_150234_add_type_to_portfolio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('portfolio', 'type', $this->integer()->comment('Тип портфеля'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('portfolio', 'type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200615_150234_add_type_to_portfolio_table cannot be reverted.\n";

        return false;
    }
    */
}
