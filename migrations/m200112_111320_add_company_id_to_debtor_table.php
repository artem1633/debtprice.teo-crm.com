<?php

use yii\db\Migration;

/**
 * Class m200112_111320_add_company_id_to_debtor_table
 */
class m200112_111320_add_company_id_to_debtor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('debtor', 'company_id', $this->integer()->comment('Должник'));

        $this->addForeignKey(
            'fk-debtor-company_id',
            'debtor',
            'company_id',
            'company',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-debtor-company_id', 'debtor');
        $this->dropColumn('debtor', 'company_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200112_111320_add_company_id_to_debtor_table cannot be reverted.\n";

        return false;
    }
    */
}
