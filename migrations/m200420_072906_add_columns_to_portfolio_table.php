<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%portfolio}}`.
 */
class m200420_072906_add_columns_to_portfolio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('portfolio', 'min_step_percent', $this->float()->comment('Минимальный шаг процент'));
		$this->addColumn('portfolio', 'min_step_fix', $this->float()->comment('Минимальный шаг фикс'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropColumn('portfolio', 'min_step_percent');
		$this->dropColumn('portfolio', 'min_step_fix');
    }
}
