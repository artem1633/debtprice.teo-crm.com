<?php

use yii\db\Migration;

/**
 * Class m200414_074436_add_count_fix_to_portfolio_rate_table
 */
class m200414_074436_add_count_fix_to_portfolio_rate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('portfolio_rate', 'count_fix', $this->float()->comment('Фиксированное значение'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('portfolio_rate', 'count_fix');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200414_074436_add_count_fix_to_portfolio_rate_table cannot be reverted.\n";

        return false;
    }
    */
}
