<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%admin_files}}`.
 */
class m191122_130637_create_admin_files_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%admin_files}}', [
            'id' => $this->primaryKey(),
            'contract_creator' => $this->string()->comment('Договор создателя'),
            'contract_winner' => $this->string()->comment('Договор победителя'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%admin_files}}');
    }
}
