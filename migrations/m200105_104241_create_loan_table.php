<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%credit}}`.
 */
class m200105_104241_create_loan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%loan}}', [
            'id' => $this->primaryKey(),
            'debtor_id' => $this->integer()->comment('Должник'),
            'date' => $this->date()->comment('Дата пердоставления кредита'),
            'type' => $this->integer()->comment('Тип кредитного продукта'),
            'currency' => $this->integer()->comment('Валюта'),
            'name_collateral'=>$this->string()->comment('Наименование залога'),
            'assessment_collateral'=>$this->string()->comment('Оценка залога'),
            'primary_amount_debt' => $this->double(2)->defaultValue(0)->comment('Сумма задолженности по основному долгу (в валюте долга)'),
            'percent_amount_debt' => $this->double(2)->defaultValue(0)->comment('Сумма задолженности по процентам (в валюте долга)'),
            'other_amount_debt' => $this->double(2)->defaultValue(0)->comment('Сумма задолженности по иным платам и штрафам (в валюте долга)'),
            'total_amount_debt' => $this->double(2)->defaultValue(0)->comment('Общая сумма задолженности (в валюте долга)'),
            'period' => $this->integer()->comment('Срок кредита'),
            'expired_days' => $this->integer()->comment('Кол-во дней просрочки'),
            'amount_last_payment' => $this->double(2)->defaultValue(0)->comment('Сумма последнего платежа'),
            'total_payment' => $this->double(2)->defaultValue(0)->comment('Общая сумма поступившая в счет оплаты по кредиту'),
            'last_payment_date' => $this->date()->comment('Дата последнего платежа'),
            'last_contact_date' => $this->date()->comment('Дата последнего контакта с должником'),
            'total_number_payment' => $this->integer()->comment('Число произведенных платежей')
            //'creditor_id' => TODO: определиться с привязкой к кредитору
        ]);

        $this->addForeignKey(
            'fk-loan-debtor_id',
            'loan',
            'debtor_id',
            'debtor',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%loan}}');
    }
}
