<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%portfolio}}`.
 */
class m191121_132608_add_stages_columns_to_portfolio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('portfolio', 'contract_creator', $this->string()->comment('Договор создателя'));
        $this->addColumn('portfolio', 'contract_winner', $this->string()->comment('Договор победителя'));
        $this->addColumn('portfolio', 'bill', $this->string()->comment('Счет'));
        $this->addColumn('portfolio', 'bill_payed', $this->boolean()->defaultValue(false)->comment('Счет оплачен'));
        $this->addColumn('portfolio', 'commission_payed', $this->boolean()->defaultValue(false)->comment('Комиссия оплачена'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('portfolio', 'contract_creator');
        $this->dropColumn('portfolio', 'contract_winner');
        $this->dropColumn('portfolio', 'bill');
        $this->dropColumn('portfolio', 'bill_payed');
        $this->dropColumn('portfolio', 'commission_payed');
    }
}
