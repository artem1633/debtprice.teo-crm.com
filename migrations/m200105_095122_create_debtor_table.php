<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%debtor}}`.
 */
class m200105_095122_create_debtor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%debtor}}', [
            'id' => $this->primaryKey(),
            'portfolio_id' => $this->integer()->comment('Портфель'),
            'outer_id' => $this->string()->comment('Идентификатор из ПО кредитора'),
            'birthday_date' => $this->date()->defaultValue(null)->comment('Дата рождения'),
            'age' => $this->integer()->comment('Возраст'),
            'gender' => $this->smallInteger(1)->comment('Пол'),
            'marital_status' => $this->smallInteger(1)->comment('Семейное положение'),
            'birthplace' => $this->text()->comment('Место рождения'),
            'education' => $this->string()->comment('Образование'),
            'email' => $this->string(),
            'number_minor_children' => $this->integer()->comment('Кол-во несовершеннолетних детей'),
            'owner_property' => $this->text()->comment('Наличие в собственности имущества'),
            'fact_residence_postcode' => $this->string()->comment('Индекс фактического проживания'),
            'fact_residence_region' => $this->string()->comment('Регион фактического проживания'),
            'fact_residence_city' => $this->string()->comment('Город фактического проживания'),
            'fact_residence_address' => $this->string()->comment('Адрес фактического проживания'),
            'register_region' => $this->string()->comment('Регион постоянной регистрации'),
            'register_city' => $this->string()->comment('Город постоянной регистрации'),
            'register_address' => $this->string()->comment('Адрес постоянной регистрации'),
            'status' => $this->integer()->comment('Статус'),
            'workplace' => $this->text()->comment('Место работы'),
            'position' => $this->text()->comment('Должность'),
            'employer_city' => $this->string()->comment('Город работодателя'),
            'employer_address' => $this->string()->comment('Адрес работодателя'),
            'period_last_job' => $this->integer()->comment('Срок работы (лет) на последнем рабочем месте')
        ]);

        $this->addForeignKey(
            'fk-debtor-portfolio_id',
            'debtor',
            'portfolio_id',
            'portfolio',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%debtor}}');
    }
}
