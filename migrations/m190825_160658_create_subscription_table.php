<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subscription`.
 */
class m190825_160658_create_subscription_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('subscription', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'days' => $this->integer()->comment('Срок в днях'),
            'price' => $this->float()->comment('Стоимость'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('subscription');
    }
}
