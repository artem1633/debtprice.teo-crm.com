<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%auto_rate}}`.
 */
class m200929_200744_add_status_column_to_auto_rate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('auto_rate', 'status', $this->integer()->defaultValue(0)->comment('Статус'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('auto_rate', 'status');
    }
}
