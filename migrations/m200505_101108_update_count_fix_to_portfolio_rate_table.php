<?php

use yii\db\Migration;

/**
 * Class m200505_101108_update_count_fix_to_portfolio_rate_table
 */
class m200505_101108_update_count_fix_to_portfolio_rate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->alterColumn('portfolio_rate', 'count_fix', $this->float()->comment('Фиксированное значение'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200505_101108_update_count_fix_to_portfolio_rate_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200505_101108_update_count_fix_to_portfolio_rate_table cannot be reverted.\n";

        return false;
    }
    */
}
