<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%portfolio_rate}}`.
 */
class m200520_113513_add_columns_to_portfolio_rate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('portfolio_rate', 'count_soft_30', $this->float()->comment('Ставка софт 1'));
		$this->addColumn('portfolio_rate', 'count_soft_90', $this->float()->comment('Ставка софт 2'));
		$this->addColumn('portfolio_rate', 'count_soft_180', $this->float()->comment('Ставка софт 3'));
		$this->addColumn('portfolio_rate', 'count_soft_365', $this->float()->comment('Ставка софт 4'));
		$this->addColumn('portfolio_rate', 'count_soft_730', $this->float()->comment('Ставка софт 5'));
		$this->addColumn('portfolio_rate', 'count_legal', $this->float()->comment('Ставка legal %'));
		$this->addColumn('portfolio_rate', 'count_legal_fix', $this->float()->comment('Ставка legal fix'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropColumn('portfolio_rate', 'count_soft_30');
		$this->dropColumn('portfolio_rate', 'count_soft_90');
		$this->dropColumn('portfolio_rate', 'count_soft_180');
		$this->dropColumn('portfolio_rate', 'count_soft_365');
		$this->dropColumn('portfolio_rate', 'count_soft_730');
		$this->dropColumn('portfolio_rate', 'count_legal');
		$this->dropColumn('portfolio_rate', 'count_legal_fix');
    }
}
