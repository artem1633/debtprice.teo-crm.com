<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%portfolio_rate_auto}}`.
 */
class m200520_065708_add_columns_to_portfolio_rate_auto_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('portfolio_rate_auto', 'limit_fix', $this->float()->comment('Фиксированное значение лимит'));
		$this->addColumn('portfolio_rate_auto', 'limit_percent', $this->float()->comment('Процент значение лимит'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropColumn('portfolio_rate_auto', 'limit_fix');
		$this->dropColumn('portfolio_rate_auto', 'limit_percent');
    }
}
