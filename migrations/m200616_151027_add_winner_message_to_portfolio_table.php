<?php

use yii\db\Migration;

/**
 * Class m200616_151027_add_winner_message_to_portfolio_table
 */
class m200616_151027_add_winner_message_to_portfolio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('portfolio', 'winner_message', $this->integer()->comment('Прочитано или нет'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('portfolio', 'winner_message');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200616_151027_add_winner_message_to_portfolio_table cannot be reverted.\n";

        return false;
    }
    */
}
