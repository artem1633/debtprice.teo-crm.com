<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%portfolio}}`.
 */
class m200819_110647_add_creditor_id_column_to_portfolio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('portfolio', 'creditor_id', $this->integer()->comment('Кредитор'));

        $this->createIndex(
            'idx-portfolio-creditor_id',
            'portfolio',
            'creditor_id'
        );

        $this->addForeignKey(
            'fk-portfolio-creditor_id',
            'portfolio',
            'creditor_id',
            'company',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-portfolio-creditor_id',
            'portfolio'
        );

        $this->dropIndex(
            'idx-portfolio-creditor_id',
            'portfolio'
        );

        $this->dropColumn('portfolio', 'creditor_id');
    }
}
