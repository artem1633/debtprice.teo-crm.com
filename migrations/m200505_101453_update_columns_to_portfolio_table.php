<?php

use yii\db\Migration;

/**
 * Class m200505_101453_update_columns_to_portfolio_table
 */
class m200505_101453_update_columns_to_portfolio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->alterColumn('portfolio', 'soft_30_days', $this->string()->comment('soft 30 дней'));
		$this->alterColumn('portfolio', 'soft_90_days', $this->string()->comment('soft 90 дней'));
		$this->alterColumn('portfolio', 'soft_180_days', $this->string()->comment('soft 180 дней'));
		$this->alterColumn('portfolio', 'soft_365_days', $this->string()->comment('soft 365 дней'));
		$this->alterColumn('portfolio', 'soft_730_days', $this->string()->comment('soft 730 дней'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200505_101453_update_columns_to_portfolio_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200505_101453_update_columns_to_portfolio_table cannot be reverted.\n";

        return false;
    }
    */
}
