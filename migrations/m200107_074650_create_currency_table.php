<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%currency}}`.
 */
class m200107_074650_create_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%currency}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'icon' => $this->string()->comment('Иконка'),
            'exchange_rate' => $this->double(2)->comment('Курс (к рублю)')
        ]);

        $this->batchInsert('currency', ['name', 'icon', 'exchange_rate'],[
            ['рубль', 'fa-ruble', 1],
            ['доллар', 'fa-dollar', 61.91],
            ['евро', 'fa-euro', 69.47],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%currency}}');
    }
}
