<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $address Фактический адрес
 * @property string $post_index Почтовый индекс
 * @property int $subscription_id Подписка
 * @property int $is_super_company Является ли супер компанией
 * @property string $subscription_end_datetime Дата и время окончания подписки
 * @property int $access Доступ (вкл/выкл)
 * @property string $last_activity_datetime Дата и время последней активности
 * @property double $balance Баланс
 * @property string $created_at
 * @property string $code Код
 * @property string $inn ИНН
 * @property string $ogrn ОГРН
 * @property string $kpp КПП
 * @property string $official_address Юридический адрес
 * @property int $address_equals Фактический адрес совпадает с юридическим
 * @property string $director Генеральный директор
 * @property string $email Email
 * @property string $phone Телефон
 * @property string $site Сайт
 * @property string $bank_bik БИК/SWIFT
 * @property string $bank_name Наименование банка
 * @property string $bank_address Адрес банка
 * @property string $bank_correspondent_account Корреспондентский счёт
 * @property string $bank_register_number Регистрационный номер
 * @property string $bank_registration_date Дата регистрации
 * @property string $bank_payment_account Расчетный счет
 * @property string api_key АПИ ключ
 *
 * @property CompanyFile[] $files
 * @property Subscription $subscription
 * @property Portfolio[] $portfolios
 * @property Portfolio[] $portfolios0
 * @property Position[] $positions
 * @property User[] $users
 * @property Debtor[] $debtors
 */
class Company extends ActiveRecord
{
    const TYPE_CREDITOR = 0;
    const TYPE_CLAIMANT = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscription_id', 'is_super_company', 'access', 'address_equals', 'type', 'moderated'], 'integer'],
            [
                [
                    'subscription_end_datetime',
                    'last_activity_datetime',
                    'created_at',
                    'bank_registration_date',
                    'bank_payment_account'
                ],
                'safe'
            ],
            [['balance'], 'number'],
            [
                [
                    'name',
                    'address',
                    'post_index',
                    'code',
                    'inn',
                    'ogrn',
                    'kpp',
                    'official_address',
                    'director',
                    'email',
                    'phone',
					'promo',
                    'site',
                    'bank_bik',
                    'bank_name',
                    'bank_address',
                    'bank_correspondent_account',
                    'bank_register_number'
                ],
                'string',
                'max' => 255
            ],
            [
                ['subscription_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Subscription::className(),
                'targetAttribute' => ['subscription_id' => 'id']
            ],
            ['api_key', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'address' => 'Фактический адрес',
            'post_index' => 'Почтовый индекс',
            'subscription_id' => 'Подписка',
            'is_super_company' => 'Является ли супер компанией',
            'subscription_end_datetime' => 'Дата и время окончания подписки',
            'access' => 'Доступ (вкл/выкл)',
            'last_activity_datetime' => 'Дата и время последней активности',
            'balance' => 'Баланс',
            'created_at' => 'Дата и время создания',
            'code' => 'Код',
            'inn' => 'ИНН',
            'ogrn' => 'ОГРН',
            'kpp' => 'КПП',
            'official_address' => 'Юридический адрес',
            'address_equals' => 'Фактический адрес совпадает с юридическим',
            'director' => 'Генеральный директор',
            'email' => 'Email',
            'phone' => 'Телефон',
			'promo' => 'Промо-код',
            'site' => 'Сайт',
            'type' => 'Тип',
            'moderated' => 'Прошла модерацию',
            'bank_bik' => 'БИК/SWIFT',
            'bank_name' => 'Наименование банка',
            'bank_address' => 'Адрес банка',
            'bank_correspondent_account' => 'Корреспондентский счёт',
            'bank_register_number' => 'Регистрационный номер',
            'bank_registration_date' => 'Дата регистрации',
            'bank_payment_account' => 'Расчетный счет',
            'api_key' => 'АПИ ключ',
        ];
    }

    /**
     * @return array
     */
    public static function typeLabels()
    {
        return [
            self::TYPE_CREDITOR => 'Кредитор',
            self::TYPE_CLAIMANT => 'Взыскатель',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->generateCodeRecursive();
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            foreach (CompanyFile::typeList() as $type) {
                (new CompanyFile(['company_id' => $this->id, 'type' => $type]))->save(false);
            }
        }
    }

    /**
     * Отправляет письмо
     * @param string $subject
     * @param string $htmlBody
     * @param array $attachments
     * @return bool
     */
    public function sendEmailMessage($subject, $htmlBody, $attachments = [])
    {
        if ($this->email == null) {
            return false;
        }

        try {
            $mail = Yii::$app->mailer->compose()
                ->setFrom('auction@debtprice.market')
                ->setTo($this->email)
                ->setSubject($subject)
                ->setHtmlBody($htmlBody);

            if (count($attachments) > 0) {
                foreach ($attachments as $attachmentPath => $attachmentName) {
                    $mail->attach($attachmentPath, ['fileName' => $attachmentName]);
                }
            }

            $mail->send();
        } catch (\Exception $e) {
            Yii::warning($e);
        }
    }

    /**
     * @param integer $i
     * @throws \yii\base\Exception
     */
    private function generateCodeRecursive($i = 1)
    {
        if ($i > 20) {
            return;
        }

        $code = Yii::$app->security->generateRandomString(5);
        $company = self::find()->where(['code' => $code])->one();
        if ($company == null) {
            $this->code = $code;
            return;
        } else {
            $i++;
            $this->generateCodeRecursive($i);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(CompanyFile::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBots()
    {
        return $this->hasMany(Bot::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['id' => 'subscription_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customer::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerStatuses()
    {
        return $this->hasMany(CustomerStatus::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(Position::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDebtors()
    {
        return $this->hasMany(Debtor::className(), ['company_id' => 'id']);
    }

    /**
     * Получает компанию по токену
     * @param $token
     * @return null|static
     */
    public function getByToken($token)
    {
        return self::findOne(['api_key' => $token]) ?? null;
    }

    /**
     * Генерация нового токена
     * @return string
     * @throws \yii\base\Exception
     */
    public function changeApiKey()
    {
        $this->api_key = Yii::$app->security->generateRandomString();

        $this->save(false);

        return $this->api_key;
    }
	
	
	
	
}
