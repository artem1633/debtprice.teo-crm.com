<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "company_file".
 *
 * @property int $id
 * @property int $company_id Компания
 * @property int $type Тип
 * @property string $name Наименование
 * @property string $path Путь
 * @property string $created_at
 *
 * @property Company $company
 */
class CompanyFile extends \yii\db\ActiveRecord
{
    const MAIN_DOCUMENT = 4;
    const OGRN_DOCUMENT = 1;
    const INN_DOCUMENT = 2;
    const CONFIRM_RULES_DIRECTOR_DOCUMENT = 3;
    const DIRECTOR_PASSPORT = 5;
    const ADD_TO_BASE_DOCUMENT = 6;
   
	public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_file';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'type'], 'integer'],
            [['created_at'], 'safe'],
            [['name', 'path'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['file'], 'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'type' => 'Тип',
            'name' => 'Наименование',
            'path' => 'Путь',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return array
     */
    public static function typeList()
    {
        return [
			self::MAIN_DOCUMENT, 
			self::OGRN_DOCUMENT, 
			self::INN_DOCUMENT, 
			self::CONFIRM_RULES_DIRECTOR_DOCUMENT, 
			self::DIRECTOR_PASSPORT, 
			self::ADD_TO_BASE_DOCUMENT, 
				
        ];
    }

    /**
     * @return array
     */
    public static function typeLabels()
    {
        return [
            self::MAIN_DOCUMENT => 'Скан устава компании',
            self::OGRN_DOCUMENT => 'Скан ОГРН',
            self::INN_DOCUMENT => 'Скан ИНН',
            self::CONFIRM_RULES_DIRECTOR_DOCUMENT => 'Документ, подтверждающий полномочия руководителя',
            self::DIRECTOR_PASSPORT => 'Скан паспорта руководителя',
            self::ADD_TO_BASE_DOCUMENT => 'Свидетельство о внесении в реестр',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
