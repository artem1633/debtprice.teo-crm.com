<?php

namespace app\models;

use app\validators\NumericValidator;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "portfolio".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $count_actions Кол-во дел
 * @property double $sum_od Сумма ОД
 * @property double $sum_percents Сумма процентов
 * @property double $general_sum_credit Общая сумма задолжности
 * @property int $creditor Кредитор
 * @property int $count_ip Кол-во ИП
 * @property int $count_il Кол-во ИЛ
 * @property int $is_pledge Залог
 * @property double $general_payments Общая сумма платежей за все время
 * @property double $count_action_before_90_days Кол-во дел до 90 дней
 * @property double $count_action_90_180_days Кол-во дел 90-180 дней
 * @property double $count_action_181_365_days Кол-во дел 181-365 дней
 * @property double $count_action_366_730_days Кол-во дел 366-730 дней
 * @property double $count_action_731_1095_days Кол-во дел 731-1095 дней
 * @property double $count_action_over_1096_days Кол-во дел 1096+ дней
 * @property string $product Продукт
 * @property string $location Местоположение
 * @property string $pledge_name Наименование залога
 * @property int $count_placements_ka Кол-во размещений в КА
 * @property int $status Статус
 * @property string $datetime_start Дата и время начала
 * @property string $datetime_end Дата и время конца
 * @property int $winner_id Победитель
 * @property double $start_rate Начальная ставка
 * @property int $stage Этап
 * @property string $contract_creator Договор создателя
 * @property string $contract_winner Договор победителя
 * @property string $bill Счет
 * @property int $bill_confirmed Подтверждена оплата счета
 * @property int $bill_payed Счет оплачен
 * @property int $commission_payed Комиссия оплачена
 * @property int $company_id Компания
 * @property string $disapprove_text Текст отказа в публикации портфеля
 * @property int $hide_saler Скрыть продавца
 * @property int $creditor_saler_is_same
 * @property string $part_message Уведомления пользователям
 * @property float $min_step Шаг ставки
 *
 * @property double $currentRateAmount
 * @property User $users
 * @property boolean $isStarted
 * @property boolean $isEnded
 *
 * @property Company $winner
 * @property Loan[] $loans
 */
class Portfolio extends ActiveRecord
{
    const STATUS_MODERATION = 0;
    const STATUS_MODERATED = 1;
    const STATUS_RATING = 2;
    const STATUS_REJECTED = 3;
    const STATUS_DONE = 4;
    const STATUS_DRAFT = 5;
    const STATUS_DONE_NO_DEAL = 6;

    const STAGE_START = 1;
    const STAGE_CONTRACTS_LOADED = 2;
    const STAGE_BILL_PAYED = 3;
    const STAGE_BILL_CONFIRMED = 4;
    const STAGE_COMMISSION_PAYED = 5;
    const STAGE_ARCHIVE = 6;
    const STAGE_DONE = 7;
    const STAGE_NONE_BIDS = 8;

    public $files;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'portfolio';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        if (Yii::$app->user->isGuest == false) {
            return [
                'created_at' => [
                    'class' => TimestampBehavior::class,
                    'updatedAtAttribute' => null,
                    'createdAtAttribute' => 'created_at',
                    'value' => date('Y-m-d H:i:s'),
                ],
                'company_id' => [
                    'class' => BlameableBehavior::class,
                    'updatedByAttribute' => null,
                    'createdByAttribute' => 'company_id',
                    'value' => Yii::$app->user->identity->company_id,
                ],
            ];
        }

        return [];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'count_ip',
                    'count_il',
                    'is_pledge',
                    'count_placements_ka',
                    'status',
                    'winner_id',
                    'company_id',
                    'stage',
                    'bill_payed',
                    'commission_payed',
                    'bill_confirmed',
                    'is_soft_30_days',
                    'is_soft_90_days',
                    'is_soft_180_days',
                    'is_soft_365_days',
                    'is_soft_730_days',
                    'is_legal_percent',
                    'is_legal_fix',
                    'count_action_before_90_days',
                    'count_action_90_180_days',
                    'count_action_181_365_days',
                    'count_action_366_730_days',
                    'count_action_731_1095_days',
                    'count_action_over_1096_days',
                    'type',
                    'winner_message',
                    'hide_saler',
                    'creditor_saler_is_same'
                ],
                'integer'
            ],
            [
                [
                    'legal_percent',
                    'legal_fix',
                    'min_step_percent',
                    'min_step_fix',
                ],
                'number'
            ],
            [['name'], 'required'],
            [['soft_30_days'], 'string'],
            [['soft_90_days'], 'string'],
            [['soft_180_days'], 'string'],
            [['soft_365_days'], 'string'],
            [['soft_730_days'], 'string'],
            [['disapprove_text'], 'string'],
            [['soft_30_days_percent'], 'number', 'min' => 0],
            [['soft_90_days_percent'], 'number', 'min' => 0],
            [['soft_180_days_percent'], 'number', 'min' => 0],
            [['soft_365_days_percent'], 'number', 'min' => 0],
            [['soft_730_days_percent'], 'number', 'min' => 0],
            [['legal_percent'], 'number', 'min' => 0],
            [['legal_fix'], 'number', 'min' => 0],

            [['datetime_start', 'datetime_end', 'created_at', 'files', 'start_rate', 'count_actions', 'sum_od', 'sum_percents', 'general_sum_credit', 'general_payments', 'min_step', 'part_message'], 'safe'],
            [
                ['name', 'product', 'location', 'pledge_name', 'contract_creator', 'contract_winner', 'bill', 'creditor'],
                'string',
                'max' => 255
            ],
            [
                ['company_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Company::className(),
                'targetAttribute' => ['company_id' => 'id']
            ],
            [
                ['winner_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Company::className(),
                'targetAttribute' => ['winner_id' => 'id']
            ],

            [['start_rate', 'count_actions', 'sum_od', 'sum_percents', 'general_sum_credit', 'general_payments', 'min_step'], NumericValidator::class],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Портфель',
            'count_actions' => 'Заемщики',
            'sum_od' => 'ОД',
            'sum_percents' => 'Проценты',
            'general_sum_credit' => 'ОСЗ',
            'count_ip' => 'Кол-во ИП',
            'count_il' => 'Кол-во ИЛ',
            'creditor' => 'Кредитор',
            'is_pledge' => 'Залог',
            'creditor_saler_is_same' => 'Кредитор и Продавец – одно лицо',
            'general_payments' => 'Общая сумма платежей за все время',
            'count_action_before_90_days' => 'Кол-во дел до 90 дней',
            'count_action_90_180_days' => 'Кол-во дел 90-180 дней',
            'count_action_181_365_days' => 'Кол-во дел 181-365 дней',
            'count_action_366_730_days' => 'Кол-во дел 366-730 дней',
            'count_action_731_1095_days' => 'Кол-во дел 731-1095 дней',
            'count_action_over_1096_days' => 'Кол-во дел 1096+ дней',
            'product' => 'Продукт',
            'location' => 'Местоположение',
            'pledge_name' => 'Наименование залога',
            'count_placements_ka' => 'Кол-во размещений в КА',
            'status' => 'Статус',
            'datetime_start' => 'Дата и время начала',
            'datetime_end' => 'Дата и время конца',
            'created_at' => 'Дата и время создания',
            'winner_id' => 'Победитель',
            'company_id' => 'Компания',
            'start_rate' => 'Начальная цена',
            'hide_saler' => 'Скрыть продавца',
            'bill_confirmed' => 'Подтверждение оплаты счета',
            'stage' => 'Этап',
            'contract_creator' => 'Contract Creator',
            'contract_winner' => 'Contract Winner',
            'bill' => 'Bill',
            'bill_payed' => 'Bill Payed',
            'commission_payed' => 'Commission Payed',
            'min_step' => 'Шаг ставки',
            'min_step_percent' => 'Шаг ставки процент',
            'min_step_fix' => 'Шаг ставки фикс',
            'disapprove_text' => 'Текст отказа в публикации портфеля',
            'soft_30_days' => '',
            'soft_90_days' => '',
            'soft_180_days' => '',
            'soft_365_days' => '',
            'soft_730_days' => '',
            'soft_30_days_percent' => '',
            'soft_90_days_percent' => '',
            'soft_180_days_percent' => '',
            'soft_365_days_percent' => '',
            'soft_730_days_percent' => '',
            'legal_percent' => 'Legal collection %',
            'legal_fix' => 'Legal collection (fix, руб)',
            'is_soft_30_days' => '',
            'is_soft_90_days' => '',
            'is_soft_180_days' => '',
            'is_soft_365_days' => '',
            'is_soft_730_days' => '',
            'is_legal_percent' => '',
            'is_legal_fix' => '',
            'type' => '',
            'winner_message' => '',
            'part_message' => 'Уведомление пользователям',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->status = self::STATUS_DRAFT;
        }

        Yii::warning([$this->contract_winner, $this->contract_creator], 'save');


        if ($this->stage != null) {
            if ($this->contract_winner != null && $this->contract_creator != null) {
                $this->stage = self::STAGE_CONTRACTS_LOADED;
            }

            if ($this->bill != null) {
                $this->stage = self::STAGE_BILL_PAYED;
            }

            if ($this->bill_confirmed == 1) {
                $this->stage = self::STAGE_BILL_CONFIRMED;
            }

            if ($this->commission_payed == 1) {
                $this->stage = self::STAGE_COMMISSION_PAYED;
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return double
     */
    public function getCurrentRateAmount()
    {
        $current = $this->start_rate;
        /** @var PortfolioRate $rate */
        $rate = PortfolioRate::find()->where(['portfolio_id' => $this->id])->orderBy('amount desc')->one();

        if ($rate) {
            $current = $rate->amount;
        }

        return $current;
    }

    /**
     * @return boolean
     */
    public function getIsStarted()
    {
        $dateStart = strtotime($this->datetime_start);
        $now = time();

        return $now >= $dateStart;
    }

    /**
     * @return boolean
     */
    public function getIsEnded()
    {
        $dateEnd = strtotime($this->datetime_end);
        $now = time();

        return $now >= $dateEnd;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $files = explode(',', $this->files);

        Yii::warning($files);

        foreach ($files as $file) {
            if ($file != null) {
                $file = PortfolioFile::findOne($file);
                $file->portfolio_id = $this->id;
                Yii::warning($file->save(false));
                Yii::warning($file->errors);
            }
        }

        if ($this->status == self::STATUS_MODERATED && $this->isAttributeChanged('status')) {
            $companies = Company::find()->where(['!=', 'id', $this->company_id])->all();

            foreach ($companies as $company) {
                $startRate = Yii::$app->formatter->asCurrency($this->start_rate, 'rub');
                $dateStart = Yii::$app->formatter->asDate($this->datetime_start, 'php:d M Y H:i:s');
                $dateEnd = Yii::$app->formatter->asDate($this->datetime_end, 'php:d M Y H:i:s');
//                $company->sendEmailMessage("Портфель на торгах", "
//                    <p>Доброго времени суток! </p>
//                    <p>На торгах появился новый портфель «{$this->name}» с начальной ставкой в {$startRate}</p>
//                    <b>Информация о портфеле</b>
//                    <ul>
//                        <li><b>Продукт:</b> {$this->product}</li>
//                        <li><b>Местоположение:</b> {$this->location}</li>
//                        <li><b>Дата и время начала:</b> {$dateStart}</li>
//                        <li><b>Дата и время конца:</b> {$dateEnd}</li>
//                    </ul>
//                ");
            }
        }

//        if(Yii::$app->user != null){
//            if(in_array('stage', array_keys($changedAttributes))){
//
//                $stages = [self::STAGE_CONTRACTS_LOADED, self::STAGE_BILL_PAYED, self::STAGE_BILL_CONFIRMED, self::STAGE_COMMISSION_PAYED];
//
//                foreach ($stages as $stage){
//                    if($this->stage == $stage){
//                        if(Yii::$app->user->identity->company_id == $this->winner_id || Yii::$app->user->identity->isSuperAdmin()){
//                            $company = $this->company;
//                        } else {
//                            $company = $this->winner;
//                        }
//                        $stageName = ArrayHelper::getValue(self::stageLabels(), $this->stage);
//                        $company->sendEmailMessage("Этап в портфеле №{$this->id} «{$this->name}»", "
//                            <p>Доброго времени суток! </p>
//                            <p>Этап в портфеле №{$this->id} «{$this->name}» был изменен на <b>{$stageName}</b></p>
//                        ");
//                    }
//                }
//            }
//        }

        try {
            Yii::$app->elephantio->emit('rate-update', ['id' => $this->id]);
        } catch (\Exception $e){

        }

    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
         Yii::$app->elephantio->emit('rate-update', ['id' => $this->id]);
    }

    /**
     * @return array
     */
    public function getUsers()
    {
        $userPks = array_unique(ArrayHelper::getColumn(PortfolioRate::find()->where(['portfolio_id' => $this->id])->all(),
            'user_id'));

        Yii::warning($userPks);

        return User::find()->where(['id' => $userPks])->ignoreCompany()->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoans()
    {
        return $this->hasMany(Loan::className(), ['portfolio_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function statusLabels()
    {
        return [
            self::STATUS_MODERATION => 'На модерации',
            self::STATUS_MODERATED => 'Одобрен',
            self::STATUS_RATING => 'Прием ставок',
            self::STATUS_REJECTED => 'Отклонен',
            self::STATUS_DONE => 'Завершен',
            self::STATUS_DRAFT => 'Черновик',
            self::STATUS_DONE_NO_DEAL => 'Завершен - ставок не было',
        ];
    }

    public static function stageLabels()
    {
        return [
            self::STAGE_START => 'Подписание договоров',
            self::STAGE_CONTRACTS_LOADED => 'Оплата',
            self::STAGE_BILL_PAYED => 'Счет выставлен',
            self::STAGE_BILL_CONFIRMED => 'Оплата переведена',
            self::STAGE_COMMISSION_PAYED => 'Оплата сервиса получена',
            self::STAGE_DONE => 'Завершен',
            self::STAGE_NONE_BIDS => 'Ставок не было',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWinner()
    {
        return $this->hasOne(Company::className(), ['id' => 'winner_id']);
    }

    /**
     * Список портфелей для компании к которой принадлежит текущий пользователь
     * @return array
     */
    public function getListForUser()
    {
        $query = self::find();

        if (!Yii::$app->user->identity->isSuperAdmin()) {
            $query
                ->andWhere(['company_id' => Yii::$app->user->identity->company_id])
                ->andWhere(['status' => Portfolio::STATUS_DRAFT]);
        }

        return ArrayHelper::map($query->all(),
            'id', 'name');

    }

    /**
     * @return array|null|ActiveRecord
     */
    public function getWinnerRate()
    {
        return PortfolioRate::find()->where(['portfolio_id' => $this->id])->orderBy('amount desc')->one();
    }

    /**
     * Обновляет данные по портфелю
     */
    public function refreshInfo()
    {
        $this->count_actions = count($this->loans);
        $this->general_sum_credit = $this->getTotalCreditSum();
    }

    /*private function getTotalCreditSum()
    {
        return Loan::find()->andWhere(['portfolio_id' => $this->id])->sum('total_amount_debt') ?? 0;
    }*/

    public function setPortfolioSetRate($id, $endDate)
    {
        $portfolioED = Portfolio::findOne($id);
        $findedDate = strtotime($portfolioED['datetime_end']) - strtotime($endDate);
        $findedDate = $findedDate/60;
        if($findedDate<5) {
            $updateDateEnd = strtotime($portfolioED['datetime_end']) + 60*15;
            $updateDateEndR = date('Y-m-d H:i:s', $updateDateEnd);
            $portfolio = Portfolio::findOne($id);
            $portfolio->datetime_end = $updateDateEndR;
            $portfolio->save();
            $companies = Company::find()->where(['=', 'moderated', 1])->all();
            $dateEnd = Yii::$app->formatter->asDate($portfolio->datetime_end, 'php:d M Y H:i:s');
            foreach ($companies as $company) {
//                $company->sendEmailMessage("Портфель {$portfolio->name} продлен!", "
//					<p>Доброго времени суток! </p>
//					<p>Портфель «{$portfolio->name}» продлен до: {$dateEnd}</p>
//				");
            }

        }
    }

    public function getApiKey()
    {
        $cid = Yii::$app->user->identity->company_id;
        $apiKey = Company::findOne(['id' => $cid]);

        return $apiKey;
    }

    public function setPercentAuct($portfolio_id, $ma)
    {
        $ma = str_replace(',','.',$ma);
        $amount = (float) $ma;
        $portfolio_id = (int) $portfolio_id;
        $sqlUpdate = "UPDATE portfolio SET 
				soft_30_days_percent = if(
					is_soft_30_days = 1,
					soft_30_days_percent-".$amount.",
					soft_30_days_percent),
				soft_90_days_percent = if(
					is_soft_90_days = 1,
					soft_90_days_percent-".$amount.",
					soft_90_days_percent),
				soft_180_days_percent = if(
					is_soft_180_days = 1,
					soft_180_days_percent-".$amount.",
					soft_180_days_percent),
				soft_365_days_percent = if(
					is_soft_365_days = 1,
					soft_365_days_percent-".$amount.",
					soft_365_days_percent),
				soft_730_days_percent = if(
					is_soft_730_days = 1,
					soft_730_days_percent-".$amount.",
					soft_730_days_percent),
				legal_percent = if(
					is_legal_percent = 1,
					legal_percent-".$amount.",
					legal_percent)

						WHERE id = ".$portfolio_id."";

        \Yii::$app->getDb()->createCommand($sqlUpdate)->execute();
    }

    public function setPercentAuctLegal($portfolio_id, $ma)
    {
        $ma = str_replace(',','.',$ma);
        $amount = (float) $ma;
        $portfolio_id = (int) $portfolio_id;
        $sqlUpdate = "UPDATE portfolio SET legal_percent = legal_percent-".$amount."
						WHERE id = ".$portfolio_id."";

        \Yii::$app->getDb()->createCommand($sqlUpdate)->execute();
    }

    public function setPercentFix($portfolio_id, $mfix)
    {
        $amount = (int) $mfix;
        $portfolio_id = (int) $portfolio_id;
        $sqlUpdate = "UPDATE portfolio SET legal_fix = legal_fix-".$amount."
						WHERE id = ".$portfolio_id."";

        \Yii::$app->getDb()->createCommand($sqlUpdate)->execute();
    }

    public function getPortfolioPublishOrNotTest($id)
    {
        $sql = "SELECT p.id FROM portfolio as p
				INNER JOIN portfolio_file as f
				WHERE p.id = ".$id." and
				and f.portfolio_id = p.id";
        $result = \Yii::$app->getDb()->createCommand($sql)->queryOne();

//        var_dump($result);
//        exit;

        return $result;
    }

    public function getPortfolioPublishOrNot($id)
    {
        $sql = "SELECT p.id FROM portfolio as p
				INNER JOIN portfolio_file as f
				WHERE p.id = ".$id." and
				p.count_actions is not null and p.sum_od is not null and p.sum_percents is not null and p.general_sum_credit is not null and 
				p.min_step is not null and p.location is not null and p.datetime_start is not null and p.datetime_end is not null and p.start_rate is not null
				and (p.creditor is not null and p.creditor != '') and f.portfolio_id = p.id";

        $result = \Yii::$app->getDb()->createCommand($sql)->queryOne();

        return $result;
    }

    public function getMessageWinner()
    {
        $userId = \Yii::$app->user->identity->company_id;
        $portfolio = Portfolio::find()->select(['id'])->where(['status' => Portfolio::STATUS_RATING])
            ->andWhere(['<=', 'datetime_end', date('Y-m-d H:i:s')])
            ->andWhere(['winner_id'=> $userId])
            ->andWhere(['winner_message'=> null])
            ->all();

        if(!empty($portfolio)) {
            foreach($portfolio as $portfolioItem) {
                $sqlUpdate = "UPDATE portfolio SET winner_message = :winner_message
								WHERE id = :id and winner_message is null";

                \Yii::$app->getDb()->createCommand($sqlUpdate, [':id' => $portfolioItem['id'], ':winner_message' => 1])->execute();
            }

            return $portfolio;
        }
    }

    public function getAutoRateMessage()
    {
        $userId = \Yii::$app->user->identity->company_id;
        $portfolio = Portfolio::find()->where(['status' => Portfolio::STATUS_RATING])
            ->all();

        if(!empty($portfolio)){
            foreach ($portfolio as $portfolioItem) {
                $autoRate = AutoRate::find()->where(['portfolio_id' => $portfolioItem->id, 'company_id' => $userId, 'status' => AutoRate::STATUS_STOP_BY_LIMIT])->one();
                if($autoRate){
                    if($autoRate->message == 0){
                        $autoRate->message = 1;
                        $autoRate->save(false);
                        return $portfolioItem;
                    }
                }
            }
        }
    }



    /**
     * @return array|ActiveRecord[]
     */
    public function getMessageWinnerNew()
    {
        $userId = \Yii::$app->user->identity->company_id;
        $portfolio = Portfolio::find()->select(['id'])->where(['status' => Portfolio::STATUS_RATING])
            ->andWhere(['winner_id'=> $userId])
            ->all();

        if(!empty($portfolio)) {
            foreach($portfolio as $portfolioItem) {

                Portfolio::updateAll(['winner_message' => 1], ['id' => $portfolioItem['id'], 'winner_message' => null]);

//                \Yii::$app->getDb()->createCommand($sqlUpdate, [':id' => $portfolioItem['id'], ':winner_message' => 1])->execute();
            }

            return $portfolio;
        }
    }


    public function getMessageCurrentRate()
    {
        $result = array();
        $userId = \Yii::$app->user->identity->id;
        $sql = "SELECT pr.portfolio_id FROM portfolio_rate as pr
				INNER JOIN portfolio as p ON datetime_end <= date(now())
				WHERE pr.user_id = ".$userId." and pr.portfolio_id = p.id group by pr.portfolio_id";
        $portfolio = \Yii::$app->getDb()->createCommand($sql)->queryAll();
        foreach($portfolio as $portfolioItem) {
            $sqlRate = "SELECT portfolio_id, id, user_id FROM portfolio_rate 
						WHERE portfolio_id = ".$portfolioItem['portfolio_id']." ORDER BY id DESC";
            $resultRate = \Yii::$app->getDb()->createCommand($sqlRate)->queryOne();

            if((int)$resultRate['user_id'] !== $userId) {
                $result[] = $portfolioItem['portfolio_id'];
            }
        }

        return $result;
    }

    public function getMainWinner()
    {
        $userId = \Yii::$app->user->identity->company_id;

//        var_dump($userId);
//        exit;

        $portfolio = Portfolio::find()->where(['status' => Portfolio::STATUS_DONE])
            ->andWhere(['<=', 'datetime_end', date('Y-m-d H:i:s')])
            ->andWhere(['winner_id'=> $userId])
            ->andWhere(['winner_message'=> null])
            ->all();


        if(!empty($portfolio)) {
            foreach($portfolio as $portfolioItem) {

                Portfolio::updateAll(['winner_message' => 1], ['id' => $portfolioItem['id'], 'winner_message' => null]);
            }

            return $portfolio;
        }
    }

    public function getPortfolioWinner()
    {
        $userId = \Yii::$app->user->identity->company_id;

//        var_dump($userId);
//        exit;

        $portfolio = Portfolio::find()->where(['status' => Portfolio::STATUS_DONE])
            ->andWhere(['<=', 'datetime_end', date('Y-m-d H:i:s')])
//            ->andWhere(['winner_id'=> $userId])
            ->andWhere(['winner_message'=> null])
            ->all();



        if(!empty($portfolio)) {
//            $notifyPks = [];
            $portfolios = [];
            foreach($portfolio as $portfolioItem) {

                $ratesUsersPks = array_unique(ArrayHelper::getColumn(PortfolioRate::find()->where(['portfolio_id' => $portfolioItem->id])->all(), 'user_id'));
                $ratesCompaniesPks = array_unique(ArrayHelper::getColumn(User::find()->where(['id' => $ratesUsersPks])->ignoreCompany()->all(), 'company_id'));

                if(in_array($userId, $ratesCompaniesPks) == false){
                    continue;
                }

                $notifiedUsers = [];

                if($portfolioItem->part_message != null){
                    $notifiedUsers = explode(',', $portfolioItem->part_message);
                }

                if(in_array($userId, $notifiedUsers) == false){
                    $notifiedUsers[] = $userId;
                    $notifiedUsers = implode(',', $notifiedUsers);
                    Portfolio::updateAll(['part_message' => $notifiedUsers], ['id' => $portfolioItem['id']]);
                    $notifyPks[] = $portfolioItem->id;
                    $portfolios[] = $portfolioItem;
                }
            }
//
//            $portfolio = array_filter($portfolio, function($port) use($notifyPks){
//                return in_array($port->id, $notifyPks);
//            });

//            var_dump($portfolios);
//            exit;

            return $portfolios;
        }
    }

    public static function getRealTestWinner()
    {
        /** @var Portfolio[] $portfolios */
        $portfolios = Portfolio::find()->where(['status' => Portfolio::STATUS_RATING])->andWhere(['<=', 'datetime_end', date('Y-m-d H:i:s')])->all();


        foreach ($portfolios as $portfolio) {
            $rate = PortfolioRate::find()->where(['portfolio_id' => $portfolio->id])->orderBy('amount desc')->one();

            if ($rate == null) {
                $portfolio->status = Portfolio::STATUS_DONE_NO_DEAL;
                $portfolio->stage = Portfolio::STAGE_START;

                $portfolio->save(false);

                continue;
            }

            $winner = User::findOne($rate->user_id);
        }

        return $winner;
    }

    public function getModeratedCompany($id)
    {
        $sql = "SELECT moderated FROM company WHERE id = ".$id."";
        $result = \Yii::$app->getDb()->createCommand($sql)->queryOne();

        return $result;
    }

    public function getMessageNowRate()
    {
        $result = [];
        $userId = \Yii::$app->user->identity->id;


        $allPortfoliosPks = array_unique(ArrayHelper::getColumn(PortfolioRate::find()->where(['user_id' => $userId])->all(), 'portfolio_id'));


        $portfolios = Portfolio::find()->where(['id' => $allPortfoliosPks, 'status' => Portfolio::STATUS_RATING])->all();

        foreach ($portfolios as $portfolio){
            $ratings = PortfolioRate::find()->where(['portfolio_id' => $portfolio->id])->orderBy('id desc')->all();

            if(count($ratings) >= 2){
                if($ratings[0]->user_id != $userId){
                    if($ratings[1]->user_id == $userId && $ratings[0]->last_rate_message == 0){
                        $result[] = $ratings[0];
                        $ratings[0]->last_rate_message = 1;
                        $ratings[0]->save(false);
                    }
                }
            }
        }

//        $lastRate = PortfolioRate::find()->where([''])->orderBy('id desc')->joinWith('portfolio')->all();
//
//        $portfolio = \Yii::$app->getDb()->createCommand($sql)->queryAll();
//
//        foreach($portfolio as $portfolioItem) {
//            $rate = PortfolioRate::find()->where(['portfolio_id' => $portfolioItem['portfolio_id']])->orderBy('id desc')->one();
//
//            if($rate->user_id !== $userId) {
//                $result[] = $rate;
//            }
//        }

        return $result;
    }


}
