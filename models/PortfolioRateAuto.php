<?php

namespace app\models;

use app\validators\NumericValidator;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

class PortfolioRateAuto extends \yii\db\ActiveRecord
{
    public $amount;
    public $amount_fix;
    public $limit_percent;
    public $limit_fix;
    public $active;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'portfolio_rate_auto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['amount'], 'number', 'min' => 0],
            [['amount_fix'], 'number', 'min' => 0],
            [['limit_percent'], 'number', 'min' => 0],
            [['limit_fix'], 'number', 'min' => 0],
            [['active'], 'integer'],
        ];
    }

    public function setAutoRate($portfolioId, $userId, $companyId, $amountA, $amount_fixA, $limit_percent, $limit_fix)
    {
        $created_at = date('Y-m-d H:i:s');
        $sqlInsert = "INSERT INTO portfolio_rate_auto(active, portfolio_id, user_id, company_id, amount, amount_fix, limit_percent, limit_fix, created_at)
						VALUES(1, $portfolioId, $userId, $companyId, $amountA, $amount_fixA, $limit_percent, $limit_fix, '$created_at')";


        \Yii::$app->getDb()->createCommand($sqlInsert)->execute();
    }

    public function getAutoRate()
    {
        //$rate = array();
        $sql = 'SELECT *, user_id, id FROM portfolio_rate_auto WHERE limit_percent>0 OR limit_fix>0
		GROUP BY user_id, id ORDER BY id ASC 
		';
        $rate = \Yii::$app->getDb()->createCommand($sql)->queryAll();
        //var_dump($rate);
        //die();
        return $rate;

    }

    public function getAutoRateUser($portfolio_id, $userId = null)
    {
        if($userId == null) {
            $userId = \Yii::$app->user->identity->id;
        }

        $sql = 'SELECT *, user_id, id FROM portfolio_rate_auto 
				WHERE (limit_percent>0 OR limit_fix>0) AND portfolio_id = '.$portfolio_id.' AND user_id = '.$userId.'
				ORDER BY id DESC LIMIT 0, 1';
        $rate = \Yii::$app->getDb()->createCommand($sql)->queryOne();

        return $rate;
    }

    /*public function getCountAutoRateUser($portfolio_id)
    {
        $sql = 'SELECT portfolio_id, created_at FROM portfolio_rate_auto
                WHERE (limit_percent>0 OR limit_fix>0) AND portfolio_id = '.$portfolio_id.'
                ORDER BY created_at ASC limit 0, 1';
        $user = \Yii::$app->getDb()->createCommand($sql)->queryOne();
        //var_dump($user);
        return $user;
    }*/

    public function getCountAutoRateUser($portfolio_id)
    {
        $sql = 'SELECT MIN(created_at) FROM portfolio_rate_auto 
				WHERE (limit_percent>0 OR limit_fix>0) AND portfolio_id = '.$portfolio_id.'
				ORDER BY created_at ASC limit 0, 1';
        $user = \Yii::$app->getDb()->createCommand($sql)->queryOne();
        //var_dump($user);
        return $user;
    }

    public function getCount2AutoRateUser($portfolio_id)
    {
        $sql = 'SELECT created_at FROM portfolio_rate_auto 
				WHERE (limit_percent>0 OR limit_fix>0) AND portfolio_id = '.$portfolio_id.'
				ORDER BY created_at';
        $user = \Yii::$app->getDb()->createCommand($sql)->queryOne();
        //var_dump($user);
        return $user;
    }

    public function updateRate($portfolio_id, $amount, $amount_fix)
    {
        $sqlUpdate = "UPDATE portfolio SET 
		soft_30_days_percent = if(
			is_soft_30_days = 1,
			soft_30_days_percent-".$amount.",
			soft_30_days_percent),
		soft_90_days_percent = if(
			is_soft_90_days = 1,
			soft_90_days_percent-".$amount.",
			soft_90_days_percent),
		soft_180_days_percent = if(
			is_soft_180_days = 1,
			soft_180_days_percent-".$amount.",
			soft_180_days_percent),
		soft_365_days_percent = if(
			is_soft_365_days = 1,
			soft_365_days_percent-".$amount.",
			soft_365_days_percent),
		soft_730_days_percent = if(
			is_soft_730_days = 1,
			soft_730_days_percent-".$amount.",
			soft_730_days_percent),
		legal_percent = if(
			is_legal_percent = 1,
			legal_percent-".$amount.",
			legal_percent)

				WHERE id = ".$portfolio_id."";

        \Yii::$app->getDb()->createCommand($sqlUpdate)->execute();

        if($amount_fix) {
            $sqlUpdateFix = "UPDATE portfolio SET 
				legal_fix = if(
					is_legal_fix = 1,
					legal_fix-".$amount_fix.",
					legal_fix)	
				WHERE id = ".$portfolio_id."";
            \Yii::$app->getDb()->createCommand($sqlUpdateFix)->execute();
        }
    }

    public function startAutoRate($created_atNow)
    {
        $rate = array();
        $users = array();
        $users2 = array();
        $port = 0;
        $rate = $this->getAutoRate();
        //var_dump($rate);
        foreach($rate as $rateItem) {
            $dt = date("Y-m-d H:i:s", strtotime($rateItem['created_at']));
            $dateT = $rateItem['created_at'];
            $randT = random_int(60, 270);
            $dateAuto = date('Y-m-d H:i:s', strtotime($rateItem['created_at']) + 60);
            $newPortfolio = (int)$rateItem['portfolio_id'];
            $users = $this->getCountAutoRateUser($newPortfolio);
            $dt2 = $users['MIN(created_at)'];


            $users2 = count($this->getCount2AutoRateUser($newPortfolio, $dateAuto));
            if($dt2 == $created_atNow) {
                continue;
            }
            //die();
            if($dateAuto<$created_atNow && $dateT == $users['MIN(created_at)']) {
                $company_id = $rateItem['company_id'];
                $user_id = $rateItem['user_id'];
                $portfolio_id = $rateItem['portfolio_id'];

                $sql = 'SELECT * FROM portfolio WHERE id = '.$portfolio_id.'';
                $portfolio[] = \Yii::$app->getDb()->createCommand($sql)->queryOne();
                $sqlRate = 'SELECT user_id FROM portfolio_rate WHERE portfolio_id = '.$portfolio_id.' ORDER BY id ASC LIMIT 0, 1';

                $portfolioRate[] = \Yii::$app->getDb()->createCommand($sqlRate)->queryOne();
                //var_dump($portfolioRate);
                //die();
                $pr = $portfolioRate[0]['user_id'];
                $pr1 = $user_id;

                if($pr1!==$pr) {
                    //var_dump($pr1);

                    $amount = (float) $rateItem['amount'];
                    $amount_fix = (float) $rateItem['amount_fix'];
                    foreach($portfolio as $portfolioItem) {
                        if(!empty($portfolioItem['legal_fix'])) {
                            $amount_fixL = (float) $portfolioItem['legal_fix'] - $amount_fix;
                        } else {
                            $amount_fixL = 0;
                        }
                        if(!empty($portfolioItem['legal_percent'])) {
                            $amountL = (float)$portfolioItem['legal_percent'] - $amount;

                        } else {
                            $amountL = 0;
                        }
                        if(!empty($portfolioItem['soft_30_days_percent'])) {
                            $amount30 = (float)$portfolioItem['soft_30_days_percent'] - $amount;
                        } else {
                            $amount30 = 0;
                        }
                        if(!empty($portfolioItem['soft_90_days_percent'])) {
                            $amount90 = (float)$portfolioItem['soft_90_days_percent'] - $amount;
                        } else {
                            $amount90 = 0;
                        }
                        if(!empty($portfolioItem['soft_180_days_percent'])) {
                            $amount180 = (float)$portfolioItem['soft_180_days_percent'] - $amount;
                        } else {
                            $amount180 = 0;
                        }
                        if(!empty($portfolioItem['soft_365_days_percent'])) {
                            $amount365 = (float)$portfolioItem['soft_365_days_percent'] - $amount;
                        } else {
                            $amount365 = 0;
                        }
                        if(!empty($portfolioItem['soft_730_days_percent'])) {
                            $amount730 = (float)$portfolioItem['soft_730_days_percent'] - $amount;
                        } else {
                            $amount730 = 0;
                        }
                    }
                    $created_at = date('Y-m-d H:i:s');
                    $this->updateRate($portfolio_id, $amount, $amount_fix);
                    $sqlInsert = "INSERT INTO portfolio_rate(amount, count_fix, user_id, company_id, portfolio_id, count_soft_30, count_soft_90, count_soft_180, count_soft_365, 
					count_soft_730, count_legal, count_legal_fix, created_at)
					VALUES($amount, 
					$amount_fix, 
					$user_id, 
					$company_id, $portfolio_id, 
					$amount30, $amount90, $amount180, 
					$amount365, $amount730,
					$amountL, 
					$amount_fixL, 
					'$created_at')";
                    \Yii::$app->getDb()->createCommand($sqlInsert)->execute();

                    $sqlUpdate = "UPDATE portfolio_rate_auto SET limit_percent = limit_percent-".$amount.", limit_fix = limit_fix-".$amount_fix.", 
									created_at = '".$created_atNow."'
								WHERE portfolio_id = ".$portfolio_id." and user_id=".$user_id."";

                    \Yii::$app->getDb()->createCommand($sqlUpdate)->execute();


                }
                //continue;
            }
            //$users = $this->getCountAutoRateUser($portfolio_id);
            //$port = (int)$users['portfolio_id'];
            //var_dump($port);

        }
    }
}
