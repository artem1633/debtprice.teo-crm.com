<?php

namespace app\models;

use app\queries\DebtorQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "debtor".
 *
 * @property int $id
 * @property string $outer_id Идентификатор из ПО кредитора
 * @property string $birthday_date Дата рождения
 * @property int $age Возраст
 * @property int $gender Пол
 * @property int $marital_status Семейное положение
 * @property string $birthplace Место рождения
 * @property string $education Образование
 * @property string $email
 * @property int $number_minor_children Кол-во несовершеннолетних детей
 * @property string $owner_property Наличие в собственности имущества
 * @property string $fact_residence_postcode Индекс фактического проживания
 * @property string $fact_residence_region Регион фактического проживания
 * @property string $fact_residence_city Город фактического проживания
 * @property string $fact_residence_address Адрес фактического проживания
 * @property string $register_region Регион постоянной регистрации
 * @property string $register_city Город постоянной регистрации
 * @property string $register_address Адрес постоянной регистрации
 * @property int $status Статус
 * @property string $workplace Место работы
 * @property string $position Должность
 * @property string $employer_city Город работодателя
 * @property string $employer_address Адрес работодателя
 * @property int $period_last_job Срок работы (лет) на последнем рабочем месте
 * @property string $genderLabel
 * @property string $maritalLabel
 * @property int $company_id Компания
 *
 * @property Loan[] $loans
 * @property DebtorStatus $debtorStatus
 */
class Debtor extends ActiveRecord
{
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    const NOT_MARRIED = 0;
    const MARRIED = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'debtor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['age', 'gender', 'marital_status', 'number_minor_children', 'status', 'period_last_job', 'company_id'], 'integer'],
            [['gender'], 'in', 'range' => [1, 2], 'message' => 'Некорректное значение'],
            [['marital_status'], 'in', 'range' => [0, 1], 'message' => 'Некорректное значение'],
            [['birthplace', 'owner_property', 'workplace', 'position'], 'string'],
            [
                [
                    'outer_id',
                    'education',
                    'email',
                    'fact_residence_postcode',
                    'fact_residence_region',
                    'fact_residence_city',
                    'fact_residence_address',
                    'register_region',
                    'register_city',
                    'register_address',
                    'employer_city',
                    'employer_address'
                ],
                'string',
                'max' => 255
            ],
            [
                'status',
                'exist',
                'message' => 'Указан несуществующий статус должника',
                'targetClass' => DebtorStatus::className(),
                'targetAttribute' => 'id'
            ],
            [['birthday_date'], 'date', 'format' => 'Y-m-d', 'message' => 'Некорректный формат даты'],
//            ['outer_id', 'unique', 'filter' => function (ActiveQuery $query){
//                $query->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
//            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'outer_id' => 'Идентификатор должника из ПО кредитора',
            'birthday_date' => 'Дата рождения',
            'age' => 'Возраст',
            'gender' => 'Пол',
            'marital_status' => 'Семейное положение',
            'birthplace' => 'Место рождения',
            'education' => 'Образование',
            'email' => 'Email',
            'number_minor_children' => 'Кол-во несовершеннолетних детей',
            'owner_property' => 'Наличие в собственности имущества',
            'fact_residence_postcode' => 'Индекс фактического проживания',
            'fact_residence_region' => 'Регион фактического проживания',
            'fact_residence_city' => 'Город фактического проживания',
            'fact_residence_address' => 'Адрес фактического проживания',
            'register_region' => 'Регион постоянной регистрации',
            'register_city' => 'Город постоянной регистрации',
            'register_address' => 'Адрес постоянной регистрации',
            'status' => 'Статус',
            'workplace' => 'Место работы',
            'position' => 'Должность',
            'employer_city' => 'Город работодателя',
            'employer_address' => 'Адрес работодателя',
            'period_last_job' => 'Срок работы (лет) на последнем рабочем месте',
            'company_id' => 'Компания',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert && !$this->company_id){
            $this->company_id = Yii::$app->user->identity->company_id;
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoans()
    {
        return $this->hasMany(Loan::className(), ['debtor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDebtorStatus()
    {
        return $this->hasOne(DebtorStatus::className(), ['status' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * {@inheritdoc}
     * @return DebtorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DebtorQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function getGenderList()
    {
        return [
            self::GENDER_MALE => 'Мужской',
            self::GENDER_FEMALE => 'Женский',
        ];
    }

    /**
     * @return string|null
     */
    public function getGenderLabel()
    {
            return $this->getGenderList()[$this->gender] ?? null;
    }

    /**
     * @return array
     */
    public function getMaritalStatuses()
    {
        return [
            self::NOT_MARRIED => 'Не состоит в браке',
            self::MARRIED => 'Состоит в браке',
        ];
    }

    /**
     * @return mixed
     */
    public function getMaritalLabel()
    {
        return $this->getMaritalStatuses()[$this->marital_status] ?? null;
    }

    /**
     * Список должников
     */
    public function getList()
    {
        return ArrayHelper::map(self::find()
            ->andWhere(['company_id' => Yii::$app->user->identity->company_id])
            ->all(), 'id', 'outer_id');
    }

    /**
     * Добавляет должника и кредит
     * @param array $data Данные полученные из пост запрсоа
     * @return array
     */
    public static function addApi($data)
    {
        $add_result = [];

        $company = Company::findOne(['api_key' => $data['token']]) ?? null;

        $debtor_model = new Debtor();
        $loan_model = new Loan();

        if ($debtor_model->load($data, 'debtor') && $loan_model->load($data, 'loan')) {
            Yii::info('Debtor and Loan Loaded!', 'test');

            if ($debtor_model->validate() && $loan_model->validate()) {
                Yii::info('Debtor and Loan Validated!', 'test');

                $debtor_model->company_id = $company->id;

                if (!$debtor_model->save()) {
                    return ['success' => 0, 'error' => $debtor_model->errors];
                }

                $add_result['debtor_id'] = $debtor_model->id;

                $loan_model->debtor_id = $debtor_model->id;
                $loan_model->save();
                $add_result['loan_id'] = $loan_model->id;
            } else {
                return ['success' => 0, 'error' => array_merge($debtor_model->errors, $loan_model->errors)];
            }
        } else {
            return ['success' => 0, 'error' => array_merge($debtor_model->errors, $loan_model->errors)];
        }


//        try {
//            $transaction = Yii::$app->db->beginTransaction();
//
//            $transaction->begin();
//
//            if ($debtor_model->load($data, 'debtor')) {
//                Yii::info('Debtor Loaded!', 'test');
//
//                if (!$debtor_model->save()) {
//                    $transaction->rollBack();
//                    return ['success' => 0, 'error' => $debtor_model->errors];
//                }
//                $add_result['debtor_id'] = $debtor_model->id;
//            } else {
//                Yii::info('Debtor NOT Loaded!', 'test');
//
//                $transaction->rollBack();
//                return ['success' => 0, 'error' => 'Ошибка получения данных о должнике. Данные отсутствуют'];
//            }
//            if ($loan_model->load($data, 'loan')) {
//                Yii::info('Loan Loaded!', 'test');
//                $loan_model->debtor_id = $debtor_model->id;
//                if (!$loan_model->save()) {
//                    $transaction->rollBack();
//                    return ['success' => 0, 'error' => $loan_model->errors];
//                }
//                $add_result['loan_id'] = $loan_model->id;
//            } else {
//                Yii::info('Loan NOT Loaded!', 'test');
//                $transaction->rollBack();
//                return ['success' => 0, 'error' => 'Ошибка получения данных о деле. Данные отсутствуют'];
//            }
//
//            $transaction->commit();
////            Yii::info($debtor_model->getAttributes(), 'test');
////            Yii::info($loan_model->getAttributes(), 'test');
//
//        } catch (\Exception $e) {
//            return ['success' => 0, 'error' => $e->getMessage()];
//        }


        return ['success' => 1, 'data' => $add_result];
    }

}
