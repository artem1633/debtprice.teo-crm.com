<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auto_rate_log".
 *
 * @property int $id
 * @property int $action Действие
 * @property int $portfolio_id Портфель
 * @property int $company_id Компания
 * @property int $user_id Пользователь
 * @property double $amount Сумма
 * @property double $od ОД
 * @property double $osz ОСЗ
 * @property string $datetime Дата и время
 *
 * @property Company $company
 * @property Portfolio $portfolio
 * @property User $user
 */
class AutoRateLog extends \yii\db\ActiveRecord
{
    const ACTION_ON = 0;
    const ACTION_STOP_USER = 1;
    const ACTION_UPDATE = 2;
    const ACTION_LIMIT = 3;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auto_rate_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['action', 'portfolio_id', 'company_id', 'user_id'], 'integer'],
            [['amount', 'od', 'osz'], 'number'],
            [['datetime'], 'safe'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['portfolio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Portfolio::className(), 'targetAttribute' => ['portfolio_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action' => 'Действие',
            'portfolio_id' => 'Портфель',
            'company_id' => 'Компания',
            'user_id' => 'Пользователь',
            'amount' => 'Сумма',
            'od' => 'ОД',
            'osz' => 'ОСЗ',
            'datetime' => 'Дата и время',
        ];
    }

    /**
     * @return array
     */
    public static function actionLabels()
    {
        return [
            self::ACTION_ON => 'Включение',
            self::ACTION_STOP_USER => 'Выключение пользователем',
            self::ACTION_UPDATE => 'Обновление',
            self::ACTION_LIMIT => 'Отключение превышением лимита',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPortfolio()
    {
        return $this->hasOne(Portfolio::className(), ['id' => 'portfolio_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
