<?php

namespace app\models;

use app\validators\NumericValidator;
use ElephantIO\Exception\ServerConnectionFailureException;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Exception;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "portfolio_rate".
 *
 * @property int $id
 * @property int $portfolio_id Портфель
 * @property double $amount Ставка
 * @property int $user_id Пользователь
 * @property int $company_id Компания
 * @property string $created_at
 * @property double $count_fix Фиксированное значение
 * @property double $count_soft_30 Ставка софт 1
 * @property double $count_soft_90 Ставка софт 2
 * @property double $count_soft_180 Ставка софт 3
 * @property double $count_soft_365 Ставка софт 4
 * @property double $count_soft_730 Ставка софт 5
 * @property double $count_legal Ставка legal %
 * @property double $count_legal_fix Ставка legal fix
 * @property int $last_rate_message Уведомление о перебитии
 * @property int $is_auto Авто
 *
 * @property Company $company
 * @property Portfolio $portfolio
 * @property User $user
 */
class PortfolioRate extends \yii\db\ActiveRecord
{
	public $amountA;
	public $amount_fixA;
	public $limit_percent;
	public $limit_fix;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'portfolio_rate';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        if(Yii::$app->user->isGuest == false && Yii::$app->controller->module->id != 'api'){
            return [
                [
                    'class' => TimestampBehavior::class,
                    'updatedAtAttribute' => null,
                    'createdAtAttribute' => 'created_at',
                    'value' => date('Y-m-d H:i:s'),
                ],
                [
                    'class' => BlameableBehavior::class,
                    'updatedByAttribute' => null,
                    'createdByAttribute' => 'company_id',
                    'value' => Yii::$app->user->identity->company_id,
                ],
                [
                    'class' => BlameableBehavior::class,
                    'updatedByAttribute' => null,
                    'createdByAttribute' => 'user_id',
                    'value' => Yii::$app->user->identity->id,
                ],
            ];
        }

        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['amount'], 'required'],
            [['portfolio_id', 'user_id', 'company_id', 'last_rate_message', 'is_auto'], 'integer'],
			//[['count_fix'], 'string'],
            [['created_at', 'amount'], 'safe'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['portfolio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Portfolio::className(), 'targetAttribute' => ['portfolio_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
			[['count_fix'], 'number', 'min' => 0],
			[['count_soft_30'], 'number', 'min' => 0],
			[['count_soft_90'], 'number', 'min' => 0],
			[['count_soft_180'], 'number', 'min' => 0],
			[['count_soft_365'], 'number', 'min' => 0],
			[['count_soft_730'], 'number', 'min' => 0],
			[['count_legal'], 'number', 'min' => 0],
			[['count_legal_fix'], 'number', 'min' => 0],
			[['amountA'], 'number', 'min' => 0],		
			[['amount_fixA'], 'number', 'min' => 0],
			[['limit_percent'], 'number', 'min' => 0],
			[['limit_fix'], 'number', 'min' => 0],
            [['amount'], NumericValidator::class],
			[['count_fix'], NumericValidator::class],
            ['amount', function(){
                $portfolio = Portfolio::findOne($this->portfolio_id);
				$auctPerc = $this->getPercentAuction($this->portfolio_id);
                $rate = PortfolioRate::find()->where(['portfolio_id' => $this->portfolio_id])->orderBy('amount desc')->one();
				if(!empty($auctPerc)) {
					foreach($auctPerc as $auctPercItem => $auctPercValue) {
						if($auctPercValue == 1) {
							$keyPerc = $auctPercItem;
							$keyPerc = substr($auctPercItem, 3);
							if($keyPerc !== 'legal_percent') {
								$keyPerc = $keyPerc.'_percent';
							}
						}
					}
				}
				if(!empty($keyPerc)) {
					$min = $portfolio->$keyPerc;
					if($this->amount >= $min){
						$this->addError('amount', "Ставка должна быть меньше чем ".Yii::$app->formatter->asCurrency($min, '%'));
						return false;
					}
				} else {
					if($rate){
						$min = $rate->amount;
					} else {
						$min = $portfolio->start_rate;	
					}
					if($this->amount <= $min){
						$this->addError('amount', "Ставка должна быть больше чем ".Yii::$app->formatter->asCurrency($min, 'rub'));
						return false;
					}
					
				}
            }],
			
			['count_fix', function(){
                $portfolio = Portfolio::findOne($this->portfolio_id);
				$auctPerc = $this->getLegalFixAuction($this->portfolio_id);
                $rate = PortfolioRate::find()->where(['portfolio_id' => $this->portfolio_id])->orderBy('count_fix desc')->one();
				
				$max = $portfolio->legal_fix;
				$min = $portfolio->min_step_fix;	
				if($this->count_fix >= $max){
					$this->addError('count_fix', "Ставка должна быть меньше чем ".Yii::$app->formatter->asCurrency($max, 'rub'));
					return false;
				}
				if($this->count_fix < $min){
					$this->addError('count_fix', "Ставка должна быть больше чем ".Yii::$app->formatter->asCurrency($min, 'rub'));
					return false;
				}
					
				
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'portfolio_id' => 'Портфель',
            'amount' => 'Ставка',
			'count_fix' => 'Фиксированое значение',
            'user_id' => 'Пользователь',
            'company_id' => 'Компания',
            'created_at' => 'Дата и время',
            'last_rate_message' => 'Уведомление о перебитии',
            'is_auto' => 'Авто',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttribute)
    {
        parent::afterSave($insert, $changedAttribute);

        try {
            Yii::$app->elephantio->emit('rate-update', ['id' => $this->id]);
        } catch (\Exception $e) {
            \Yii::warning($e->getMessage());
        }

        $portfolio = $this->portfolio;
        $newRate = Yii::$app->formatter->asCurrency($this->amount, 'rub');

        foreach ($portfolio->users as $user){
            if($user->id == $this->user_id){
                continue;
            }

//            $user->sendEmailMessage("Изменилась ставка по портфелю «{$portfolio->name}»", "<p>Доброго времени суток, {$user->name}!</p><p>Аукционная ставка по портфелю №{$portfolio->id} «{$portfolio->name}» изменилась на {$newRate}</p>");

            // try{
            //     Yii::$app->mailer->compose()
            //         ->setFrom('debtprice@yandex.ru')
            //         ->setTo($user->email)
            //         ->setSubject("Изменилась ставка по портфелю «{$portfolio->name}»")
            //         ->setHtmlBody("<p>Доброго времени суток, {$user->name}!</p><p>Аукционная ставка по портфелю №{$portfolio->id} «{$portfolio->name}» изменилась на {$newRate}</p>")
            //         ->send();
            // } catch(\Exception $e){
            //     Yii::warning($e);
            // }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPortfolio()
    {
        return $this->hasOne(Portfolio::className(), ['id' => 'portfolio_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
	
	public function getPercentSetRate($id)
	{
		$sql = 'SELECT sum_od, general_sum_credit, min_step, min_step_fix, min_step_percent FROM portfolio WHERE id = "'.$id.'"';
		$res = \Yii::$app->getDb()->createCommand($sql)->queryOne();
		
		return $res;
	}
	
	public function getPercentSetRateHistory($id)
	{
		$sql = 'SELECT * FROM portfolio_rate WHERE portfolio_id = "'.$id.'" ORDER BY id DESC';
		$res = \Yii::$app->getDb()->createCommand($sql)->queryAll();
		
		return $res;
	}
	
	public function getPercentSetRateHistoryLast($id)
	{
		$sql = 'SELECT * FROM portfolio_rate WHERE portfolio_id = "'.$id.'" ORDER BY id DESC LIMIT 0, 1';
		$res = \Yii::$app->getDb()->createCommand($sql)->queryOne();
		
		return $res;
	}
	
	public function getPercentAuction($id)
	{
		$sql = 'SELECT is_soft_30_days, is_soft_90_days, is_soft_180_days, is_soft_365_days, is_soft_730_days, is_legal_percent
					FROM portfolio WHERE id = "'.$id.'"';
		$res = \Yii::$app->getDb()->createCommand($sql)->queryOne();
		
		return $res;
	}
	
	public function getLegalFixAuction($id)
	{
		$sql = 'SELECT is_legal_fix
					FROM portfolio WHERE id = "'.$id.'"';
		$res = \Yii::$app->getDb()->createCommand($sql)->queryOne();
		
		return $res;
	}
}

