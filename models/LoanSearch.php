<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * LoanSearch represents the model behind the search form about `app\models\Loan`.
 */
class LoanSearch extends Loan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'portfolio_id',
                    'debtor_id',
                    'type',
                    'currency',
                    'period',
                    'expired_days',
                    'total_number_payment'
                ],
                'integer'
            ],
            [['date', 'name_collateral', 'assessment_collateral', 'last_payment_date', 'last_contact_date'], 'safe'],
            [
                [
                    'primary_amount_debt',
                    'percent_amount_debt',
                    'other_amount_debt',
                    'total_amount_debt',
                    'amount_last_payment',
                    'total_payment'
                ],
                'number'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Loan::find();

        if (Yii::$app->user->identity->isSuperAdmin() == false){
            $query->leftJoin('debtor', 'loan.debtor_id = debtor.id');
            $query->andWhere(['debtor.company_id' => Yii::$app->user->identity->company_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'portfolio_id' => $this->portfolio_id,
            'debtor_id' => $this->debtor_id,
            'date' => $this->date,
            'type' => $this->type,
            'currency' => $this->currency,
            'primary_amount_debt' => $this->primary_amount_debt,
            'percent_amount_debt' => $this->percent_amount_debt,
            'other_amount_debt' => $this->other_amount_debt,
            'total_amount_debt' => $this->total_amount_debt,
            'period' => $this->period,
            'expired_days' => $this->expired_days,
            'amount_last_payment' => $this->amount_last_payment,
            'total_payment' => $this->total_payment,
            'last_payment_date' => $this->last_payment_date,
            'last_contact_date' => $this->last_contact_date,
            'total_number_payment' => $this->total_number_payment,
        ]);

        $query->andFilterWhere(['like', 'name_collateral', $this->name_collateral])
            ->andFilterWhere(['like', 'assessment_collateral', $this->assessment_collateral]);

        return $dataProvider;
    }
}
