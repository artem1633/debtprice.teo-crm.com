<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PortfolioRate;

/**
 * PortfolioRateSearch represents the model behind the search form about `app\models\PortfolioRate`.
 */
class PortfolioRateSearch extends PortfolioRate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'portfolio_id', 'user_id', 'company_id'], 'integer'],
            [['amount'], 'number'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PortfolioRate::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'portfolio_id' => $this->portfolio_id,
            'amount' => $this->amount,
            'user_id' => $this->user_id,
            'company_id' => $this->company_id,
            'created_at' => $this->created_at,
        ]);

        return $dataProvider;
    }
}
