<?php

namespace app\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "currency".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $icon Иконка
 * @property double $exchange_rate Курс (к рублю)
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['exchange_rate'], 'number'],
            [['name', 'icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'icon' => 'Иконка',
            'exchange_rate' => 'Курс (к рублю)',
        ];
    }

    /**
     * Список всех валют
     * @return array
     */
    public function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

}
