<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Portfolio;

/**
 * PortfolioSearch represents the model behind the search form about `app\models\Portfolio`.
 */
class PortfolioSearch extends Portfolio
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'count_actions', 'count_ip', 'count_il', 'is_pledge', 'count_placements_ka', 'status', 'winner_id'], 'integer'],
            [['name', 'product', 'location', 'pledge_name', 'datetime_start', 'datetime_end'], 'safe'],
            [['sum_od', 'sum_percents', 'general_sum_credit', 'general_payments', 'count_action_before_90_days', 'count_action_90_180_days', 'count_action_181_365_days', 'count_action_366_730_days', 'count_action_731_1095_days', 'count_action_over_1096_days', 'start_rate'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $sort = null)
    {
        $query = Portfolio::find();

        if($sort == null){
            $sort = [
                'defaultOrder' => [
                   // 'id' => SORT_DESC,
                    'status' => SORT_DESC,
                    // 'name' => SORT_ASC,
                    // 'created_at' => SORT_DESC,
                     'datetime_end' => SORT_DESC,
                ],
            ];
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => $sort,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        

        $query->andFilterWhere([
            'id' => $this->id,
            'count_actions' => $this->count_actions,
            'sum_od' => $this->sum_od,
            'sum_percents' => $this->sum_percents,
            'general_sum_credit' => $this->general_sum_credit,
            'count_ip' => $this->count_ip,
            'count_il' => $this->count_il,
            'is_pledge' => $this->is_pledge,
            'general_payments' => $this->general_payments,
            'count_action_before_90_days' => $this->count_action_before_90_days,
            'count_action_90_180_days' => $this->count_action_90_180_days,
            'count_action_181_365_days' => $this->count_action_181_365_days,
            'count_action_366_730_days' => $this->count_action_366_730_days,
            'count_action_731_1095_days' => $this->count_action_731_1095_days,
            'count_action_over_1096_days' => $this->count_action_over_1096_days,
            'count_placements_ka' => $this->count_placements_ka,
            'status' => $this->status,
            'datetime_start' => $this->datetime_start,
            'datetime_end' => $this->datetime_end,
            'winner_id' => $this->winner_id,
            'start_rate' => $this->start_rate,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'product', $this->product])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'pledge_name', $this->pledge_name]);

        return $dataProvider;
    }
}

