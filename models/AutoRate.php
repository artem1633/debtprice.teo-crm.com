<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auto_rate".
 *
 * @property int $id
 * @property int $portfolio_id Портфель
 * @property int $company_id Компания
 * @property int $user_id Пользователь
 * @property double $rate Ставка
 * @property int $status Статус
 * @property int $message Уведомление
 *
 * @property Company $company
 * @property Portfolio $portfolio
 * @property User $user
 */
class AutoRate extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 0;
    const STATUS_STOP_BY_LIMIT = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auto_rate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['portfolio_id', 'company_id', 'user_id', 'message'], 'integer'],
            [['rate'], 'number'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['portfolio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Portfolio::class, 'targetAttribute' => ['portfolio_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'portfolio_id' => 'Портфель',
            'company_id' => 'Компания',
            'user_id' => 'Пользователь',
            'rate' => 'Ставка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPortfolio()
    {
        return $this->hasOne(Portfolio::class, ['id' => 'portfolio_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
