<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AutoRateLog;

/**
 * AutoRateLogSearch represents the model behind the search form about `app\models\AutoRateLog`.
 */
class AutoRateLogSearch extends AutoRateLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'action', 'portfolio_id', 'company_id', 'user_id'], 'integer'],
            [['amount', 'od', 'osz'], 'number'],
            [['datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AutoRateLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'action' => $this->action,
            'portfolio_id' => $this->portfolio_id,
            'company_id' => $this->company_id,
            'user_id' => $this->user_id,
            'amount' => $this->amount,
            'od' => $this->od,
            'osz' => $this->osz,
            'datetime' => $this->datetime,
        ]);

        return $dataProvider;
    }
}
