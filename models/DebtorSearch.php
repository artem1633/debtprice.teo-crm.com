<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DebtorSearch represents the model behind the search form about `app\models\Debtor`.
 */
class DebtorSearch extends Debtor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'age', 'gender', 'marital_status', 'number_minor_children', 'status', 'period_last_job'], 'integer'],
            [['outer_id', 'birthday_date', 'birthplace', 'education', 'email', 'owner_property', 'fact_residence_postcode', 'fact_residence_region', 'fact_residence_city', 'fact_residence_address', 'register_region', 'register_city', 'register_address', 'workplace', 'position', 'employer_city', 'employer_address'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Debtor::find();

        if (Yii::$app->user->identity->isSuperAdmin() == false){
            $query->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'birthday_date' => $this->birthday_date,
            'age' => $this->age,
            'gender' => $this->gender,
            'marital_status' => $this->marital_status,
            'number_minor_children' => $this->number_minor_children,
            'status' => $this->status,
            'period_last_job' => $this->period_last_job,
        ]);

        $query->andFilterWhere(['like', 'outer_id', $this->outer_id])
            ->andFilterWhere(['like', 'birthplace', $this->birthplace])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'owner_property', $this->owner_property])
            ->andFilterWhere(['like', 'fact_residence_postcode', $this->fact_residence_postcode])
            ->andFilterWhere(['like', 'fact_residence_region', $this->fact_residence_region])
            ->andFilterWhere(['like', 'fact_residence_city', $this->fact_residence_city])
            ->andFilterWhere(['like', 'fact_residence_address', $this->fact_residence_address])
            ->andFilterWhere(['like', 'register_region', $this->register_region])
            ->andFilterWhere(['like', 'register_city', $this->register_city])
            ->andFilterWhere(['like', 'register_address', $this->register_address])
            ->andFilterWhere(['like', 'workplace', $this->workplace])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'employer_city', $this->employer_city])
            ->andFilterWhere(['like', 'employer_address', $this->employer_address]);

        return $dataProvider;
    }
}
