<?php

namespace app\models;

use app\queries\LoanQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "loan".
 *
 * @property int $id
 * @property int $debtor_id Должник
 * @property string $date Дата пердоставления кредита
 * @property int $type Тип кредитного продукта
 * @property int $currency_id Валюта
 * @property string $name_collateral Наименование залога
 * @property string $assessment_collateral Оценка залога
 * @property double $primary_amount_debt Сумма задолженности по основному долгу (в валюте долга)
 * @property double $percent_amount_debt Сумма задолженности по процентам (в валюте долга)
 * @property double $other_amount_debt Сумма задолженности по иным платам и штрафам (в валюте долга)
 * @property double $total_amount_debt Общая сумма задолженности (в валюте долга)
 * @property int $period Срок кредита
 * @property int $expired_days Кол-во дней просрочки
 * @property double $amount_last_payment Сумма последнего платежа
 * @property double $total_payment Общая сумма поступившая в счет оплаты по кредиту
 * @property string $last_payment_date Дата последнего платежа
 * @property string $last_contact_date Дата последнего контакта с должником
 * @property int $total_number_payment Число произведенных платежей
 * @property string $currencyLabel
 * @property int $portfolio_id Портфель
 * @property string $pks ID выбранных дел (через запятую)
 *
 * @property Portfolio $portfolio
 * @property Debtor $debtor
 * @property Currency $currency
 * @property ProductType $loanType
 */
class Loan extends ActiveRecord
{
    const CURRENCY_RUB = 1;
    const CURRENCY_USD = 2;

    /** @var string */
    public $pks;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['portfolio_id', 'debtor_id', 'type', 'currency_id', 'period', 'expired_days', 'total_number_payment'],
                'integer'
            ],
            [['pks'], 'safe'],
            [
                [
                    'primary_amount_debt',
                    'percent_amount_debt',
                    'other_amount_debt',
                    'total_amount_debt',
                    'amount_last_payment',
                    'total_payment'
                ],
                'number'
            ],
            [['name_collateral', 'assessment_collateral'], 'string', 'max' => 255],
//            [['debtor_id'], 'required'],
            [
                ['debtor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Debtor::className(),
                'targetAttribute' => ['debtor_id' => 'id']
            ],
            [
                'type',
                'exist',
                'message' => 'Указан несуществующий тип кредитного продукта',
                'targetClass' => ProductType::className(),
                'targetAttribute' => 'id'
            ],
            [
                'currency_id',
                'exist',
                'message' => 'Указана несуществующая валюта',
                'targetClass' => Currency::className(),
                'targetAttribute' => 'id'
            ],
            [['date', 'last_payment_date', 'last_contact_date'], 'date', 'format' => 'Y-m-d', 'message' => 'Некорректный формат даты'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'portfolio_id' => 'Портфель',
            'debtor_id' => 'Должник',
            'date' => 'Дата пердоставления кредита',
            'type' => 'Тип кредитного продукта',
            'currency_id' => 'Валюта',
            'name_collateral' => 'Наименование залога',
            'assessment_collateral' => 'Оценка залога',
            'primary_amount_debt' => 'Сумма задолженности по основному долгу (в валюте долга)',
            'percent_amount_debt' => 'Сумма задолженности по процентам (в валюте долга)',
            'other_amount_debt' => 'Сумма задолженности по иным платам и штрафам (в валюте долга)',
            'total_amount_debt' => 'Общая сумма задолженности (в валюте долга)',
            'period' => 'Срок кредита',
            'expired_days' => 'Кол-во дней просрочки',
            'amount_last_payment' => 'Сумма последнего платежа',
            'total_payment' => 'Общая сумма поступившая в счет оплаты по кредиту',
            'last_payment_date' => 'Дата последнего платежа',
            'last_contact_date' => 'Дата последнего контакта с должником',
            'total_number_payment' => 'Число произведенных платежей',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPortfolio()
    {
        return $this->hasOne(Portfolio::className(), ['id' => 'portfolio_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDebtor()
    {
        return $this->hasOne(Debtor::className(), ['id' => 'debtor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoanType()
    {
        return $this->hasOne(ProductType::className(), ['id' => 'type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * {@inheritdoc}
     * @return LoanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LoanQuery(get_called_class());
    }
}
