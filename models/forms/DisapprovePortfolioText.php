<?php

namespace app\models\forms;

use yii\base\Model;
use app\models\Portfolio;

class DisapprovePortfolioText extends Model
{
    /** @var int */
    public $portfolioId;

    /** @var string */
    public $disapproveText;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['portfolioId', 'disapproveText'], 'required'],
            [['portfolioId'], 'number'],
            [['disapproveText'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'portfolioId' => 'Портфель',
            'disapproveText' => 'Текст отказа',
        ];
    }

    /**
     * @return bool
     */
    public function disapprove()
    {
        if($this->validate()) {
            $portfolio = Portfolio::findOne(['id' => $this->portfolioId]);
            $portfolio->disapprove_text = $this->disapproveText;
            $portfolio->status = Portfolio::STATUS_DRAFT;

            $company = $portfolio->company;

            if ($company != null) {
                $company->sendEmailMessage("Портфель «{$portfolio->name}» не прошел модерацию",
                    "<p>Ваш портфель №{$portfolio->id} «{$portfolio->name}» не прошел модерацию</p><p style='color: red;'>Причина: \"{$this->disapproveText}\"</p>");
            }

            return $portfolio->save(false);
        }

        return false;
    }
}