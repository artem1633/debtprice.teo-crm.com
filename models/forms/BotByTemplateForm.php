<?php

namespace app\models\forms;

use app\models\Bot;
use yii\base\Model;

/**
 * Class BotByTemplateForm
 * @package app\models\forms
 */
class BotByTemplateForm extends Model
{
    /**
     * @var integer
     */
    public $templateId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['templateId'], 'required'],
        ];
    }

    /**
     *
     */
    public function copy()
    {
        $template = Bot::find()->ignoreCompany()->where(['id' => $this->templateId])->one();

        return $template->copy();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'templateId' => 'Шаблон'
        ];
    }
}