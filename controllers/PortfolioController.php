<?php

namespace app\controllers;

use app\models\AutoRate;
use app\models\AutoRateLog;
use app\models\AutoRateLogSearch;
use app\models\forms\DisapprovePortfolioText;
use app\models\LoanSearch;
use app\models\PortfolioFile;
use app\models\PortfolioFileSearch;
use app\models\PortfolioRate;
use app\models\PortfolioRateAuto;
use app\models\PortfolioRateSearch;
use Yii;
use app\models\Portfolio;
use app\models\PortfolioSearch;
use app\models\Company;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * PortfolioController implements the CRUD actions for Portfolio model.
 */
class PortfolioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function beforeAction($action)
    {
        if(\Yii::$app->user->isGuest == false) {
            $portfolio = new Portfolio();
            $messageWinner = $portfolio->getPortfolioWinner();

//			$winner = Portfolio::getRealTestWinner();
//			exit;
//
            $messageSetRate = $portfolio->getMessageNowRate();

            $autoRateMessagePortfolio = $portfolio->getAutoRateMessage();

            if($autoRateMessagePortfolio != null){
                Yii::$app->session->setFlash('warning', "Текущая ставка по портфелю №{$autoRateMessagePortfolio->id} \"{$autoRateMessagePortfolio->name}\" превысила, установленный Вами лимит автоставки");
            }


            if(!empty($messageWinner)) {

//			    foreach($messageWinner as $portfolio){
//                    Yii::$app->session->addFlash('success', "Поздравляем!з
//Вы победили в торгах на портфель №{$portfolio->id} {$portfolio->name} со ставкой ".Yii::$app->formatter->asCurrency($portfolio->currentRateAmount)." ({$portfolio->sum_od}% ОД, {$portfolio->general_sum_credit}% ОСЗ). В ближайшее время менеджер аукциона свяжется с Вами для помощи в оформлении сделки.»
//");
//                }

                $portfolio = $messageWinner[0];

                $od = $portfolio->sum_od;
                $gsc = $portfolio->general_sum_credit;
                $st = $portfolio->currentRateAmount;
                if((!empty($st))&&(!empty($od))&&(!empty($gsc))) {
                    $res = $st/$od*100;
                    $res = round($res, 2);
                    $st = round($st, 0);
                    $st1 = number_format(intval($st), 0, '', ' ');
                    $res = number_format(floatval($res), 2, ',', ' ');
                    $resOZ = $st/$gsc*100;
                    $resOZ = round($resOZ, 2);
                    $resOZ = number_format(floatval($resOZ), 2, ',', ' ');
                    //$st = number_format(floatval($st), 2, ',', ' ');
                } else {
                    $res = '';
                    $resOZ = '';
                }

                if($portfolio->winner_id == Yii::$app->user->identity->company_id){
                    Yii::$app->session->setFlash('success', "Вы победили в торгах на портфель №{$portfolio->id} \"{$portfolio->name}\" со ставкой ".number_format($portfolio->currentRateAmount, 0, 0, ' ')." ₽ ({$res}% ОД, {$resOZ}% ОСЗ). В ближайшее время менеджер аукциона свяжется с Вами для помощи в оформлении сделки.");
                } else {
                    Yii::$app->session->setFlash('warning', "Портфель №{$portfolio->id} \"{$portfolio->name}\" — победил участник со ставкой ".number_format($portfolio->currentRateAmount, 0, 0, ' ')." ₽ ({$res}% ОД, {$resOZ}% ОСЗ). Благодарим Вас за участие в торгах!");
                }
            }
            if(!empty($messageSetRate)) {
                $messageSetRate = $messageSetRate[0];

                $portfolio = Portfolio::findOne($messageSetRate->portfolio_id);

                if($portfolio){
                    Yii::$app->session->setFlash('warning', "<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> Портфель №{$portfolio->id} \"{$portfolio->name}\" — Внимание! Вашу ставку перебили. Если вы намерены продолжать торги, Вам нужно сделать новую.");
                }
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['auto-rate'],
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionAutoRate() {
        $model = new PortfolioRateAuto;
        $created_at = date('Y-m-d H:i:s');
        $model->startAutoRate($created_at);
    }

    /**
     * Lists all Portfolio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PortfolioSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [
            'defaultOrder' => [
                // 'id' => SORT_DESC,
                'status' => SORT_DESC,
                // 'name' => SORT_ASC,
                // 'created_at' => SORT_DESC,
                'datetime_end' => SORT_DESC,
            ],
        ]);

        $dataProvider->query->andWhere(['or', ['stage' => null], ['stage' => 1]]);
        $dataProvider->pagination->pageSize = 50;

        $statuses = [];

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $statuses = [Portfolio::STATUS_RATING, Portfolio::STATUS_MODERATED];
        } else {
            $statuses = [Portfolio::STATUS_RATING, Portfolio::STATUS_MODERATED, Portfolio::STATUS_MODERATION];
        }

        $dataProvider->query->andFilterWhere(['status' => $statuses]);

//        \Yii::warning($dataProvider->query->createCommand()->getRawSql());

        if(isset($_GET['sort']) == false){
            $models = $dataProvider->models;

            usort($models, function($a, $b){
                if($a->status == Portfolio::STATUS_RATING && $b->status == Portfolio::STATUS_RATING){
                    return strtotime($a->datetime_end) > strtotime($b->datetime_end);
                }
                return $a->status < $b->status;
            });

            $dataProvider->setModels($models);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Portfolio model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
//        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $filesSearchModel = new PortfolioFileSearch();
        $filesDataProvider = $filesSearchModel->search([]);
        $filesDataProvider->query->andWhere(['portfolio_id' => $id]);

        return $this->render('view', [
            'model' => $model,
            'filesSearchModel' => $filesSearchModel,
            'filesDataProvider' => $filesDataProvider
        ]);

//        }
    }

    /**
     * Creates a new Portfolio model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Portfolio();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Цессия",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Цессия",
                        'content' => '<span class="text-success">Создание портфеля успешно завершено</span>',
                        'footer' => Html::button('ОК',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Создать еще', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                    ];
                } else {
                    return [
                        'title' => "Цессия",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * @param int $id
     */
    public function actionDownloadAllFiles($id)
    {
        $z = new \ZipArchive();
        $filename = '_archive.zip';

        if(file_exists($filename)){
            unlink($filename);
        }

        $z->open($filename, \ZipArchive::CREATE);
        $portfolio = $this->findModel($id);

        /** @var PortfolioFile[] $files */
        $files = PortfolioFile::find()->where(['portfolio_id' => $id])->all();

        foreach ($files as $file)
        {
            $z->addFile($file->path, $file->name);
        }

        $z->close();

        Yii::$app->response->sendFile($filename, 'Файлы_'.$portfolio->name.'.zip');
    }

    public function actionCreatePerc()
    {
        $request = Yii::$app->request;
        $model = new Portfolio();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Агентский договор",
                    'content' => $this->renderAjax('create_perc', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Агентский договор",
                        'content' => '<span class="text-success">Создание портфеля успешно завершено</span>',
                        'footer' => Html::button('ОК',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Создать еще', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                    ];
                } else {
                    return [
                        'title' => "Агентский договор",
                        'content' => $this->renderAjax('create_perc', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create_perc', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionRateHistory($portfolio_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $searchModel = new PortfolioRateSearch();
        $model = new PortfolioRate(['portfolio_id' => $portfolio_id]);
        $modelPortfolio = new Portfolio(['id' => $portfolio_id]);
        $portfolio = $this->findModel($portfolio_id);
        $auctPerc = $model->getPercentAuction($portfolio_id);

        $rates = PortfolioRate::find()->select('portfolio_rate.*, company.name as company_name, user.email as user_email')->joinWith('user')->joinWith('company')->andWhere(['portfolio_id' => $portfolio_id])->orderBy('id desc')->asArray()->all();

        $rates = ArrayHelper::merge($rates, [
            [
                'user_id' => 0,
                'amount' => number_format(intval($portfolio->start_rate), 0, 0, ' '),
                'created_at' => $portfolio->datetime_start,
            ],
        ]);

//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider->query->andWhere(['portfolio_id' => $portfolio_id])->orderBy('id desc');

        $dataProvider = new ArrayDataProvider(['models' => $rates]);
        $dataProvider->pagination = false;
        $titleSet = "История ставок. Портфель ".$portfolio->id." ".$portfolio->name;

        return [
            'title' => $titleSet,
            'content' => $this->renderAjax('@app/views/portfolio-rate/index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'portfolio' => $portfolio,
                'auctPerc' => $auctPerc,
                'modelR' => $model,
                'modelPortfolio' => $modelPortfolio,
            ]),
            'footer' => Html::button('ОК', ['class' => 'btn btn-default btn-block', 'data-dismiss' => "modal"])

        ];
    }

    /**
     * @param $portfolio_id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionApprove($portfolio_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($portfolio_id);

        $model->status = Portfolio::STATUS_MODERATED;
        $model->save(false);

        $company = $model->company;

        if ($company != null) {
            $company->sendEmailMessage("Портфель «{$model->name}» прошел модерацию",
                "<p>Ваш портфель №{$model->id} «{$model->name}» успешно прошел модерацию</p>");
        }

        return ['forceReload' => '#crud-datatable-pjax', 'forceClose' => true];
    }

//	/**
//     * @param $portfolio_id
//     * @return array
//     * @throws NotFoundHttpException
//     */
//    public function actionDisapprove($portfolio_id)
//    {
//        Yii::$app->response->format = Response::FORMAT_JSON;
//        $model = $this->findModel($portfolio_id);
//
//        $model->status = Portfolio::STATUS_DRAFT;
//        $model->save(false);
//
//        $company = $model->company;
//
//        if ($company != null) {
//            $company->sendEmailMessage("Портфель «{$model->name}» не прошел модерацию",
//                "<p>Ваш портфель №{$model->id} «{$model->name}» не прошел модерацию</p>");
//        }
//
//        return ['forceReload' => '#crud-datatable-pjax', 'forceClose' => true];
//    }

    /**
     * @param $portfolio_id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionDisapprove($portfolio_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new DisapprovePortfolioText(['portfolioId' => $portfolio_id]);

        if($model->load($request->post()) && $model->disapprove()){
            return ['forceReload' => '#crud-datatable-pjax', 'forceClose' => true];
        } else {
            return [
                'title' => "Отказать",
                'content' => $this->renderAjax('disapprove', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Отмена',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button('Готово', ['class' => 'btn btn-primary', 'type' => "submit"])

            ];
        }
    }

    /**
     * @param $id
     * @param $attribute
     * @throws NotFoundHttpException
     */
    public function actionUpdateAttributeBoolean($id, $attribute)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        $model->$attribute = 1;

        if ($attribute == 'bill_confirmed') {
//            $model->winner->sendEmailMessage("Продавец подтвердил оплату счета", "
//                <p>Доброго времени суток! </p>
//                <p>Продавец подтвердил оплату счета по портфелю №{$model->id} «{$model->name}»</p>
//            ");
        }

        if ($attribute == 'commission_payed') {
//            $model->company->sendEmailMessage("Администратор подтвердил оплату комиссии", "
//                <p>Доброго времени суток! </p>
//                <p>Администратор подтвердил оплату комиссии №{$model->id} «{$model->name}»</p>
//            ");
        }


        $model->save(false);
    }

    /**
     * @param integer $portfolio_id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionSetRate($portfolio_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $searchModel = new PortfolioRateSearch();
        $model = new PortfolioRate(['portfolio_id' => $portfolio_id]);
        $g = new Portfolio;
        $modelRateAuto = new PortfolioRateAuto;
        $getAutoRate = $modelRateAuto->getAutoRateUser($portfolio_id);
        $auctPerc = $model->getPercentAuction($portfolio_id);
        $request = Yii::$app->request;
        $portfolio = $this->findModel($portfolio_id);
        $per = $model->getPercentSetRate($portfolio_id);
        $rHistory = $model->getPercentSetRateHistory($portfolio_id);
        $rHistoryItemL = $model->getPercentSetRateHistoryLast($portfolio_id);
        $auctPerc = $model->getPercentAuction($portfolio_id);
        $titleSet = "Сделать ставку. Портфель ".$portfolio->id." ".$portfolio->name;

        if ($model->load($request->post())) {
            if(!empty($auctPerc)) {
                foreach($auctPerc as $auctPercItem => $auctPercValue) {
                    if($auctPercValue == 1) {
                        $keyPerc = $auctPercItem;
                        $keyPerc = substr($auctPercItem, 3);
                        if($keyPerc !== 'legal_percent') {
                            $keyPerc = $keyPerc.'_percent';
                        }
                    }
                }
                if($portfolio->is_legal_fix == 1) {
                    $fixPerc = $portfolio->legal_fix;
                }


            }
            if(!empty($keyPerc)) {
                if(isset($portfolio->min_step_percent)) { $step = $portfolio->min_step_percent; } else { $step = 1; }
                if(isset($portfolio->min_step_fix)) { $stepFix = $portfolio->min_step_fix; } else { $stepFix = 1; }
                $ms = $step;
                $ma = preg_replace("/\s+/", "", $model->amount);
                $ma = str_replace(',','.',$ma);
                $ma = (float) $ma;
                if($portfolio->soft_30_days_percent) {
                    $model->count_soft_30 = $portfolio->soft_30_days_percent-$ma;
                }
                if($portfolio->soft_90_days_percent) {
                    $model->count_soft_90 = $portfolio->soft_90_days_percent-$ma;
                }
                if($portfolio->soft_180_days_percent) {
                    $model->count_soft_180 = $portfolio->soft_180_days_percent-$ma;
                }
                if($portfolio->soft_365_days_percent) {
                    $model->count_soft_365 = $portfolio->soft_365_days_percent-$ma;
                }
                if($portfolio->soft_730_days_percent) {
                    $model->count_soft_730 = $portfolio->soft_730_days_percent-$ma;
                }
                if($portfolio->legal_fix) {
                    $model->count_legal_fix = $portfolio->legal_fix-$stepFix;
                }
                if($portfolio->legal_percent) {
                    $maxP = $portfolio->legal_percent;
                    $model->count_legal = $portfolio->legal_percent-$ma;
                } else {
                    $maxP = $portfolio->$keyPerc;
                }
                if($ma>$maxP) {

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => $titleSet,
                        'content' => '<span class="text-error">Внимание. Ставка не должна быть больше текущего значения!</span>',
                        'footer' => Html::button('ОК', ['class' => 'btn btn-default btn-block', 'data-dismiss' => "modal"])

                    ];
                }
                if($model->count_fix) {
                    $max = $portfolio->legal_fix;
                    $mfix = $model->count_fix;
                    if($mfix < 0) {
                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => $titleSet,
                            'content' => '<span class="text-error">Внимание. Ставка не должна быть меньше 0!</span>',
                            'footer' => Html::button('ОК', ['class' => 'btn btn-default btn-block', 'data-dismiss' => "modal"])

                        ];
                    }
                    if($mfix >= $max) {
                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => $titleSet,
                            'content' => '<span class="text-error">Внимание. Ставка не должна быть больше текущего значения!</span>',
                            'footer' => Html::button('ОК', ['class' => 'btn btn-default btn-block', 'data-dismiss' => "modal"])
                        ];
                    }
                } else {
                    $mfix = 2;
                    $stepFix = 0;
                }

                if($model->save()) {
                    if($ma<$ms || $mfix<$stepFix) {
                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => $titleSet,
                            'content' => '<span class="text-error">Внимание. Ставка не должна быть меньше минимального шага!</span>',
                            'footer' => Html::button('ОК', ['class' => 'btn btn-default btn-block', 'data-dismiss' => "modal"])

                        ];
                    }
                    $endDate = $model->created_at;
                    $g->setPortfolioSetRate($portfolio_id, $endDate);
                    $amountA = $model->amountA;
                    $amount_fixA = $model->amount_fixA;
                    $limit_percent = $model->limit_percent;
                    $limit_fix = $model->limit_fix;
                    if(empty($limit_fix)) {
                        $limit_fix = 0;
                    }
                    if(empty($amount_fixA)) {
                        $amount_fixA = 0;
                    }
                    $companyId = $portfolio->company_id;
                    $userId = \Yii::$app->user->identity->id;
                    if($limit_percent || $limit_fix) {
                        $modelRateAuto->setAutoRate($portfolio_id, $userId, $companyId, $amountA, $amount_fixA, $limit_percent, $limit_fix);
                    }
                    $g->setPercentAuct($portfolio_id, $ma);
                    if($model->count_fix !== $portfolio->legal_fix) {
                        $g->setPercentFix($portfolio_id, $mfix);
                    }

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => $titleSet,
                        'content' => '<span class="text-success">Внимание. Вы сделали ставку</span>',
                        'footer' => Html::button('ОК', ['class' => 'btn btn-default btn-block', 'data-dismiss' => "modal"])
                    ];
                }
            } else {
                $ms = $per['min_step']+$portfolio->currentRateAmount;
                $ma = preg_replace("/\s+/", "", $model->amount);
                $ma = (float)$ma;
                if($ma<$ms) {

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => $titleSet,
                        'content' => '<span class="text-error">Внимание. Ставка не должна быть меньше минимального шага!</span>',
                        'footer' => Html::button('ОК', ['class' => 'btn btn-default btn-block', 'data-dismiss' => "modal"])

                    ];
                } else {
                    if($model->save()) {
                        $endDate = $model->created_at;
                        $g->setPortfolioSetRate($portfolio_id, $endDate);


                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => $titleSet,
                            'content' => '<span class="text-success">Внимание. Вы сделали ставку</span>',
                            'footer' => Html::button('ОК', ['class' => 'btn btn-default btn-block', 'data-dismiss' => "modal"])

                        ];
                    }
                }
            }
        } else {
            if(!empty($per['min_step_fix'])||!empty($per['min_step_percent'])) {

                return [
                    'title' => $titleSet,
                    'content' => $this->renderAjax('set-rate', [
                        'model' => $model,
                        'modelRateAuto' => $modelRateAuto,
                        'portfolio' => $portfolio,
                        'per' => $per,
                        'auctPerc' => $auctPerc,
                        'searchModel' => $searchModel,
                        'rHistory' => $rHistory,
                        'rHistoryItemL' => $rHistoryItemL,
                        'getAutoRate' => $getAutoRate,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-default pull-left perc-btn', 'data-dismiss' => "modal"]) .
                        Html::button('Готово', ['class' => 'btn btn-primary perc-btn', 'type' => "submit"])

                ];
            } else {

                $autoRate = Yii::$app->user->identity->getPortfolioAutoRate($portfolio_id);

                if($autoRate) {
                    $onClick = '
                        event.preventDefault();
                        
                        var value = $("#portfoliorate-amount").attr("value");
                        
                        $("[data-perc=\"perc-auto-rate\"]").html(value+" ₽");
                        
                        var od = $("[data-perc=\"perc-od-td\"]").data("value");
                        var osz = $("[data-perc=\"perc-oz-td\"]").data("value");
                        // var amount = $("#portfoliorate-amount").attr("value");
                        
                        $.get("/portfolio/update-autorate?amount="+value+"&portfolio_id='.$portfolio_id.'&od="+od+"&osz="+osz, function(response){
                            alert("Ставка обновлена");
                        });
                    ';
                } else {
                    $onClick = '
                        event.preventDefault();
                        
                        var od = $("[data-perc=\"perc-od-td\"]").data("value");
                        var osz = $("[data-perc=\"perc-oz-td\"]").data("value");
                        var amount = $("#portfoliorate-amount").attr("value");
                        
                        $("[data-perc=\"perc-auto-rate\"]").html(amount+" ₽");
                        
                        $.get("/portfolio/toggle-autorate?portfolio_id='.$portfolio_id.'&toggle=1&od="+od+"&osz="+osz+"&rate="+amount+"&min_step='.$portfolio->min_step.'", function(response){
                            console.log(response);
                            if(response.result){
                                window.enableAutoRate();
                            }
                        });
                    ';
                }

                $disableBtn = false;

                if($autoRate == null){
                    $disableBtn = false;
                } else {
                    if($autoRate->status == AutoRate::STATUS_STOP_BY_LIMIT){
                        $disableBtn = false;
                    } else {
                        $disableBtn = true;
                    }
                }

                $autoRateBtns = Html::a('Выключить автоставку', '#',
                        ['id' => 'autorate-disable-btn','class' => $disableBtn ? 'btn btn-red pull-left' : 'btn btn-gray pull-left', 'onclick' => '
                        event.preventDefault();
                        
                        var od = $("[data-perc=\"perc-od-td\"]").data("value");
                        var osz = $("[data-perc=\"perc-oz-td\"]").data("value");
                        var amount = $("#portfoliorate-amount").attr("value");
                        
                        $.get("/portfolio/get-actual-rate?portfolio_id={$portfolio->id}", function(response){
                            $("#portfoliorate-amount").val(response.result);
                        });
                        
                        $.get("/portfolio/toggle-autorate?portfolio_id='.$portfolio_id.'&toggle=0&od="+od+"&rate="+amount+"&osz="+osz, function(response){
                            console.log(response);
                            
                            if(response.result){
                                window.disableAutoRate();
                            }
                        });
                        
                    ', 'style' => 'font-size: 13px;', 'disabled' => $disableBtn ? false : true]) .
                    Html::a($autoRate ? 'Обновить автоставку' : 'Включить автоставку', '#', ['id' => 'autorate-enable-btn', 'onclick' => $onClick, 'class' => 'btn btn-blue pull-left', 'style' => 'font-size: 13px;']);



                $makeRateBtn = '';

                if($autoRate == null){
                    $makeRateBtn = Html::a('Сделать ставку', '#', ['id' => 'make-rate-false-btn', 'class' => 'btn btn-gray pull-left', 'style' => 'font-size: 13px; display: none']).
                        Html::button('Сделать ставку', ['id' => 'make-rate-true-btn', 'type' => 'submit', 'class' => 'btn btn-blue pull-left', 'style' => 'font-size: 13px']);
                } else {
                    if($autoRate->status == AutoRate::STATUS_STOP_BY_LIMIT){
                        $makeRateBtn = Html::a('Сделать ставку', '#', ['id' => 'make-rate-false-btn', 'class' => 'btn btn-gray pull-left', 'style' => 'font-size: 13px; display: none']).
                            Html::button('Сделать ставку', ['id' => 'make-rate-true-btn', 'type' => 'submit', 'class' => 'btn btn-blue pull-left', 'style' => 'font-size: 13px']);
                    } else {
                        $makeRateBtn = Html::a('Сделать ставку', '#', ['id' => 'make-rate-false-btn', 'class' => 'btn btn-gray pull-left', 'style' => 'font-size: 13px;', 'disabled' => true]).
                            Html::button('Сделать ставку', ['id' => 'make-rate-true-btn', 'type' => 'submit', 'class' => 'btn btn-blue pull-left', 'style' => 'font-size: 13px; display: none;']);
                    }
                }

                $footer = Html::button('Отмена',
                        ['class' => 'btn btn-blue pull-left', 'data-dismiss' => "modal", 'style' => 'font-size: 13px;']) . $autoRateBtns .
                    $makeRateBtn;

                if($portfolio->status != Portfolio::STATUS_RATING){
                    $footer = Html::button('Отмена',
                            ['class' => 'btn btn-blue pull-left', 'data-dismiss' => "modal", 'style' => 'font-size: 13px;']) . $autoRateBtns .
                        Html::a('Сделать ставку', '#', ['class' => 'btn btn-gray pull-left', 'title' => 'Ставки по портфелю ещё не начались', 'style' => 'font-size: 13px;', 'disabled' => true]);
                }

                return [
                    'title' => $titleSet,
                    'content' => $this->renderAjax('set-rate', [
                        'autoRate' => $autoRate,
                        'model' => $model,
                        'modelRateAuto' => $modelRateAuto,
                        'portfolio' => $portfolio,
                        'per' => $per,
                        'auctPerc' => $auctPerc,
                        'searchModel' => $searchModel,
                        'rHistory' => $rHistory,
                        'rHistoryItemL' => $rHistoryItemL,
                    ]),
                    'footer' => $footer,

                ];
            }
        }
    }

    /**
     * @param int $portfolio_id
     * @param double $amount
     * @param double $od
     * @param double $osz
     * @return array
     */
    public function actionUpdateAutorate($portfolio_id, $amount, $od, $osz)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $amount = str_replace(' ', '', $amount);

        $portfolio = Portfolio::findOne($portfolio_id);

        /** @var AutoRate $autoRate */
        $autoRate = Yii::$app->user->identity->getPortfolioAutoRate($portfolio_id);

        $autoRate->rate = $amount;
        $autoRate->status = AutoRate::STATUS_ACTIVE;
        $autoRate->message = 0;
        $autoRate->save(false);

//        $od = $portfolio->sum_od;
//        $gsc = $portfolio->general_sum_credit;
//        $st = $autoRate->rate;
//        if((!empty($st))&&(!empty($od))&&(!empty($gsc))) {
//            $res = $st/$od*100;
//            $res = round($res, 2);
//            $st = round($st, 0);
//            $st1 = number_format(intval($st), 0, '', ' ');
//            $res = number_format(floatval($res), 2, ',', ' ');
//            $resOZ = $st/$gsc*100;
//            $resOZ = round($resOZ, 2);
////            $resOZ = number_format(floatval($resOZ), 2, ',', ' ');
//            //$st = number_format(floatval($st), 2, ',', ' ');
//        } else {
//            $res = '';
//            $resOZ = '';
//        }

        $log = new AutoRateLog([
            'portfolio_id' => $portfolio_id,
            'action' => AutoRateLog::ACTION_UPDATE,
            'company_id' => $autoRate->company_id,
            'user_id' => $autoRate->user_id,
            'amount' => $autoRate->rate,
            'od' => $od,
            'osz' => $osz,
            'datetime' => date('Y-m-d H:i:s'),
        ]);

        $log->save(false);

        return ['result' => true];
    }

    /**
     * @param int $portfolio_id
     * @param int $toggle
     * @param int $min_step
     * @param double $od
     * @param double $osz
     * @param double $rate
     * @return array
     */
    public function actionToggleAutorate($portfolio_id, $toggle, $min_step = null, $od = null, $osz = null, $rate = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if($rate != null){
            $rate = str_replace(' ', '', $rate);
        }

        $autoRate = Yii::$app->user->identity->getPortfolioAutoRate($portfolio_id);

        if($toggle == 1) {
            if($autoRate == null){

                $lastLog = AutoRateLog::find()->where(['portfolio_id' => $portfolio_id, 'user_id' => Yii::$app->user->identity->id])->orderBy('id desc')->one();

                if($lastLog){
                    $rate = $lastLog->amount;
                }


                $autoRate = new AutoRate([
                    'user_id' => Yii::$app->user->identity->id,
                    'company_id' => Yii::$app->user->identity->company_id,
                    'portfolio_id' => $portfolio_id,
                    'rate' => $rate
                ]);

                (new AutoRateLog([
                    'action' => AutoRateLog::ACTION_ON,
                    'portfolio_id' => $portfolio_id,
                    'company_id' => Yii::$app->user->identity->company_id,
                    'user_id' => Yii::$app->user->identity->id,
                    'od' => $od,
                    'osz' => $osz,
                    'amount' => $rate,
                    'datetime' => date('Y-m-d H:i:s'),
                ]))->save(false);

                return $autoRate->save(false) ? ['result' => true] : ['result' => false];
            }
        } else if($toggle == 0) {
            if($autoRate != null){
                $autoRate->delete();

                (new AutoRateLog([
                    'action' => AutoRateLog::ACTION_STOP_USER,
                    'portfolio_id' => $portfolio_id,
                    'company_id' => Yii::$app->user->identity->company_id,
                    'user_id' => Yii::$app->user->identity->id,
                    'od' => $od,
                    'osz' => $osz,
                    'amount' => $rate,
                    'datetime' => date('Y-m-d H:i:s'),
                ]))->save(false);

                return ['result' => true];
            }
        }

        return ['result' => false];
    }

    /**
     * @param int $id
     */
    public function actionRateLog($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $portfolio = $this->findModel($id);
        $searchModel = new AutoRateLogSearch();
        $dataProvider = $searchModel->search([]);
        $dataProvider->query->andWhere(['portfolio_id' => $id]);
        $dataProvider->pagination = false;

        return [
            'title' => "Лог активации Автоставок по портфелю №{$portfolio->id} {$portfolio->name}",
            'content' => $this->renderAjax('@app/views/auto-rate-log/index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]),
            'footer' => Html::button('ОК',
                ['class' => 'btn btn-default btn-block', 'data-dismiss' => "modal"]),
        ];
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDownloadFile($id)
    {
        $candidateFile = PortfolioFile::findOne($id);

        if ($candidateFile == null) {
            throw new NotFoundHttpException();
        }

        if (file_exists($candidateFile->path)) {
            Yii::$app->response->sendFile($candidateFile->path, $candidateFile->name);
        }
    }

    /**
     * @param null $portfolio_id
     * @return array|bool|null|UploadedFile
     * @throws \yii\base\Exception
     */
    public function actionUploadFile($portfolio_id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $fileName = Yii::$app->security->generateRandomString();
        if (is_dir('uploads') == false) {
            mkdir('uploads');
        }
        if (is_dir('uploads/data') == false) {
            mkdir('uploads/data');
        }
        $uploadPath = 'uploads/data';

        if (isset($_FILES['file'])) {
            $file = \yii\web\UploadedFile::getInstanceByName('file');
            $path = $uploadPath . '/' . $fileName . '.' . $file->extension;

            if ($file->saveAs($path)) {
                //Now save file data to database
                $portfolioFile = new PortfolioFile([
                    'portfolio_id' => $portfolio_id,
                    'name' => $_FILES['file']['name'],
                    'path' => $path
                ]);
                $portfolioFile->save(false);

                $file = (array)$file;
                $file['record'] = $portfolioFile;

                return $file;
            }
        }

        return false;
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUploadForStageFile($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        // $model->load($request->post());

        if ($model->validate()) {
            $fileAttribues = ['contract_creator', 'contract_winner', 'bill'];

            if (is_dir('uploads') == false) {
                mkdir('uploads');
            }

            foreach ($fileAttribues as $fileAttribute) {
                // if(isset($_FILES[$fileAttribute]) == false){
                // continue;
                // }
                $file = UploadedFile::getInstance($model, $fileAttribute);

                Yii::warning($file);

                if ($file == null) {
                    continue;
                }

                $model->$fileAttribute = $file;

                $name = Yii::$app->security->generateRandomString();
                $path = 'uploads/' . $name . '.' . $model->$fileAttribute->extension;
                $model->$fileAttribute->saveAs($path);
                $model->$fileAttribute = $path;

                if ($fileAttribute == 'contract_creator') {
                    $model->winner->sendEmailMessage("Продавец загрузил скан договора", "
                        <p>Доброго времени суток! </p>
                        <p>Продавец загрузил скан договора по портфелю №{$model->id} «{$model->name}»</p>
                    ", [$path => 'Скан договора']);
                }

                if ($fileAttribute == 'contract_winner') {
                    $model->company->sendEmailMessage("Покупатель загрузил скан договора", "
                        <p>Доброго времени суток! </p>
                        <p>Покупатель загрузил скан договора по портфелю №{$model->id} «{$model->name}»</p>
                    ", [$path => 'Скан договора']);
                }

                if ($fileAttribute == 'bill') {
                    $model->winner->sendEmailMessage("Продавец выставил счет", "
                        <p>Доброго времени суток! </p>
                        <p>Продавец выставил счет по портфелю №{$model->id} «{$model->name}»</p>
                    ", [$path => 'Счет']);
                }

                $model->save(false);
                break;
            }
        }

        return $model->errors;
    }

    /**
     * Получаем актуальную сумму ставки, то есть ту сумму, которая подойдет для новой ставки
     * @param int $portfolio_id
     * @return array
     */
    public function actionGetActualRate($portfolio_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var Portfolio $portfolio */
        $portfolio = $this->findModel($portfolio_id);
        /** @var PortfolioRate $lastRate */
        $lastRate = PortfolioRate::find()->where(['portfolio_id' => $portfolio_id])->orderBy('id desc')->one();

        if($lastRate){
            return ['result' => ($lastRate->amount+$portfolio->min_step)];
        } else {
            return ['result' => ($portfolio->start_rate+$portfolio->min_step)];
        }
    }

    /**
     * Updates an existing Portfolio model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Изменить портфель #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'forceClose' => true,
                    ];
                } else {
                    return [
                        'title' => "Изменить портфель #" . $id,
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Portfolio model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * @inheritdoc
     */
    /* public function beforeAction($action)
     {
         if (Yii::$app->user->isGuest == false) {
             if (Yii::$app->user->identity->isSuperAdmin() == false && Yii::$app->user->identity->company->moderated == 0) {
                 throw new ForbiddenHttpException('Ваша компания ещё не прошла модерацию');
             }
         }

         return parent::beforeAction($action);
     }*/

    /**
     * Delete multiple existing Portfolio model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Portfolio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Portfolio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Portfolio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }

    /**
     * Смена статуса у портфеля на STATUS_MODERATION
     * @param int $id Идентификатор портрфеля
     * @return array
     */
    public function actionToModeration($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = Portfolio::findOne($id);

        $model->status = $model::STATUS_MODERATION;
        $model->disapprove_text = null;
        $res = $model->getPortfolioPublishOrNot($id);

//		var_dump($res);
//		exit;

        if(empty($res)) {
            return [
                'forceReload' => '#crud-datatable-pjax',
                'title' => 'Публикация',
                'content' => '<span class="text-error">Для публикации портфеля заполните все обязательные поля!</span>',
                'footer' => Html::button('ОК', ['class' => 'btn btn-default btn-block', 'data-dismiss' => "modal"])

            ];
        }

        if (!$model->save()) {
            Yii::error($model->errors, 'error');
        }

        return [
            'forceReload' => '#crud-datatable-pjax',
            'forceClose' => true,
        ];
    }

    public function actionDisapproveText($id){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = Portfolio::findOne($id);

        return [
            'title' => 'Модерация не пройдена',
            'content' => '<span class="text-error">'.$model->disapprove_text.'</span>',
            'footer' => Html::button('ОК', ['class' => 'btn btn-default btn-block', 'data-dismiss' => "modal"])
        ];
    }

    public function actionViewLoans($id)
    {
        $portfolio = Portfolio::findOne($id) ?? null;

        if (!$portfolio) return $this->redirect('index');

        $searchModel = new LoanSearch();

        Yii::info(Yii::$app->request->queryParams, 'test');

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['portfolio_id' => $portfolio->id]);

        return $this->render('/loan/_index_portfolio', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'portfolio' => $portfolio
        ]);
    }

    public function actionMy()
    {
        Yii::info(Yii::$app->request->queryParams, 'test');

        $searchModel = new PortfolioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [
            'defaultOrder' => [
                'created_at' => SORT_DESC,
            ],
        ]);
        $dataProvider->query->andWhere(['stage' => null]);
        $dataProvider->query->orWhere(['status' => [Portfolio::STATUS_MODERATION, 6, Portfolio::STATUS_DONE_NO_DEAL]]);
        if (!Yii::$app->user->identity->isSuperAdmin())	{
            $dataProvider->query->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
        }
        $dataProvider->pagination->pageSize = 50;

        return $this->render('my', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}

