<?php

namespace app\controllers;

use app\models\AdminFiles;
use app\models\Portfolio;
use Yii;
use app\models\PortfolioSearch;
use yii\base\Controller;
use yii\filters\VerbFilter;

/**
 * Class PortfolioWonController
 * @package app\controllers
 */
class PortfolioWonController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if(\Yii::$app->user->isGuest == false) {
            $portfolio = new Portfolio();
            $messageWinner = $portfolio->getPortfolioWinner();

//			$winner = Portfolio::getRealTestWinner();
//			exit;
//
            $messageSetRate = $portfolio->getMessageNowRate();
            if(!empty($messageWinner)) {

//			    foreach($messageWinner as $portfolio){
//                    Yii::$app->session->addFlash('success', "Поздравляем!
//Вы победили в торгах на портфель №{$portfolio->id} {$portfolio->name} со ставкой ".Yii::$app->formatter->asCurrency($portfolio->currentRateAmount)." ({$portfolio->sum_od}% ОД, {$portfolio->general_sum_credit}% ОСЗ). В ближайшее время менеджер аукциона свяжется с Вами для помощи в оформлении сделки.»
//");
//                }

                $portfolio = $messageWinner[0];

                $od = $portfolio->sum_od;
                $gsc = $portfolio->general_sum_credit;
                $st = $portfolio->currentRateAmount;
                if((!empty($st))&&(!empty($od))&&(!empty($gsc))) {
                    $res = $st/$od*100;
                    $res = round($res, 2);
                    $st = round($st, 0);
                    $st1 = number_format(intval($st), 0, '', ' ');
                    $res = number_format(floatval($res), 2, ',', ' ');
                    $resOZ = $st/$gsc*100;
                    $resOZ = round($resOZ, 2);
                    $resOZ = number_format(floatval($resOZ), 2, ',', ' ');
                    //$st = number_format(floatval($st), 2, ',', ' ');
                } else {
                    $res = '';
                    $resOZ = '';
                }

                if($portfolio->winner_id == Yii::$app->user->identity->company_id){
                    Yii::$app->session->setFlash('success', "Вы победили в торгах на портфель №{$portfolio->id} {$portfolio->name} со ставкой ".number_format($portfolio->currentRateAmount, 0, 0, ' ')." ₽ ({$res}% ОД, {$resOZ}% ОСЗ). В ближайшее время менеджер аукциона свяжется с Вами для помощи в оформлении сделки.");
                } else {
                    Yii::$app->session->setFlash('warning', "Портфель №{$portfolio->id} \"{$portfolio->name}\" — победил участник со ставкой ".number_format($portfolio->currentRateAmount, 0, 0, ' ')." ₽ ({$res}% ОД, {$resOZ}% ОСЗ). Благодарим Вас за участие в торгах!");
                }
            }
            if(!empty($messageSetRate)) {
                $messageSetRate = $messageSetRate[0];

                $portfolio = Portfolio::findOne($messageSetRate->id);

                if($portfolio){
                    Yii::$app->session->setFlash('warning', "<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> Портфель №{$portfolio->id} \"{$portfolio->name}\" — Внимание! Вашу ставку перебили. Если вы намерены продолжать торги, Вам нужно сделать новую.");
                }
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * Lists all Portfolio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PortfolioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $adminFilesModel = AdminFiles::find()->one();
        if($adminFilesModel == null){
            $adminFilesModel = new AdminFiles();
        }

        if(Yii::$app->user->identity->isSuperAdmin()){
            $dataProvider->query
                ->andWhere(['status' => [Portfolio::STATUS_DONE]]);
        } else {
            $dataProvider->query
                ->andWhere(['status' => [Portfolio::STATUS_DONE]])
                ->andWhere(['or', ['winner_id' => Yii::$app->user->identity->company_id], ['company_id' => Yii::$app->user->identity->company_id]]);
        }

        return $this->render('@app/views/portfolio/index-won', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'adminFilesModel' => $adminFilesModel,
        ]);
    }
}