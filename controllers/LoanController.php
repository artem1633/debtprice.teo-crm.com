<?php

namespace app\controllers;

use app\models\Debtor;
use app\models\Portfolio;
use Yii;
use app\models\Loan;
use app\models\LoanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * LoanController implements the CRUD actions for Loan model.
 */
class LoanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Loan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LoanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere([
            'IS',
            'loan.portfolio_id',
            null
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Loan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $loan_model = $this->findModel($id);
        $debtor_model = Debtor::findOne($loan_model->debtor_id);

        return $this->render('view', [
            'loan_model' => $loan_model,
            'debtor_model' => $debtor_model,
        ]);
    }

    /**
     * Creates a new Loan model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate()
//    {
//        $request = Yii::$app->request;
//        $model = new Loan();
//
//        if ($request->isAjax) {
//            /*
//            *   Process for ajax request
//            */
//            Yii::$app->response->format = Response::FORMAT_JSON;
//            if ($request->isGet) {
//                return [
//                    'title' => "Создание дела",
//                    'size' => 'large',
//                    'content' => $this->renderAjax('create', [
//                        'model' => $model,
//                    ]),
//                    'footer' => Html::button('Закрыть',
//                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
//                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
//
//                ];
//            } else {
//                if ($model->load($request->post()) && $model->save()) {
//                    return [
//                        'forceReload' => '#crud-datatable-pjax',
//                        'title' => "Создание дела",
//                        'content' => '<span class="text-success">Дело создано успешно</span>',
//                        'footer' => Html::button('Закрыть',
//                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
//                            Html::a('Добавить еще', ['create'],
//                                ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
//
//                    ];
//                } else {
//                    return [
//                        'title' => "Создание дела",
//                        'size' => 'large',
//                        'content' => $this->renderAjax('create', [
//                            'model' => $model,
//                        ]),
//                        'footer' => Html::button('Закрыть',
//                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
//                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
//
//                    ];
//                }
//            }
//        } else {
//            /*
//            *   Process for non-ajax request
//            */
//            if ($model->load($request->post()) && $model->save()) {
//                return $this->redirect(['view', 'id' => $model->id]);
//            } else {
//                return $this->render('create', [
//                    'model' => $model,
//                ]);
//            }
//        }
//
//    }

    /**
     * Создает должника и дело
     * @return string|Response
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $loan_model = new Loan();
        $debtor_model = new Debtor();

        if ($request->isGet) {
            return $this->render('create', [
                'loan_model' => $loan_model,
                'debtor_model' => $debtor_model,
            ]);
        } else {
            if ($loan_model->load($request->post()) && $debtor_model->load($request->post())) {

                $transaction = Yii::$app->db->beginTransaction();
                if (!$debtor_model->save()) {
                    Yii::error($debtor_model->errors, 'error');
                    return $this->render('create', [
                        'loan_model' => $loan_model,
                        'debtor_model' => $debtor_model,
                    ]);
                } else {
                    //Сохраняем дело
                    $loan_model->debtor_id = $debtor_model->id;
                    if (!$loan_model->save()) {
                        Yii::error($loan_model->errors, 'error');
                        $transaction->rollBack();
                        return $this->render('create', [
                            'loan_model' => $loan_model,
                            'debtor_model' => $debtor_model,
                        ]);
                    } else {
                        $transaction->commit();
                    }
                }
            } else {
                return $this->render('create', [
                    'loan_model' => $loan_model,
                    'debtor_model' => $debtor_model,
                ]);
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Updates an existing Loan model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
//    public function actionUpdate($id)
//    {
//        $request = Yii::$app->request;
//        $model = $this->findModel($id);
//
//        if ($request->isAjax) {
//            /*
//            *   Process for ajax request
//            */
//            Yii::$app->response->format = Response::FORMAT_JSON;
//            if ($request->isGet) {
//                return [
//                    'title' => "Редактирование дела #" . $id,
//                    'size' => 'large',
//                    'content' => $this->renderAjax('update', [
//                        'model' => $model,
//                    ]),
//                    'footer' => Html::button('Закрыть',
//                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
//                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
//                ];
//            } else {
//                if ($model->load($request->post()) && $model->save()) {
//                    return [
//                        'forceReload' => '#crud-datatable-pjax',
//                        'title' => "Дело #" . $id,
//                        'content' => $this->renderAjax('view', [
//                            'model' => $model,
//                        ]),
//                        'footer' => Html::button('Закрыть',
//                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
//                            Html::a('Редактировать', ['update', 'id' => $id],
//                                ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
//                    ];
//                } else {
//                    return [
//                        'title' => "Update Loan #" . $id,
//                        'size' => 'large',
//                        'content' => $this->renderAjax('update', [
//                            'model' => $model,
//                        ]),
//                        'footer' => Html::button('Закрыть',
//                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
//                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
//                    ];
//                }
//            }
//        } else {
//            /*
//            *   Process for non-ajax request
//            */
//            if ($model->load($request->post()) && $model->save()) {
//                return $this->redirect(['view', 'id' => $model->id]);
//            } else {
//                return $this->render('update', [
//                    'model' => $model,
//                ]);
//            }
//        }
//    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $loan_model = $this->findModel($id) ?? null;
        $debtor_model = Debtor::findOne($loan_model->debtor_id) ?? null;

        if ($loan_model->load($request->post()) && $loan_model->save() && $debtor_model->load($request->post()) && $debtor_model->save()) {
            return $this->redirect(['view', 'id' => $loan_model->id]);
        } else {
            return $this->render('update', [
                'loan_model' => $loan_model,
                'debtor_model' => $debtor_model,
            ]);
        }
    }

    /**
     * Delete an existing Loan model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Loan model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Loan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Loan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Loan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Создает портфель и добавляет выбранные дела (кредиты) в созданный портфель
     * @return array
     */
    public function actionBulkCreatePortfolio()
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;


        if ($request->isPost) {
            $pks = explode(',', $request->post('pks'));

            $portfolio = new Portfolio([
                'name' => 'Новый портфель ' . date('d.m.Y', time()),
            ]);

            $portfolio->save();

            foreach ($pks as $id) {
                $model = Loan::findOne($id) ?? null;

                if (!$model) {
                    continue;
                }

                $model->portfolio_id = $portfolio->id;

                if (!$model->save()) {
                    Yii::error($model->errors, 'error');
                }
            }
        }
        return [
            'forceReload' => '#crud-datatable-pjax',
            'forceClose' => true,
        ];

    }

    /**
     * Добавляет выбранные дела в вбранный же портфель
     * @return array
     */
    public function actionBulkAddToPortfolio()
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new Loan();

        if ($request->isPost) {
            $model->pks = $request->post('pks');

            if ($model->load($request->post())) {
                //Портфель выбран

                foreach (explode(',', $model->pks) as $loan_id) {
                    $loan = Loan::findOne($loan_id);
                    $loan->portfolio_id = $model->portfolio_id;

                    if (!$loan->save()) {
                        Yii::error($loan->errors, 'error');
                    }
                }

                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'forceClose' => true,
                ];
            }
            //Портфель еще не выбран
            return [
                'title' => 'Выбор портфеля',
                'content' => $this->renderAjax('_select_portfolio_form', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Закрыть',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button('Выбрать', ['class' => 'btn btn-primary', 'type' => "submit"])
            ];

        }

        return [
            'forceReload' => '#crud-datatable-pjax',
            'forceClose' => true,
        ];
    }

    /**
     * Исключает дело из портфеля
     * @param int $id Дела
     * @return array
     */
    public function actionExcludeLoan($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = Loan::findOne($id) ?? null;

        if (!$model) {
            return [
                'title' => 'Исключение дела из портяеля',
                'content' => 'Ошибка. Дело не найдено'
            ];
        }

        $model->portfolio_id = null;
        if (!$model->save()) {
            Yii::error($model->save(), 'error');
        }

        return [
            'forceReload' => '#crud-datatable-pjax',
            'forceClose' => true,
        ];
    }

}
