<?php

namespace app\assets\plugins;

use yii\web\AssetBundle;

/**
 * Class D3Asset
 * @package app\plugins\assets
 */
class TooltipsterAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

    public $css = [
        'libs/tooltipster/tooltipster.bundle.min.css',
        'libs/tooltipster/themes/tooltipster-sideTip-borderless.min.css',
        'libs/tooltipster/themes/tooltipster-sideTip-light.min.css',
        'libs/tooltipster/themes/tooltipster-sideTip-noir.min.css',
        'libs/tooltipster/themes/tooltipster-sideTip-punk.min.css',
        'libs/tooltipster/themes/tooltipster-sideTip-shadow.min.css',
    ];
    public $js = [
        'libs/tooltipster/tooltipster.bundle.min.js',
    ];
    public $depends = [
        'app\assets\AppAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
