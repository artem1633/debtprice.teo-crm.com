<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Portfolio */

$this->title = "Редактировать портфель «{$model->name}»";
if(isset($model->min_step)||$model->type == 1) { ?>
	<?= $this->render('_form', [
                'model' => $model,
            ]) ?>
<?php } else {
?>
            <?= $this->render('_form_perc', [
                'model' => $model,
            ]) ?>
<?php } ?>