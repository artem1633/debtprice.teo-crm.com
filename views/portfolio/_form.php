<script type="text/javascript"> 

$(function(){ 
	/*setInterval(function() {
		$('.field-portfolio-start_rate input').each(function() {
			var sr = $(this).val();
			if(sr.length>0) {
				var srv = parseFloat(sr);
				srv = Math.round(srv);
				$('.field-portfolio-start_rate input').val(srv);
			}
		});
	}, 500);*/
	
});
	
</script>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Portfolio;
use kartik\select2\Select2;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use app\models\Company;
use app\widgets\NumericInput;

/* @var $this yii\web\View */
/* @var $model app\models\Portfolio */
/* @var $form yii\widgets\ActiveForm */

\app\assets\plugins\AutoNumericAsset::register($this);


?>

<div class="portfolio-form">

    <?php $form = ActiveForm::begin(); ?>
	<div class="row" hidden="hidden">
		<?= $form->field($model, 'type')->textInput(['value'=>1]) ?>
	</div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 required-field">
            <?= $form->field($model, 'count_actions')->textInput(['data-widget' => 'autonumeric'])?><span class="req">*</span>
        </div>
        <div class="col-md-3 required-field">
            <?= $form->field($model, 'sum_od')->textInput(['data-widget' => 'autonumeric'])?><span class="req">*</span>
        </div>
        <div class="col-md-3 required-field">
            <?= $form->field($model, 'sum_percents')->textInput(['data-widget' => 'autonumeric'])?><span class="req">*</span>
        </div>
        <div class="col-md-3 required-field">
            <?= $form->field($model, 'general_sum_credit')->textInput(['data-widget' => 'autonumeric'])?><span class="req">*</span>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'count_ip')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'count_il')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'is_pledge')->checkbox() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'general_payments')->textInput(['data-widget' => 'autonumeric']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'count_action_before_90_days')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'count_action_90_180_days')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'count_action_181_365_days')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'count_action_366_730_days')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'count_action_731_1095_days')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'count_action_over_1096_days')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'product')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6 required-field">
            <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?><span class="req">*</span>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'pledge_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'count_placements_ka')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'company_id')->dropDownList(ArrayHelper::map(Company::find()->all(), 'id', 'name'), ['value' => Yii::$app->user->identity->company_id, 'disabled' => true])->label('Продавец') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'creditor')->textInput()->label('Кредитор <span class="req" style="color: red !important; font-size: 13px;">*</span>') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'creditor_saler_is_same')->checkbox() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model,'hide_saler')->checkbox() ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6 required-field">
            <?= $form->field($model, 'datetime_start')->widget(DateTimePicker::className(), [

            ]) ?><span class="req">*</span>
        </div>
        <div class="col-md-6 required-field">
            <?= $form->field($model, 'datetime_end')->widget(DateTimePicker::className(), [

            ]) ?><span class="req">*</span>
        </div>
    </div>
	<div class="row">
		<div class="col-md-6 required-field">
			<?= $form->field($model, 'start_rate')->textInput(['data-widget' => 'autonumeric']) ?><span class="req">*</span>
		</div>
	</div>
    <div class="row">
        <div class="col-md-6 required-field">
            <?= $form->field($model, 'min_step')->textInput(['data-widget' => 'autonumeric']) ?><span class="req">*</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php if($model->isNewRecord): ?>
                <?= \kato\DropZone::widget([
                    'id'        => 'dzImage', // <-- уникальные id
                    'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file' ]),
                    'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                    'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                    'options' => [
                        'autoDiscover' => false,
                        'maxFilesize' => '50',
                    ],
                    'clientEvents' => [
                        'success' => "function(file, response){
                                console.log(response);

                                var id = response.record.id;

                                var value = $('#portfolio-files').val();
                                $('#portfolio-files').val(value+','+id);
                             }",
//                        'complete' => "function(file){ console.log(file); }",
                    ],
                ]);?>
            <?php else: ?>
                <?= \kato\DropZone::widget([
                    'id'        => 'dzImage', // <-- уникальные id
                    'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file', 'portfolio_id' => $model->id]),
                    'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                    'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                    'options' => [
                        'maxFilesize' => '50',
                    ],
                ]);?>
            <?php endif; ?>
        </div>
    </div>



    <div class="hidden">
        <?= $form->field($model, 'files')->hiddenInput() ?>
    </div>


    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>


<?php

$companyId = Yii::$app->user->identity->company_id;
$company = Company::findOne($companyId);

$companyName = '';

if($company) {
    $companyName = $company->name;
}

$script = <<< JS
// $('[data-widget="autonumeric"]').autoNumeric('init', {
//     allowDecimalPadding: false,
//     digitGroupSeparator: " "
// });

$('#portfolio-creditor_saler_is_same').change(function(){
    if($(this).is(':checked')){
        $('#portfolio-creditor').val('{$companyName}');
        $('#portfolio-creditor').attr('readonly', true);
    } else {
        $('#portfolio-creditor').removeAttr('readonly');
    }
});

$('#portfolio-creditor_saler_is_same').trigger('change');

$('[data-widget="autonumeric"]').each(function(){
   new AutoNumeric(this, {
       allowDecimalPadding: false,
       digitGroupSeparator: " ",
       modifyValueOnWheel: false,
   }); 
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
