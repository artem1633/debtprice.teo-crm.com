<?php

use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use app\models\Portfolio;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Portfolio */

$this->title = "Портфель «{$model->name}»";

?>
<style>
    .detail-view th {
        text-align: left;
    }
</style>
<script type="text/javascript">
	$(function(){
		$('.setka-price').each(function() {
			$(this).siblings().children('tbody').children().children('th').each(function() {
				if($(this).text() == 'Кол-во дел до 90 дней') {
					$(this).parent().hide();
				}
				if($(this).text() == 'Кол-во дел 90-180 дней') {
					$(this).parent().hide();
				}
				if($(this).text() == 'Кол-во дел 181-365 дней') {
					$(this).parent().hide();
				}
				if($(this).text() == 'Кол-во дел 366-730 дней') {
					$(this).parent().hide();
				}
				if($(this).text() == 'Кол-во ИЛ') {
					$(this).parent().hide();
				}
				if($(this).text() == 'Начальная ставка') {
					$(this).parent().hide();
				}
			});
		});
	});
</script>
<div class="portfolio-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'creditor',
                'label' => 'Кредитор',
            ],
            [
                'attribute' => 'company_id',
                'label' => 'Продавец',
                'value' => function($model){
                    if($model->hide_saler == 1 && Yii::$app->user->identity->isSuperAdmin() == false){
                        return 'DEBTPRICE';
                    } else if($model->hide_saler == 1 && Yii::$app->user->identity->isSuperAdmin()){
                        $company = \app\models\Company::findOne($model->company_id);

                        if($company){
                            return "DEBTPRICE ({$company->name})";
                        }

                    }

                    $company = \app\models\Company::findOne($model->company_id);

                    if($company){
                        return $company->name;
                    }
                },
            ],
            'name',
            [
                'attribute' => 'count_actions',
                'label' => 'Заемщики',
                'format' => 'integer',
            ],
            [
                'attribute' => 'sum_od',
                'label' => 'ОД',
                // 'format' => ['currency', 'rub'],
                'value' => function($model){
                    if($model->sum_od > 0){
                        return number_format($model->sum_od, 0, 0, ' ').' ₽';
                    }
                },
            ],
            [
                'attribute' => 'sum_percents',
                'label' => 'Проценты',
                'value' => function($model){
                    if($model->sum_percents > 0){
                        return number_format($model->sum_percents, 0, 0, ' ').' ₽';
                    }
                },
            ],
            [
                'attribute' => 'general_sum_credit',
                'value' => function($model){
                    if($model->general_sum_credit > 0){
                        return number_format($model->general_sum_credit, 0, 0, ' ').' ₽';
                    }
                },
            ],
            [
                'attribute' => 'count_ip',
                'format' => 'integer',
            ],
            [
                'attribute' => 'count_il',
                'format' => 'integer',
            ],
            'is_pledge',
            [
                'attribute' => 'general_payments',
                'value' => function($model){
                    if($model->general_payments > 0){
                        return number_format($model->general_payments, 0, 0, ' ').' ₽';
                    }
                },
            ],
            [
                'attribute' => 'count_action_before_90_days',
                'format' => 'integer',
            ],
            [
                'attribute' => 'count_action_90_180_days',
                'format' => 'integer',
            ],
            [
                'attribute' => 'count_action_181_365_days',
                'format' => 'integer',
            ],
            [
                'attribute' => 'count_action_366_730_days',
                'format' => 'integer',
            ],
            [
                'attribute' => 'count_action_731_1095_days',
                'format' => 'integer',
            ],
            [
                'attribute' => 'count_action_over_1096_days',
                'format' => 'integer',
            ],
            'product',
            'location',
            'pledge_name',
            [
                'attribute' => 'count_placements_ka',
                'format' => 'integer',
            ],
            [
                'attribute' => 'status',
                'value' => function($model){
                    return ArrayHelper::getValue(Portfolio::statusLabels(), $model->status);
                }
            ],
//            [
//                'attribute' => 'datetime_start',
//                'format' => ['date', 'php:d M Y H:i'],
//            ],
//            [
//                'attribute' => 'datetime_end',
//                'format' => ['date', 'php:d M Y H:i'],
//            ],
            [
                'attribute' => 'datetime_start',
                'label' => 'Время торгов',
                'format' => 'raw',
                'value' => function($model){
                    $od = $model->datetime_start;
                    $od1 = $model->datetime_end;
                    $_monthsList = array("Jan" => "янв.", "Feb" => "фев.",
                        "Mar" => "мар.", "Apr" => "апр.", "May" => "мая", "Jun" => "июн.",
                        "Jul" => "июл.", "Aug" => "авг.", "Sep" => "сен.",
                        "Oct" => "окт.", "Nov" => "ноя.", "Dec" => "дек.");
                    $currentDate = date("d M Y H:i", strtotime($od));
                    $currentDate1 = date("d M Y H:i", strtotime($od1));

                    $_mD = date("M", strtotime($od));
                    $_mD1 = date("M", strtotime($od1));
                    $date = date("d M Y", strtotime($od));
                    $date1 = date("d M Y", strtotime($od1));
                    $time = date("H:i", strtotime($od));
                    $time1 = date("H:i", strtotime($od1));

                    if($date == $date1) {
                        $date = str_replace($_mD, " ".$_monthsList[$_mD]." ", $date);
                        $currentDate = $date.'<br/><span class="time-s">'.$time.'-'.$time1.'</span>';

                        return $currentDate;
                    } else {
                        $date = str_replace($_mD, " ".$_monthsList[$_mD]." ", $date);
                        $date1 = str_replace($_mD1, " ".$_monthsList[$_mD1]." ", $date1);
                        //$currentDate1 = str_replace($_mD1, " ".$_monthsList[$_mD1]." ", $currentDate1);

                        return $date.'<br/><span class="time-s">'.$time.'-'.$time1.'</span><br/>'.$date1;
                    }
                },
            ],
            [
                'attribute' => 'winner.name',
                'label' => 'Победитель',
                'visible' => Yii::$app->user->identity->isSuperAdmin(),
            ],
            [
                'attribute' => 'start_rate',
                // 'format' => ['currency', 'rub'],
                'value' => function($model){
                    if($model->start_rate > 0){
                        return number_format($model->start_rate, 0, 0, ' ').' ₽';
                    }
                },
            ],
            [
                'attribute' => 'min_step',
                // 'format' => ['currency', 'rub'],
                'value' => function($model){
                    if($model->min_step > 0){
                        return number_format($model->min_step, 0, 0, ' ').' ₽';
                    }
                },
            ],
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function($model){
                    $date = Yii::$app->formatter->asDate($model->created_at, 'php:d M Y');
                    $time = Yii::$app->formatter->asDate($model->created_at, 'php:H:i:s');

                    return $date.'<br/><span class="time-s">'.$time.'</span>';
                },
            ],
            [
                'attribute' => 'legal_percent',
                'format' => 'decimal',
                'visible' => ($model->is_soft_30_days || $model->is_soft_90_days || $model->is_soft_180_days || $model->is_soft_365_days || $model->is_soft_730_days || $model->is_legal_percent || $model->is_legal_fix),
            ],
            [
                'attribute' => 'legal_fix',
                'format' => 'integer',
                'visible' => ($model->is_soft_30_days || $model->is_soft_90_days || $model->is_soft_180_days || $model->is_soft_365_days || $model->is_soft_730_days || $model->is_legal_percent || $model->is_legal_fix),
            ],
        ],
    ]) ?>
	<?php if($model->is_soft_30_days || $model->is_soft_90_days || $model->is_soft_180_days || $model->is_soft_365_days || $model->is_soft_730_days || $model->is_legal_percent || $model->is_legal_fix) {?>
		<h4 style="text-align: center; width: 600px;">Сетка вознаграждений</h4>
		<table class="table table-striped table-bordered detail-view setka-price" style="width: 600px;">
			<thead>
				<tr>
					<td style="width: 50%;">Период просрочки (дней)</td><td style="width: 50%;">Вознаграждение %</td>
				</tr>
			</thead>
			<tbody>
				<?php if($model->is_soft_30_days) { ?>
					<tr style="height: 40px;">
						<td><?php echo $model->soft_30_days; ?></td>
						<td><?php echo $model->soft_30_days_percent; ?></td>
					</tr>
				<?php } ?>
				<?php if($model->is_soft_90_days) { ?>
					<tr style="height: 40px;">
						<td><?php echo $model->soft_90_days; ?></td>
						<td><?php echo $model->soft_90_days_percent; ?></td>
					</tr>
				<?php } ?>
				<?php if($model->is_soft_180_days) { ?>
					<tr style="height: 40px;">
						<td><?php echo $model->soft_180_days; ?></td>
						<td><?php echo $model->soft_180_days_percent; ?></td>
					</tr>
				<?php } ?>
				<?php if($model->is_soft_365_days) { ?>
					<tr style="height: 40px;">
						<td><?php echo $model->soft_365_days; ?></td>
						<td><?php echo $model->soft_365_days_percent ?></td>
					</tr>
				<?php } ?>
				<?php if($model->is_soft_730_days) { ?>
					<tr style="height: 40px;">
						<td><?php echo $model->soft_730_days ?></td>
						<td><?php echo $model->soft_730_days_percent; ?></td>
					</tr>
				<?php } ?>
				<?php if($model->is_legal_percent) { ?>
					<tr style="height: 40px;">
						<td>Legal collection %</td>
						<td><?php echo $model->legal_percent; ?></td>
					</tr>
				<?php } ?>
				<?php if($model->is_legal_fix) { ?>
					<tr style="height: 40px;">
						<td>Legal collection (fix, руб)</td>
						<td><?php echo $model->legal_fix; ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	<?php } ?>

    <h3>Файлы</h3>
    <?= $this->render('@app/views/portfolio-file/index', [
        'searchModel' => $filesSearchModel,
        'dataProvider' => $filesDataProvider,
        'portfolioId' => $model->id
    ]) ?>
</div>
