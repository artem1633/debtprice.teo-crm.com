<?php

/** @var $model \app\models\forms\DisapprovePortfolioText */

use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin() ?>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'disapproveText')->textarea() ?>
    </div>
</div>

<?php ActiveForm::end() ?>


