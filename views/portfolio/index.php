<?php
use app\models\Portfolio;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PortfolioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Портфели";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

<?php if (Yii::$app->user->identity->isSuperAdmin() == true) {?>
    <div class="admin-true" hidden="hidden">admin</div>
<?php }
?>

<div class="panel panel-inverse portfolio-index">
    <div class="panel-heading">
        <!--        <div class="panel-heading-btn">-->
        <!--        </div>-->
        <h4 class="panel-title">ТОРГИ</h4>
    </div>
    <div class="panel-body">
        <?php
        // Pjax::begin(['id' => 'rate-io-pjax', 'enablePushState' => false, 'timeout' => 15000])
        ?>
        <div id="ajaxCrudDatatable">
            <?php
            if (Yii::$app->user->identity->isSuperAdmin() == false) {
                try {
                    echo GridView::widget([
                        'id' => 'crud-datatable',
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'pjax' => true,
                        'responsiveWrap' => false,
                        'rowOptions' => function($model){
                            if($model->status == Portfolio::STATUS_RATING){
                                return ['class' => 'row-rating'];
                            }
                        },
                        'emptyText' => 'Кредитные портфели готовятся к публикации.',
                        'columns' => require(__DIR__ . '/_columns.php'),
                        'panelBeforeTemplate' =>

                            Html::a('Цессия <i class="fa fa-plus"></i>', ['create'],
                                [
                                    'role' => 'modal-remote',
                                    'title' => 'Цессия',
                                    'class' => 'btn btn-success'
                                ]) . '&nbsp;' .
                            Html::a('Агентский договор <i class="fa fa-plus"></i>', ['create-perc'],
                                [
                                    'role' => 'modal-remote',
                                    'title' => 'Агентский договор',
                                    'class' => 'btn btn-success',
                                    'style' => 'display: none;',
                                ]) . '&nbsp;' .
                            Html::a('<i class="fa fa-repeat"></i>', [''],
                                ['data-pjax' => 1, 'class' => 'btn btn-white', 'title' => 'Обновить']),

                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'headingOptions' => ['style' => 'display: none;'],


                        ]
                    ]);
                } catch (Exception $e) {
                    Yii::error($e->getMessage(), 'error');
                    echo $e->getMessage();
                }
            } else {

                try {
                    echo GridView::widget([
                        'id' => 'crud-datatable',
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'pjax' => true,
                        'responsiveWrap' => false,
                        'rowOptions' => function($model){
                            if($model->status == Portfolio::STATUS_RATING){
                                return ['class' => 'row-rating'];
                            }
                        },
                        'emptyText' => 'Кредитные портфели готовятся к публикации.',
                        'columns' => require(__DIR__ . '/_columns.php'),
                        'panelBeforeTemplate' =>

                            Html::a('Цессия <i class="fa fa-plus"></i>', ['create'],
                                [
                                    'role' => 'modal-remote',
                                    'title' => 'Цессия',
                                    'class' => 'btn btn-success'
                                ]) . '&nbsp;' .
                            Html::a('Агентский договор <i class="fa fa-plus"></i>', ['create-perc'],
                                [
                                    'role' => 'modal-remote',
                                    'title' => 'Агентский договор',
                                    'class' => 'btn btn-success',
                                    'style' => 'display: none;',
                                ]) . '&nbsp;' .
                            Html::a('<i class="fa fa-repeat"></i>', [''],
                                ['data-pjax' => 1, 'class' => 'btn btn-white', 'title' => 'Обновить']),

                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'headingOptions' => ['style' => 'display: none;'],
                            'after' => BulkButtonWidget::widget([
                                    'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                                        ["bulk-delete"],
                                        [
                                            "class" => "btn btn-danger btn-xs",
                                            'role' => 'modal-remote-bulk',
                                            'data-confirm' => false,
                                            'data-method' => false,// for overide yii data api
                                            'data-request-method' => 'post',
                                            'data-confirm-title' => 'Вы уверены?',
                                            'data-confirm-message' => 'Вы действительно хотите удалить данный элемент?'
                                        ]),
                                ]) .
                                '<div class="clearfix"></div>',
                        ]
                    ]);
                } catch (Exception $e) {
                    Yii::error($e->getMessage(), 'error');
                    echo $e->getMessage();
                }
            }?>
        </div>
        <?php
        // Pjax::end()
        ?>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
//    'options' => ['class' => 'fade modal-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php

$script = <<< JS
$('#ajaxCrudModal').on('hidden.bs.modal', function () {
    if($(this).hasClass("modal-slg")){
        $(this).removeClass("modal-slg");        
    }
    $.pjax.reload('#report-messages-pjax');
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>
