<?php
use app\widgets\NumericInput;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PortfolioRate */
/* @var $portfolio app\models\Portfolio */
/* @var $form yii\widgets\ActiveForm */
?>
<script type="text/javascript"> 

$(function(){ 
	$("#w1").keyup(function() { 
		var setRate = $(this).val();
		setRate = setRate.replace(/\s+/g,'');
		var od = $('.sum-od-h').val();
		var oz = $('.sum-od-g').val();
		var resOd = parseFloat(setRate)/parseFloat(od)*100;
		var resOz = parseFloat(setRate)/parseFloat(oz)*100;
		resOd = resOd.toFixed(2);
		resOz = resOz.toFixed(2);
		$('.perc-od span').text('Процент от ОД: '+resOd+' %');
		$('.perc-oz span').text('Процент от ОЗ: '+resOz+' %');
	}) 
});

</script>
<div class="portfolio-form portfolio-form-set">
	<?php
		if(!empty($auctPerc)) {
			foreach($auctPerc as $auctPercItem => $auctPercValue) {
				if($auctPercValue == 1) {
					$keyPerc = $auctPercItem;
					$keyPerc = substr($auctPercItem, 3);
					if($keyPerc !== 'legal_percent') {
						$keyPerc = $keyPerc.'_percent';
					}
				}
			}
			if($portfolio->is_legal_fix == 1) {
				$fixPerc = $portfolio->legal_fix;
			}
	    }
	?>
	<?php if(!empty($keyPerc) || $portfolio->is_legal_fix == 1) {?>
	<div class="row">
		<div class="col-md-4">
			<?php $form = ActiveForm::begin(); ?>
			<table class="auto-rate soft_p" style=" width: 100%;">
				<thead>
					<tr>
						<td style="width: 33%;"></td><td style="width: 33%; text-align: center;">Шаг</td><td style="width: 33%; text-align: center;">Лимит</td>
					</tr>
				</thead>
				<?php if(isset($per['min_step_percent'])) { $step = $per['min_step_percent']; } else { $step = 0; }?>
				<?php if(isset($per['min_step_fix'])) { $stepFix = $per['min_step_fix']; } else { $stepFix = 0; }?>
				<tbody>
					<tr>
						<td>%</td><td><?= $form->field($modelAuto, 'amount')->textInput(['value'=>$step]) ?></td><td><?= $form->field($modelAuto, 'limit_percent')->textInput() ?></td>
					</tr>
					<tr>
						<td>FIX</td><td><?= $form->field($modelAuto, 'amount_fix')->textInput(['value'=>$stepFix]) ?></td><td><?= $form->field($modelAuto, 'limit_fix')->textInput() ?></td>
					</tr>
					<tr>
						<td colspan="3"><?= Html::submitButton('Автоставка', ['class' => 'btn btn-default btn-block', 'name' => 'rate-button']) ?></td>
					</tr>	
				</tbody>
			</table>
			<?php ActiveForm::end(); ?>
		</div>
		<div class="col-md-4">
			<table class="auto-rate soft_p" style=" width: 100%;">
				<?php 
					if(isset($per['min_step_percent'])) { 
						$step = $per['min_step_percent']; 
						$labelStep = '% -';
					} else {
						$step = $per['min_step']; 
						$labelStep = 'Шаг ставки: ';
					}
					if(isset($per['min_step_fix']) && ($portfolio->is_legal_fix == 1)) { 
						$stepFix = $per['min_step_fix']; 
						$labelStepFix = 'FIX - ';
					}
				?>
				<tbody>
					<tr>
						<td>Шаг ставки (min):</td>
					</tr>
					<tr>
						<td><?php echo $labelStep.$step.' %'; ?></td>
					</tr>
					<?php if(isset($per['min_step_fix']) && ($portfolio->is_legal_fix == 1)) { ?>
					<tr>
						<td><?php echo $labelStepFix.''.$stepFix.' ₽'; ?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<div class="col-md-4">
			<table class="auto-rate-set soft_p" style=" width: 100%;">
				<tbody>
					<tr><td colspan="2">Новая ставка</td></tr>
					<?php $form = ActiveForm::begin(); ?>
	
					<?php if(!empty($keyPerc) || $portfolio->is_legal_fix == 1) {?>
					<tr><td style=" width: 30%;">%</td>
						<td>
						<?php if(isset($per['min_step_percent'])) { $step = $per['min_step_percent']; } else { $step = 1; }?>
						<?= $form->field($model, 'amount')->widget(NumericInput::class, [
							'pluginOptions' => [
								'data-a-dec' => ',',
								'data-a-sep' => ' ',
								'value' => $step,
							],
						]) ?>
						</td>
					</tr>
					<?php if($portfolio->is_legal_fix == 1) { ?>
					<?php if(isset($per['min_step_fix'])) { $step = $per['min_step_fix']; } else { $step = 1; }?>
					<tr><td>FIX</td>
						<td>
						<?= $form->field($model, 'count_fix')->textInput(['value' => $step]) ?>	
						</td>
					</tr>
					<?php } ?>
				<?php } ?>
					<tr><td colspan="2"><?= Html::submitButton('Сделать ставку', ['class' => 'btn btn-default btn-block', 'name' => 'rate-button']) ?></td></tr>
    			<?php ActiveForm::end(); ?>
				</tbody>
			</table>
		</div>
	</div>
	<br/>
	<?php } ?>
	<?php if(empty($keyPerc) && $portfolio->is_legal_fix !== 1) {?>
    <?php $form = ActiveForm::begin(); ?>
		<div class="row">
			<div class="col-md-12">
				<?php if(isset($per['min_step'])) { $step = $per['min_step']; } else { $step = 1; }?>
				<?= $form->field($model, 'amount')->widget(NumericInput::class, [
					'pluginOptions' => [
						'data-a-dec' => ',',
						'data-a-sep' => ' ',
						'value' => $portfolio->currentRateAmount+$per['min_step'],
					],
				]) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 perc-od">
				<input class="sum-od-h" hidden="hidden" value="<?php echo $per['sum_od']; ?>">
				<?php 
					$st = $portfolio->currentRateAmount+$per['min_step'];
					$od = $per['sum_od'];
					if((isset($st))&&(isset($od))) {
						$res = $st/$od*100;?>
						<span><?php echo 'Процент от ОД: '.round($res, 2).' %'; ?></span>
					<?php }
				?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 perc-oz">
				<input class="sum-od-g" hidden="hidden" value="<?php echo $per['general_sum_credit']; ?>">
				<?php 
					$st = $portfolio->currentRateAmount+$per['min_step'];
					$od = $per['general_sum_credit'];
					if((isset($st))&&(isset($od))) {
						$res = $st/$od*100; ?>
						<span><?php echo 'Процент от ОЗ: '.round($res, 2).' %'; ?></span>
					<?php }
				?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php 
						$step = $per['min_step']; 
						$labelStep = 'Шаг ставки: ';
				?>
				<?php 
					echo $labelStep.$step;
				?>
			</div>
		</div>
		<?php ActiveForm::end(); ?>
	<?php } ?>

</div>

