<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Portfolio;
use kartik\select2\Select2;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use app\models\Company;
use app\widgets\NumericInput;

/* @var $this yii\web\View */
/* @var $model app\models\Portfolio */
/* @var $form yii\widgets\ActiveForm */
?>

<script type="text/javascript"> 

/*$(function(){
	$('.checkbox-a label').click(function() {
		$(this).children('input').val(33);
	});
	setInterval(function() {
		$('.checkbox-a').each(function() {
			var hh = $(this).children().children('label').children().val();
			alert(hh);
			if($(this).children().children('label').children().val() == 33) {
				$(this).siblings('.checkbox-a').children().children('label').children('input').val(22);
				$(this).children().children('label').children().attr("checked", false);
				//alert(gg);
				//$(this).hide();
				//console.log(1);
				//return false;
			} 
		});
	}, 5000);
});*/

</script>

<div class="portfolio-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<div class="row" hidden="hidden">
		<?= $form->field($model, 'type')->textInput(['value'=>2]) ?>
	</div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'count_actions')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'sum_od')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'sum_percents')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'general_sum_credit')->textInput() ?>
        </div>
    </div>
	
	<div class="row">
        <div class="col-md-6">
			<?= $form->field($model, 'min_step_percent')->textInput(['placeholder'=> '0.5']) ?>
		</div>
		<div class="col-md-6">
            <?= $form->field($model, 'min_step_fix')->textInput(['placeholder'=> '50']) ?>
        </div>
    </div>
	<h4 style="text-align: center;">Сетка вознаграждений</h4>
	<div class="row">
		<table class="soft_p" style="width: 97%; margin-left: 6px;">
			<thead>
				<tr>
					<td style="width: 4%"></td><td style="width: 48%; text-align: center;">Период просрочки (дней)</td><td style="width: 48%; text-align: center;">Вознаграждение %</td>
				</tr>
			</thead>
			<tbody>
				<tr style="height: 40px;">
					<td><?= $form->field($model, 'is_soft_30_days')->checkbox() ?></td>
					<td><?= $form->field($model, 'soft_30_days')->textInput(['placeholder'=> '90-180']) ?></td>
					<td><?= $form->field($model, 'soft_30_days_percent')->textInput() ?></td>
				</tr>
				<tr>
					<td><?= $form->field($model, 'is_soft_90_days')->checkbox() ?></td>
					<td><?= $form->field($model, 'soft_90_days')->textInput(['placeholder'=> '90-180']) ?></td>
					<td><?= $form->field($model, 'soft_90_days_percent')->textInput() ?></td>
				</tr>
				<tr>
					<td><?= $form->field($model, 'is_soft_180_days')->checkbox() ?></td>
					<td><?= $form->field($model, 'soft_180_days')->textInput(['placeholder'=> '90-180']) ?></td>
					<td><?= $form->field($model, 'soft_180_days_percent')->textInput() ?></td>
				</tr>
				<tr>
					<td><?= $form->field($model, 'is_soft_365_days')->checkbox() ?></td>
					<td><?= $form->field($model, 'soft_365_days')->textInput(['placeholder'=> '90-180']) ?></td>
					<td><?= $form->field($model, 'soft_365_days_percent')->textInput() ?></td>
				</tr>
				<tr>
					<td><?= $form->field($model, 'is_soft_730_days')->checkbox() ?></td>
					<td><?= $form->field($model, 'soft_730_days')->textInput(['placeholder'=> '90-180']) ?></td>
					<td><?= $form->field($model, 'soft_730_days_percent')->textInput() ?></td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<div class="row">
		<table class="soft_p" style="width: 97%; margin-left: 6px;">
			<tbody>
				<tr>
					<td style="width: 4%"><?= $form->field($model, 'is_legal_percent')->checkbox()->label(false) ?></td>
					<td style="width: 48%; color: #242a30; font-weight: 700; top: 4px; position: relative;">Legal collection %</td>
					<td style="width: 48%"><?= $form->field($model, 'legal_percent')->textInput() ?></td>
				</tr>
				<tr>
					<td style="width: 4%"><?= $form->field($model, 'is_legal_fix')->checkbox()->label(false) ?></td>
					<td style="width: 48%; color: #242a30; font-weight: 700; top: 4px; position: relative;">Legal collection (fix, руб)</td>
					<td style="width: 48%"><?= $form->field($model, 'legal_fix')->textInput() ?></td>
				</tr>
			</tbody>
		</table>
	</div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'count_ip')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'is_pledge')->checkbox() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'general_payments')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'product')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'pledge_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'count_placements_ka')->textInput() ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'company_id')->dropDownList(ArrayHelper::map(Company::find()->all(), 'id', 'name'), ['value' => Yii::$app->user->identity->company_id, 'disabled' => true])->label('Продавец') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'creditor')->textInput()->label('Кредитор <span class="req" style="color: red !important; font-size: 13px;">*</span>') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'datetime_start')->widget(DateTimePicker::className(), [

            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'datetime_end')->widget(DateTimePicker::className(), [

            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php if($model->isNewRecord): ?>
                <?= \kato\DropZone::widget([
                    'id'        => 'dzImage', // <-- уникальные id
                    'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file' ]),
                    'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                    'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                    'options' => [
                        'autoDiscover' => false,
                        'maxFilesize' => '50',
                    ],
                    'clientEvents' => [
                        'success' => "function(file, response){
                                console.log(response);

                                var id = response.record.id;

                                var value = $('#portfolio-files').val();
                                $('#portfolio-files').val(value+','+id);
                             }",
//                        'complete' => "function(file){ console.log(file); }",
                    ],
                ]);?>
            <?php else: ?>
                <?= \kato\DropZone::widget([
                    'id'        => 'dzImage', // <-- уникальные id
                    'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file', 'portfolio_id' => $model->id]),
                    'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                    'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                    'options' => [
                        'maxFilesize' => '50',
                    ],
                ]);?>
            <?php endif; ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model,'hide_saler')->checkbox() ?>
        </div>
    </div>

    <div class="hidden">
        <?= $form->field($model, 'files')->hiddenInput() ?>
    </div>


    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>

