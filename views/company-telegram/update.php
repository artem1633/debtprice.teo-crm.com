<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyTelegram */
?>
<div class="company-telegram-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
