<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'template' => '{get-js-code} {webhook}{update}{delete}',
        'buttons' => [
            'get-js-code' => function($url, $model){
                if($model->type == \app\models\BotSetting::TYPE_SITE){
                    return Html::a('<i class="fa fa-code text-warning" style="font-size: 16px;"></i>', $url, [
                        'role' => 'modal-remote', 'title' => 'Получить js-код',
                    ]);
                }

                return '';
            },
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                        'role'=>'modal-remote', 'title'=>'Изменить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                    ])."&nbsp;";
            },
            'webhook' => function ($url, $model) {
                if ($model->type == 1){
                    if($model->status === 0 or !$model->status){
                        return Html::a('<i class="fa fa-flag text-primary" style="font-size: 16px;"></i>', ['webhook', 'id' => $model->id], [
                                'title'=>'Подключить бота',
                            ])."&nbsp;";
                    }
                }

            }
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'bot_id',
        'value' => 'bot.name',
//        'content' => function($data){
//            $html = '';
//
//            if($data->bot != null) {
//                $html .= $data->bot->name;
//            }
//
//            $html .= '<p>';
//
//            if ($data->type == 1){
//                if($data->status === 0 or !$data->status){
//                    $html .= Html::a('<i class="fa fa-flag text-primary" style="font-size: 16px;"></i>', ['webhook', 'id' => $data->id], [
//                            'title'=>'Подключить бота',
//                        ])."&nbsp;";
//                }
//            }
//
//            if($data->type == \app\models\BotSetting::TYPE_SITE){
//                $html .= Html::a('<i class="fa fa-code text-warning" style="font-size: 16px;"></i>', ['get-js-code', 'id' => $data->id], [
//                    'role' => 'modal-remote', 'title' => 'Получить js-код',
//                ])."&nbsp;";
//            }
//
//            $html .= Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', ['update', 'id' => $data->id], [
//                    'role'=>'modal-remote', 'title'=>'Изменить',
//                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                    'data-request-method'=>'post',
//                ])."&nbsp;";
//
//
//            $html .= Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', ['delete', 'id' => $data->id], [
//                'role'=>'modal-remote', 'title'=>'Удалить',
//                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                'data-request-method'=>'post',
//                'data-confirm-title'=>'Вы уверены?',
//                'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
//            ]);
//
//
//            $html .= '</p>';
//
//            return $html;
//        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type',
        'content' => function($data){
            switch ($data->type){
                case \app\models\BotSetting::TYPE_TELEGRAM:
                    return 'Телеграм';
                    break;
                case \app\models\BotSetting::TYPE_VK:
                    return 'ВК';
                    break;
                default:
                    return 'Не определено';
                    break;
            }
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'content' => function($model){
            if($model->status == 1){
                return 'Подключен';
            }

            return 'Не подключен';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'company_id',
        'value' => 'company.name',
        'filterType' => \kartik\select2\Select2::className(),
        'filterWidgetOptions' => [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\Company::find()->all(), 'id', 'name'),
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Выберите компани'],
        ],
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'token',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'additional_setting',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'group_id',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'time_code',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'company_id',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],

];