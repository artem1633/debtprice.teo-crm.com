<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BotSetting */
?>
<div class="bot-setting-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'bot_id',
            'name',
            'type',
            'link',
            'status',
            'token',
            'additional_setting',
            'group_id',
            'time_code',
            'company_id',
            'created_at',
        ],
    ]) ?>

</div>
