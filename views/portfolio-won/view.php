<?php

use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use app\models\Portfolio;
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Portfolio */
/* @var $adminFilesModel \app\models\AdminFiles */

?>
<style>
    .detail-view th {
        text-align: left;
    }
</style>
<div class="portfolio-view">
    <p>

        <?php if($model->stage == Portfolio::STAGE_START): ?>
            <?php if(Yii::$app->user->identity->company_id == $model->company_id): ?>
                <?php if($model->contract_creator == null): ?>
                    <?= Html::a('Загрузить скан договора', '#', ['class' => 'btn btn-success', 'data-file-input' => 'contract_creator', 'data-id' => $model->id]) ?>
                <?php else: ?>
                    <?= Html::a('Загрузить скан договора', '#', ['class' => 'btn btn-default']) ?>
                <?php endif; ?>
            <?php else: ?>
                <?php if($model->contract_winner == null): ?>
                    <?= Html::a('Загрузить скан договора', '#', ['class' => 'btn btn-success', 'data-file-input' => 'contract_winner', 'data-id' => $model->id]) ?>
                <?php else: ?>
                    <?= Html::a('Загрузить скан договора', '#', ['class' => 'btn btn-default']) ?>
                <?php endif; ?>
            <?php endif; ?>
        <?php else: ?>
            <?= Html::a('Загрузить скан договора', '#', ['class' => 'btn btn-default']) ?>
        <?php endif; ?>

        <?php if(Yii::$app->user->identity->company_id == $model->company_id): ?>
            <?php if($model->stage == Portfolio::STAGE_CONTRACTS_LOADED && $model->bill == null): ?>
                <?= Html::a('Загрузить счет', '#', ['class' => 'btn btn-warning', 'data-file-input' => 'bill', 'data-id' => $model->id]) ?>
            <?php else: ?>
                <?= Html::a('Загрузить счет', '#', ['class' => 'btn btn-default']) ?>
            <?php endif; ?>
        <?php endif; ?>

        <?php if(Yii::$app->user->identity->company_id == $model->winner_id && false): ?>
            <?php if($model->bill != null): ?>
                <?= Html::a('Скачать счет', '/'.$model->bill, ['class' => 'btn btn-info', 'download' => 'Договор победителя', 'data-pjax' => '0']) ?>
            <?php else: ?>
                <?= Html::a('Скачать счет', '#', ['class' => 'btn btn-default', 'data-pjax' => '0']) ?>
            <?php endif; ?>
        <?php endif; ?>


        <?php if(Yii::$app->user->identity->company_id == $model->company_id): ?>
            <?php if($model->stage == Portfolio::STAGE_BILL_PAYED && $model->bill_confirmed == null): ?>
                <?= Html::a('Платеж получен', '#', ['class' => 'btn btn-warning', 'data-attribute-input' => 'bill_confirmed', 'data-id' => $model->id]) ?>
            <?php else: ?>
                <?= Html::a('Платеж получен', '#', ['class' => 'btn btn-default']) ?>
            <?php endif; ?>
        <?php endif; ?>

        <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
            <?php if($model->commission_payed == null): ?>
                <?= Html::a('Комиссия получена', '#', ['class' => 'btn btn-warning',  'data-attribute-input' => 'commission_payed', 'data-id' => $model->id]) ?>
            <?php else: ?>
                <?= Html::a('Комиссия получена', '#', ['class' => 'btn btn-default']) ?>
            <?php endif; ?>
        <?php endif; ?>
    </p>
    <h3>Документы по сделке</h3>
    <?php
    $arr = [];
    if($model->contract_winner != null){
        $arr[] = [
            'name' => 'Скан договора покупателя',
            'path' => $model->contract_winner
        ];
    }

    if($model->contract_creator != null){
        $arr[] = [
            'name' => 'Скан договора продавца',
            'path' => $model->contract_creator
        ];
    }

    if($model->bill != null){
        $arr[] = [
            'name' => 'Скан счета',
            'path' => $model->bill
        ];
    }

    $clientFilesDataProvider = new ArrayDataProvider();
    $clientFilesDataProvider->setModels($arr);
    ?>
    <?= GridView::widget([
        'id'=>'crud-file-client-datatable',
        'dataProvider' => $clientFilesDataProvider,
//            'filterModel' => $searchModel,
        'pjax'=>true,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'width' => '30px',
            ],
            // [
            // 'class'=>'\kartik\grid\DataColumn',
            // 'attribute'=>'id',
            // ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'name',
                'label' => 'Наименование',
                'content' => function($data){
                    return Html::a($data['name'], $data['path'], ['data-pjax' => 0, 'download' => $data['name']]);
                }
            ],
        ],
//            'panelBeforeTemplate' =>
//                Html::a('Добавить <i class="fa fa-plus"></i>', ['add-file', 'id' => $portfolioId],
//                    ['role'=>'modal-remote','title'=> 'Добавить файл','class'=>'btn btn-success']).'&nbsp;'.
//                Html::a('<i class="fa fa-repeat"></i>', [''],
//                    ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']),
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => null,
    ]) ?>
    <h3>Портфель:</h3>
    <?= $this->render('@app/views/portfolio-file/index', [
        'searchModel' => $filesSearchModel,
        'dataProvider' => $filesDataProvider,
        'portfolioId' => $model->id
    ]) ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'creditor',
                'label' => 'Кредитор',
            ],
            [
                'attribute' => 'company_id',
                'label' => 'Продавец',
                'value' => function($model){
                    if($model->hide_saler == 1 && Yii::$app->user->identity->isSuperAdmin() == false){
                        return 'DEBTPRICE';
                    } else if($model->hide_saler == 1 && Yii::$app->user->identity->isSuperAdmin()){
                        $company = \app\models\Company::findOne($model->company_id);

                        if($company){
                            return "DEBTPRICE ({$company->name})";
                        }

                    }

                    $company = \app\models\Company::findOne($model->company_id);

                    if($company){
                        return $company->name;
                    }
                },
            ],
            'name',
            [
                'attribute' => 'count_actions',
                'label' => 'Заемщики',
                'value' => function($model){
                    if($model->count_actions > 0){
                        return number_format($model->count_actions, 0, 0, ' ').' ₽';
                    }
                },
            ],
            [
                'attribute' => 'sum_od',
                'label' => 'ОД',
                'value' => function($model){
                    if($model->sum_od > 0){
                        return number_format($model->sum_od, 0, 0, ' ').' ₽';
                    }
                },
            ],
            [
                'attribute' => 'sum_percents',
                'label' => 'Проценты',
                'value' => function($model){
                    if($model->sum_percents > 0){
                        return number_format($model->sum_percents, 0, 0, ' ').' ₽';
                    }
                },
            ],
            [
                'attribute' => 'general_sum_credit',
                'value' => function($model){
                    if($model->general_sum_credit > 0){
                        return number_format($model->general_sum_credit, 0, 0, ' ').' ₽';
                    }
                },
            ],
            [
                'attribute' => 'count_ip',
                'format' => 'integer',
            ],
            [
                'attribute' => 'count_il',
                'format' => 'integer',
            ],
            'is_pledge',
            [
                'attribute' => 'general_payments',
                'value' => function($model){
                    if($model->general_payments > 0){
                        return number_format($model->general_payments, 0, 0, ' ').' ₽';
                    }
                },
            ],
            [
                'attribute' => 'count_action_before_90_days',
                'format' => 'integer',
            ],
            [
                'attribute' => 'count_action_90_180_days',
                'format' => 'integer',
            ],
            [
                'attribute' => 'count_action_181_365_days',
                'format' => 'integer',
            ],
            [
                'attribute' => 'count_action_366_730_days',
                'format' => 'integer',
            ],
            [
                'attribute' => 'count_action_731_1095_days',
                'format' => 'integer',
            ],
            [
                'attribute' => 'count_action_over_1096_days',
                'format' => 'integer',
            ],
            'product',
            'location',
            'pledge_name',
            [
                'attribute' => 'count_placements_ka',
                'format' => 'integer',
            ],
            [
                'attribute' => 'status',
                'value' => function($model){
                    return ArrayHelper::getValue(Portfolio::statusLabels(), $model->status);
                }
            ],
//            [
//                'attribute' => 'datetime_start',
//                'format' => ['date', 'php:d M Y H:i'],
//            ],
//            [
//                'attribute' => 'datetime_end',
//                'format' => ['date', 'php:d M Y H:i'],
//            ],
            [
                'attribute' => 'datetime_start',
                'label' => 'Время торгов',
                'format' => 'raw',
                'value' => function($model){
                    $od = $model->datetime_start;
                    $od1 = $model->datetime_end;
                    $_monthsList = array("Jan" => "янв.", "Feb" => "фев.",
                        "Mar" => "мар.", "Apr" => "апр.", "May" => "мая", "Jun" => "июн.",
                        "Jul" => "июл.", "Aug" => "авг.", "Sep" => "сен.",
                        "Oct" => "окт.", "Nov" => "ноя.", "Dec" => "дек.");
                    $currentDate = date("d M Y H:i", strtotime($od));
                    $currentDate1 = date("d M Y H:i", strtotime($od1));

                    $_mD = date("M", strtotime($od));
                    $_mD1 = date("M", strtotime($od1));
                    $date = date("d M Y", strtotime($od));
                    $date1 = date("d M Y", strtotime($od1));
                    $time = date("H:i", strtotime($od));
                    $time1 = date("H:i", strtotime($od1));

                    if($date == $date1) {
                        $date = str_replace($_mD, " ".$_monthsList[$_mD]." ", $date);
                        $currentDate = $date.'<br/><span class="time-s">'.$time.'-'.$time1.'</span>';

                        return $currentDate;
                    } else {
                        $date = str_replace($_mD, " ".$_monthsList[$_mD]." ", $date);
                        $date1 = str_replace($_mD1, " ".$_monthsList[$_mD1]." ", $date1);
                        //$currentDate1 = str_replace($_mD1, " ".$_monthsList[$_mD1]." ", $currentDate1);

                        return $date.'<br/><span class="time-s">'.$time.'-'.$time1.'</span><br/>'.$date1;
                    }
                },
            ],
            [
                'attribute' => 'winner.name',
                'label' => 'Победитель',
                'visible' => Yii::$app->user->identity->isSuperAdmin(),
            ],
            [
                'attribute' => 'start_rate',
                'value' => function($model){
                    if($model->start_rate > 0){
                        return number_format($model->start_rate, 0, 0, ' ').' ₽';
                    }
                },
            ],
            [
                'attribute' => 'min_step',
                'value' => function($model){
                    if($model->min_step > 0){
                        return number_format($model->min_step, 0, 0, ' ').' ₽';
                    }
                },
            ],
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function($model){
                    $date = Yii::$app->formatter->asDate($model->created_at, 'php:d M Y');
                    $time = Yii::$app->formatter->asDate($model->created_at, 'php:H:i:s');

                    return $date.'<br/><span class="time-s">'.$time.'</span>';
                },
            ],
			[
                'attribute' => 'legal_percent',
                'format' => 'decimal',
                'visible' => ($model->is_soft_30_days || $model->is_soft_90_days || $model->is_soft_180_days || $model->is_soft_365_days || $model->is_soft_730_days || $model->is_legal_percent || $model->is_legal_fix),
            ],
            [
                'attribute' => 'legal_fix',
                'format' => 'integer',
                'visible' => ($model->is_soft_30_days || $model->is_soft_90_days || $model->is_soft_180_days || $model->is_soft_365_days || $model->is_soft_730_days || $model->is_legal_percent || $model->is_legal_fix),
            ],
        ],
    ]) ?>
	<?php if($model->is_soft_30_days || $model->is_soft_90_days || $model->is_soft_180_days || $model->is_soft_365_days || $model->is_soft_730_days) {?>
		<h4 style="text-align: center; width: 600px;">Сетка вознаграждений</h4>
		<table class="table table-striped table-bordered detail-view" style="width: 600px;">
			<thead>
				<tr>
					<td style="width: 50%;">Период просрочки (дней)</td><td style="width: 50%;">Вознаграждение %</td>
				</tr>
			</thead>
			<tbody>
				<?php if($model->is_soft_30_days) { ?>
					<tr style="height: 40px;">
						<td><?php echo $model->soft_30_days; ?></td>
						<td><?php echo $model->soft_30_days_percent; ?></td>
					</tr>
				<?php } ?>
				<?php if($model->is_soft_90_days) { ?>
					<tr style="height: 40px;">
						<td><?php echo $model->soft_90_days; ?></td>
						<td><?php echo $model->soft_90_days_percent; ?></td>
					</tr>
				<?php } ?>
				<?php if($model->is_soft_180_days) { ?>
					<tr style="height: 40px;">
						<td><?php echo $model->soft_180_days; ?></td>
						<td><?php echo $model->soft_180_days_percent; ?></td>
					</tr>
				<?php } ?>
				<?php if($model->is_soft_365_days) { ?>
					<tr style="height: 40px;">
						<td><?php echo $model->soft_365_days; ?></td>
						<td><?php echo $model->soft_365_days_percent ?></td>
					</tr>
				<?php } ?>
				<?php if($model->is_soft_730_days) { ?>
					<tr style="height: 40px;">
						<td><?php echo $model->soft_730_days ?></td>
						<td><?php echo $model->soft_730_days_percent; ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	<?php } ?>
    <div class="hidden">
        <?php $formModel = new Portfolio(['contract_creator' => $model->contract_creator, 'contract_winner' => $model->contract_winner, 'bill' => $model->bill]); $form = ActiveForm::begin(['id' => 'portfolio-file-form-'.$model->id]) ?>
        <?= $form->field($formModel, 'contract_creator')->fileInput() ?>
        <?= $form->field($formModel, 'contract_winner')->fileInput() ?>
        <?= $form->field($formModel, 'bill')->fileInput() ?>
        <?php ActiveForm::end() ?>
    </div>

    <?php
    $script = <<< JS
$("#portfolio-file-form-{$model->id} input[type='file']").change(function(e){

    var data = new FormData($('#portfolio-file-form-{$model->id}')[0]);  
 
    $.ajax({
        url: '/portfolio/upload-for-stage-file?id={$model->id}',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST', // For jQuery < 1.9
        success: function(data){
            if(data.length == 0){
                $.pjax.reload('#crud-datatable-pjax');
            } else {
                alert('Возникла ошибка при загрузке фото');
            }
        }
    });
});

JS;


    $this->registerJs($script, \yii\web\View::POS_READY);
    ?>


</div>
