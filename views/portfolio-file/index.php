<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use app\models\Portfolio;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CandidateFileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $portfolioId integer */

//$this->title = "Файлы кандидата";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$portfolio = Portfolio::findOne($portfolioId);

?>
<?php if($dataProvider->totalCount > 0): ?>
    <p>
        <?= Html::a('Скачать все файлы', ['portfolio/download-all-files', 'id' => $portfolioId], ['class' => 'btn btn-primary', 'data-pjax' => 0]) ?>
    </p>
<?php endif; ?>
<?=GridView::widget([
    'id'=>'crud-file-datatable',
    'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
    'responsiveWrap' => false,
    'pjax'=>true,
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
        // ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'name',
            'content' => function($data){
                return Html::a($data->name, ['/portfolio/download-file', 'id' => $data->id], ['data-pjax' => 0]);
            }
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'vAlign'=>'middle',
            'visible' => Yii::$app->user->identity->isSuperAdmin(),
            'urlCreator' => function($action, $model, $key, $index) {
                return Url::to(['portfolio-file/'.$action,'id'=>$key]);
            },
            'template' => '{delete}',
            'buttons' => [
                'delete' => function ($url, $model) {
                    return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                        'role'=>'modal-remote', 'title'=>'Удалить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'Вы уверены?',
                        'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                    ]);
                },
                'update' => function ($url, $model) {
                    return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                            'role'=>'modal-remote', 'title'=>'Изменить',
                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                            'data-request-method'=>'post',
                        ])."&nbsp;";
                }
            ],
        ],

    ],
//            'panelBeforeTemplate' =>
//                Html::a('Добавить <i class="fa fa-plus"></i>', ['add-file', 'id' => $portfolioId],
//                    ['role'=>'modal-remote','title'=> 'Добавить файл','class'=>'btn btn-success']).'&nbsp;'.
//                Html::a('<i class="fa fa-repeat"></i>', [''],
//                    ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']),
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'panel' => null,
])?>

