<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BotScenario */

?>
<div class="bot-scenario-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
