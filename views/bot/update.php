<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bot */
?>
<div class="bot-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
