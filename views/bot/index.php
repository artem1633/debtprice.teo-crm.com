<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BotSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Боты";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$dataProvider->pagination = false;



?>
<div class="col-md-12">
    <div class="panel panel-inverse bot-index">
        <div class="panel-heading">
            <!--        <div class="panel-heading-btn">-->
            <!--        </div>-->
            <h4 class="panel-title">Боты</h4>
        </div>
        <div class="panel-body">
            <?php

            echo  Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                    ['role'=>'modal-remote','title'=> 'Добавить бота','class'=>'btn btn-success']).'&nbsp;'.Html::a('Использовать шаблон', ['create-by-template'], [
                    'role' => 'modal-remote', 'title' => 'Использовать шаблон для создрания', 'class' => 'btn btn-info'
                ]).'&nbsp;'.
                Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']);

            ?>
            <!--        <div id="ajaxCrudDatatable">-->
            <?php
            //            echo GridView::widget([
            //            'id'=>'crud-datatable',
            //            'dataProvider' => $dataProvider,
            //            'filterModel' => $searchModel,
            //            'pjax'=>true,
            //            'columns' => require(__DIR__.'/_columns.php'),
            //            'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
            //                    ['role'=>'modal-remote','title'=> 'Добавить бота','class'=>'btn btn-success']).'&nbsp;'.Html::a('Использовать шаблон', ['create-by-template'], [
            //                    'role' => 'modal-remote', 'title' => 'Использовать шаблон для создрания', 'class' => 'btn btn-info'
            //                ]).'&nbsp;'.
            //                Html::a('<i class="fa fa-repeat"></i>', [''],
            //                    ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']),
            //            'striped' => true,
            //            'condensed' => true,
            //            'responsive' => true,
            //                'responsiveWrap' => false,
            //            'panel' => [
            //            'headingOptions' => ['style' => 'display: none;'],
            //            'after'=>BulkButtonWidget::widget([
            //            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
            //            ["bulk-delete"] ,
            //            [
            //            "class"=>"btn btn-danger btn-xs",
            //            'role'=>'modal-remote-bulk',
            //            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            //            'data-request-method'=>'post',
            //            'data-confirm-title'=>'Вы уверены?',
            //            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
            //            ]),
            //            ]).
            //            '<div class="clearfix"></div>',
            //            ]
            //            ])
            ?>
            <!--        </div>-->
        </div>
    </div>
</div>

<?php \yii\widgets\Pjax::begin(['id' => 'crud-datatable-pjax']) ?>
<?php foreach($dataProvider->models as $model): ?>
    <?php
    /** @var \app\models\Bot $model */
    $statuses = \app\models\CustomerStatus::find()->all();
    $statusMap = [];
    foreach ($statuses as $status) {
        $statusMap[$status->name] = \app\models\Customer::find()->where(['bot_id' => $model->id, 'status_id' => $status->id])->count();
    }
    ?>
    <div class="col-md-4">
        <?php

        if($model->using_as_template == 1){
            $panelClass = 'panel-default';
        } else {
            $panelClass = 'panel-inverse';
        }

        ?>
        <div class="panel <?=$panelClass?>">
            <div class="panel-heading">
                <h4 class="panel-title"><?=$model->name?></h4>
                <div class="panel-heading-btn" style="margin-top: -20px;">
                    <?= Html::a('<i class="fa fa-envelope"></i>', ['send-message-to-all', 'id' => $model->id], [
                        'class' => 'btn btn-sm btn-icon btn-white',
                        'role'=>'modal-remote', 'title'=>'Отправить массовое сообщение',
                    ]) ?>
                    <?= Html::a('<i class="fa fa-magic"></i>', ['view', 'id' => $model->id], [
                        'class' => 'btn btn-sm btn-icon btn-warning',
                        'data-pjax'=>0, 'title'=>'Просмотр',
                    ]) ?>
                    <?= Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $model->id], [
                        'class' => 'btn btn-sm btn-icon btn-primary',
                        'role'=>'modal-remote', 'title'=>'Изменить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',]); ?>
                    <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-sm btn-icon btn-danger',
                        'role'=>'modal-remote', 'title'=>'Удалить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'Вы уверены?',
                        'data-confirm-message'=>'Вы действительно хотите удалить данную запись?']); ?>
                    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                        <?= Html::a('<i class="fa fa-sticky-note"></i>', ['as-template', 'id' => $model->id], [
                            'class' => 'btn btn-sm btn-icon btn-info',
                            'role'=>'modal-remote', 'title'=>'Пометить как шаблон',
                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                            'data-request-method'=>'post',
                            'data-confirm-title'=>'Вы уверены?',
                            'data-confirm-message'=>'Вы действительно хотите пометить запись как шаблон?'
                        ]) ?>
                    <?php endif; ?>
                    <?= Html::a('<i class="fa fa-clone"></i>', ['copy', 'id' => $model->id], [
                        'class' => 'btn btn-sm btn-icon btn-success',
                        'role'=>'modal-remote', 'title'=>'Копировать',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'Вы уверены?',
                        'data-confirm-message'=>'Вы действительно хотите скопировать запись?'
                    ]); ?>
                </div>
            </div>
            <div class="panel-body">
                <?php foreach ($statusMap as $label => $count): ?>
                    <div class="col-md-3">
                        <p>
                            <b><?=$label?></b>
                            <span><?=$count?></span>
                        </p>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<?php \yii\widgets\Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
