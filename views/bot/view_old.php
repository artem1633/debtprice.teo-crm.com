<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bot */

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>
<div class="bot-view">

    <div class="row">
            <div class="col-md-12">
                <div class="panel panel-success">
                    <?=Html::a('Добавить', ['bot-scenario/create', 'botId' => $model->id], ['class' => 'panel-body btn-success btn-block text-center', 'style' => 'borer-radius: 50%; opacity: .8;', 'role' => 'modal-remote'])?>
                </div>
            </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="view-container" style="width: 100%; overflow-x: scroll; white-space: nowrap;">
                <?php \yii\widgets\Pjax::begin(['id' => 'scenarios-pjax-container', 'enablePushState' => false]) ?>
                <?php $beingIds = []; ?>
                <?php foreach ($model->botScenarios as $scenario): ?>
                    <?php

                    if(in_array($scenario->id, $beingIds)){
                        continue;
                    }

                    $beingIds[] = $scenario->id;

                    $panelClass = 'inverse';
                    if($scenario->is_start) {
                        $panelClass = 'success';
                    }

                    $panelButtons = Html::a('<i class="fa fa-pencil"></i>', ['bot-scenario/update', 'id' => $scenario->id], ['class' => 'btn btn-info btn-xs', 'role' => 'modal-remote'])
                    .Html::a('<i class="fa fa-plus"></i>', ['bot-scenario/create', 'id' => $scenario->id, 'botId' => $scenario->bot_id], ['class' => 'btn btn-success btn-xs', 'role' => 'modal-remote']);

                    if($scenario->is_start == false){
                        $panelButtons .= Html::a('<i class="fa fa-trash"></i>', ['bot-scenario/delete', 'id' => $scenario->id], ['class' => 'btn btn-danger btn-xs', 'role' => 'modal-remote',
                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                            'data-request-method'=>'post',
                            'data-confirm-title'=>'Вы уверены?',
                            'data-confirm-message'=>'Вы действительно хотите удалить данный сценарий?']);
                    }
                    ?>
                    <div class="panel panel-<?=$panelClass?>" style="display: block; width: 100%;">
                        <div class="panel-heading">
                            <h4 class="panel-title"><?=$scenario->name?></h4>
                            <div class="panel-heading-btn" style="margin-top: -21px;">
                                <?= $panelButtons ?>
                            </div>
                        </div>
                        <div class="panel-body" style="white-space: initial;">
                            <div class="hidden">
                                <p><b><?= $scenario->getAttributeLabel('variants') ?>: </b> <?= implode(', ', \yii\helpers\ArrayHelper::getColumn(\app\models\BotScenarioVariant::find()->where(['bot_scenario_id' => $scenario->id])->all(), 'content')) ?></p>
                                <p><b><?= $scenario->getAttributeLabel('bot_answer') ?>: </b> <?= $scenario->bot_answer ?></p>
                                <div class="row">
                                    <div class="col-md-5">
                                        <p><b><?= $scenario->getAttributeLabel('strict_search_mode') ?>: </b> <?= \app\widgets\BooleanValue::widget(['value' => $scenario->strict_search_mode ]) ?></p>
                                        <p><b><?= $scenario->getAttributeLabel('repeat') ?>: </b> <?= \app\widgets\BooleanValue::widget(['value' => $scenario->repeat]) ?></p>
                                    </div>
                                    <div class="col-md-5">
                                        <p><b><?= $scenario->getAttributeLabel('any_user_answer') ?>: </b> <?= \app\widgets\BooleanValue::widget(['value' => $scenario->any_user_answer]) ?></p>
                                        <p><b><?= $scenario->getAttributeLabel('no_user_answer') ?>: </b> <?= \app\widgets\BooleanValue::widget(['value' => $scenario->no_user_answer]) ?></p>
                                    </div>
                                </div>
                            </div>
                            <?php foreach ($scenario->nextScenarios as $nextScenario): ?>
                                <?php echo renderScenario($nextScenario, $beingIds); ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php \yii\widgets\Pjax::end() ?>
            </div>
        </div>
    </div>
</div>

<?php

function renderScenario($scenario, &$beingIds, $recurse = true){
    $panelClass = 'inverse';
    if($scenario->is_start) {
        $panelClass = 'success';
    }

    $beingIds[] = $scenario->id;

    $panelButtons = Html::a('<i class="fa fa-pencil"></i>', ['bot-scenario/update', 'id' => $scenario->id], ['class' => 'btn btn-info btn-xs', 'role' => 'modal-remote'])
        .Html::a('<i class="fa fa-plus"></i>', ['bot-scenario/create', 'id' => $scenario->id, 'botId' => $scenario->bot_id], ['class' => 'btn btn-success btn-xs', 'role' => 'modal-remote']);

    if($scenario->is_start == false){
        $panelButtons .= Html::a('<i class="fa fa-copy"></i>', ['bot-scenario/copy', 'id' => $scenario->id], ['class' => 'btn btn-primary btn-xs', 'role' => 'modal-remote', 'data-request-method'=>'post',
            'data-confirm-title'=>'Вы уверены?',
            'data-confirm-message'=>'Вы действительно хотите скопировать данный сценарий?'
                ]).Html::a('<i class="fa fa-trash"></i>', ['bot-scenario/delete', 'id' => $scenario->id], ['class' => 'btn btn-danger btn-xs', 'role' => 'modal-remote',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-confirm-title'=>'Вы уверены?',
            'data-confirm-message'=>'Вы действительно хотите удалить данный сценарий?']);
    }

    $html = '
    <div class="panel panel-'.$panelClass.'" style="display: block; width: 100%;">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">'.$scenario->name.'</h4>
                                        <div class="panel-heading-btn" style="margin-top: -21px;">
                                            '.$panelButtons.'
                                        </div>
                                    </div>
                                    <div class="panel-body" style="white-space: initial; background: #cecece; border: 1px solid #000">
                                    '.$scenario->bot_answer.'
                                        ';

    if($recurse){
        foreach ($scenario->nextScenarios as $nextScenario){
            if(in_array($nextScenario->id, $beingIds)){
                $html .= renderScenario($nextScenario, $beingIds, false);
            } else {
                $html .= renderScenario($nextScenario, $beingIds);
            }
        }
    }

    $html .= '
                                    </div>
                                </div>
    ';

    return $html;
}

?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
