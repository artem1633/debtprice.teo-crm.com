<style type="text/css">
	.modal-body #crud-datatable-container table tbody tr td[data-col-seq="6"],
	.modal-body #crud-datatable-container table tbody tr td[data-col-seq="7"],
	.modal-body #crud-datatable-container table thead tr th[data-col-seq="6"],
	.modal-body #crud-datatable-container table thead tr th[data-col-seq="7"] {
		display: none;
	}
</style>

<script type="text/javascript"> 

$(function(){
	var cn = $('.modal-body #crud-datatable-container table tbody tr td[data-col-seq="0"]').length;
	cn = cn + 1;
//	var srRate = $('.modal-body #crud-datatable-container table tbody tr:first-child td[data-col-seq="6"]').text();
//	var srDT = $('.modal-body #crud-datatable-container table tbody tr:first-child td[data-col-seq="7"]').text();
//	$('.modal-body #crud-datatable-container table tbody').append('<tr class="crud-datatable"><td style="text-align: center;">1</td><td style="text-align: center;">Начальная</td><td>'+
//	srRate+' ₽</td><td>'+srDT+'</td></tr>');
////	$('.modal-body #crud-datatable-container table tbody tr td[data-col-seq="3"]').each(function() {
////		var str = $(this).text();
////		str = str + ' ₽';
////		$(this).text(str);
////	});
//
	$('.modal-body #crud-datatable-container table tbody tr td[data-col-seq="0"]').each(function() {
		cn = cn - 1;
		$(this).text(cn);
	});
});

</script>

<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;
//if($portfolio->currentRateAmount) {
	return [
		[
			'class' => 'kartik\grid\SerialColumn',
			'width' => '30px',
		],
		[

			'class'=>'\kartik\grid\DataColumn',
			'attribute'=>'amount',
			'label' => 'Моя ставка',
			'format' => 'raw',
			'value' => function($model){
	            if($model['user_id'] == 0){
	                return 'Начальная';
                }

				if(Yii::$app->user->getId() == $model['user_id']){
					$output = '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';

					if($model['is_auto'] == 1){
					    $output .= '<img src="/img/autobid_orange.png" style="height: 16px;"></img>';
                    }
				} else {
                    $output = '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
				}

				if(Yii::$app->user->identity->isSuperAdmin() && $model['is_auto'] == 1){
                    $output .= '<img src="/img/autobid_orange.png" style="height: 16px;"></img>';
                }

				return $output;
			},
			'hAlign' => GridView::ALIGN_CENTER,
		],
		[
			'class'=>'\kartik\grid\DataColumn',
			'attribute'=>'company_name',
            'label' => 'Компания',
			'visible' => Yii::$app->user->identity->isSuperAdmin(),
		],
		[
			'class'=>'\kartik\grid\DataColumn',
			'attribute'=>'user_email',
			'label' => 'Пользователь',
			'visible' => Yii::$app->user->identity->isSuperAdmin(),
		],
		[

			'class'=>'\kartik\grid\DataColumn',
			'attribute'=>'amount',
			'label' => 'Сумма',
			'width' => '20%',
            'headerOptions' => ['style' => 'text-align: center;'],
			'value' => function($model) {
					$od = $model['amount'];
					if($model['user_id'] == 0){
                        return '<span class="gsc" style="text-align: right;">'.$od. ' ₽</span><br/>';
                    }
					$od = round($od, 0);
					$od1 = number_format(intval($od), 0, '', ' ');

					return '<span class="gsc" style="text-align: right;">'.$od1. ' ₽</span><br/>';
				},
			'hAlign' => GridView::ALIGN_RIGHT,
			//'format' => ['currency', 'rub'],
		],
		[
			'class'=>'\kartik\grid\DataColumn',
			'attribute'=>'created_at',
			'label' => 'Дата и время',
			'format' => ['date', 'php:d M Y H:i:s'],
		],
		[
			'class'=>'\kartik\grid\DataColumn',
			'attribute'=>'start_rate',
			'value' => 'portfolio.start_rate',
			'visible' => 'hidden',
		],
		[
			'class'=>'\kartik\grid\DataColumn',
			'attribute'=>'datetime_start',
			'value' => 'portfolio.datetime_start',
			'format' => ['date', 'php:d M Y H:i:s'],
			'visible' => 'hidden',
		],
	];   
/*} else {
	
	return [
		[
			'class' => 'kartik\grid\SerialColumn',
			'width' => '30px',
		],
		[

			'class'=>'\kartik\grid\DataColumn',
			'label' => 'Моя ставка',
			'value' => 'Начальная',
		],
		[
			'class'=>'\kartik\grid\DataColumn',
			'attribute'=>'portfolio.start_rate',
			'label' => 'Cтавка',
			'value' => function($model) {
				$od = $model->start_rate;
				//$od = round($od, 0);
				$od1 = number_format(intval($od), 0, '', ' ');

				return '<span class="gsc">'.$od1. ' ₽</span><br/>';
			}
		],
		[
			'class'=>'\kartik\grid\DataColumn',
			'attribute'=>'portfolio.datetime_start',
			'label' => 'Дата и время',
			'value' => 'portfolio.datetime_start',
			'format' => ['date', 'php:d M Y H:i:s'],
		],
	]; 
}*/
