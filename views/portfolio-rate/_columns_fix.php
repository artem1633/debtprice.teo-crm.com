<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_name',
        'label' => 'Компания',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_email',
        'label' => 'Пользователь',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'amount',
        'label' => 'Сумма',
        'format' => ['currency', 'percent'],
    ],
	[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'count_fix',
        'label' => 'Фиксированое значение',
        'format' => ['currency', 'rur'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'label' => 'Дата и время',
        'format' => ['date', 'php:d M Y H:i:s'],
    ],
];   