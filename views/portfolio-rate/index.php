<?php

use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PortfolioRateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */




?>

<script type="text/javascript"> 

$(function(){
	$('.modal-body #crud-datatable-container table tbody tr td').each(function() {
		var str = $(this).text();
		var strTmp = str.substr(-3);
		if(strTmp == 'per') {
			str = str.slice(0, -3);
			str = str + ' %';
			$(this).text(str);
		}
	});
	
});

</script>
<?php 
	if(!empty($auctPerc)) {
		foreach($auctPerc as $auctPercItem => $auctPercValue) {
			if($auctPercValue == 1) {
				$keyPerc = $auctPercItem;
				$keyPerc = substr($auctPercItem, 3);
				if($keyPerc !== 'legal_percent') {
					$keyPerc = $keyPerc.'_percent';
				}
			}
			if($portfolio->is_legal_fix == 1) {
				$fixPerc = $portfolio->legal_fix;
			}
		}
	}
	//echo $keyPerc;
?>
<?php
	if(!empty($fixPerc)) { ?>
		<?=
		GridView::widget([
			'id' => 'crud-datatable',
			'dataProvider' => $dataProvider,
			'responsiveWrap' => false,
			'pjax' => false,
			'columns' => require(__DIR__ . '/_columns_fix.php'),
			'panelBeforeTemplate' => '',
			'striped' => true,
			'condensed' => true,
			'bordered' => true,
			'responsive' => true,
			'panel' => '',
		]);
	?>	
	<?php } else {
	if(!empty($keyPerc)) {
?>
	<?=
		GridView::widget([
			'id' => 'crud-datatable',
			'dataProvider' => $dataProvider,
			'responsiveWrap' => false,
			'pjax' => false,
			'columns' => require(__DIR__ . '/_columns_perc.php'),
			'panelBeforeTemplate' => '',
			'striped' => true,
			'condensed' => true,
			'bordered' => true,
			'responsive' => true,
			'panel' => '',
		]);
	?>
<?php } else {?>
	<?=
		GridView::widget([
			'id' => 'crud-datatable',
			'dataProvider' => $dataProvider,
			'responsiveWrap' => false,
			'pjax' => false,
			'columns' => require(__DIR__ . '/_columns.php'),
			'panelBeforeTemplate' => '',
			'striped' => true,
			'condensed' => true,
			'bordered' => true,
			'responsive' => true,
			'panel' => '',
		]);
	?>
<?php } ?>
<?php } ?>


