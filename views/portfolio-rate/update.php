<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PortfolioRate */
?>
<div class="portfolio-rate-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
