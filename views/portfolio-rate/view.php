<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PortfolioRate */
?>
<div class="portfolio-rate-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'portfolio_id',
            'amount',
            'user_id',
            'company_id',
            'created_at',
        ],
    ]) ?>

</div>
