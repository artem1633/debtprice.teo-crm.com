<?php


/* @var $this yii\web\View */
/* @var $model app\models\Debtor */

?>
<div class="debtor-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
