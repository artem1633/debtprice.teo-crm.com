<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Debtor */
?>
<div class="debtor-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'outer_id',
                'birthday_date',
                'age',
                'gender',
                'marital_status',
                'birthplace:ntext',
                'education',
                'email:email',
                'number_minor_children',
                'owner_property:ntext',
                'fact_residence_postcode',
                'fact_residence_region',
                'fact_residence_city',
                'fact_residence_address',
                'register_region',
                'register_city',
                'register_address',
                'status',
                'workplace:ntext',
                'position:ntext',
                'employer_city',
                'employer_address',
                'period_last_job',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), 'error');
        echo $e->getMessage();
    } ?>

</div>
