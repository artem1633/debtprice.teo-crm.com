<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CompanyFile */

?>
<div class="company-file-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
