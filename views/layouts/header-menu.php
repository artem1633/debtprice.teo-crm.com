<?php

use app\admintheme\widgets\Menu;
use app\models\Portfolio;


$portfolioQuery = Portfolio::find();

$portfolioQuery->andWhere(['or', ['stage' => null], ['stage' => 1]]);

$statuses = [];

$portfolioQuery->andFilterWhere(['status' => Portfolio::STATUS_RATING]);


$portfolios = $portfolioQuery->all();


?>

<div id="sidebar" class="sidebar">
    <?php if (Yii::$app->user->isGuest == false): ?>
        <?php
        try {
            echo Menu::widget(
                [
                    'encodeLabels' => false,
                    'options' => ['class' => 'nav'],
                    'items' => [
                        //                    ['label' => 'Пользователи', 'icon' => 'fa fa-user-o', 'url' => ['/user'],],
                        // ['label' => 'Кабинет администратора', 'url' => ['/admin-files'], 'icon' => 'fa fa-file', 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                        ['label' => 'Мои сделки', 'url' => ['/portfolio-won'], 'icon' => 'fa fa-star'],
                        ['label' => 'ТОРГИ', 'url' => ['/portfolio/index'], 'icon' => (count($portfolios) > 0) ? 'fa fa-hammer text-warning' : 'fa fa-hammer'],
                        ['label' => 'История торгов', 'url' => ['portfolio-history/index'], 'icon' => 'fa fa-archive'],
                        ['label' => 'Мои портфели', 'url' => ['/portfolio/my'], 'icon' => 'fa fa-archive'],
                        ['label' => 'Реестр дел', 'url' => ['/loan'], 'icon' => 'fa fa-money', 'visible' => Yii::$app->user->identity->company->api_key ?? false],
                        [
                            'label' => 'Моя компания',
                            'url' => ['/company'],
                            'icon' => 'fa fa-users',
                            'visible' => Yii::$app->user->identity->isSuperAdmin() == false
                        ],
                        ['label' => 'Пользователи', 'url' => ['/user'], 'icon' => 'fa fa-users'],
                        [
                            'label' => 'Компании',
                            'icon' => 'fa fa-users',
                            'url' => ['/company'],
                            'visible' => Yii::$app->user->identity->isSuperAdmin()
                        ],
                        ['label' => 'Обратная связь', 'url' => ['/ticket'], 'icon' => 'fa fa-question'],
                        [
                            'label' => 'Справочники',
                            'icon' => 'fa fa-book',
                            'url' => '#',
                            'options' => ['class' => 'has-sub'],
                            'items' => [
                                ['label' => 'Должности', 'url' => ['/position']],
                                [
                                    'label' => 'Подписки',
                                    'url' => ['/subscription'],
                                    'visible' => Yii::$app->user->identity->isSuperAdmin()
                                ],
                                [
                                    'label' => 'Валюта',
                                    'url' => ['/currency'],
                                    'visible' => Yii::$app->user->identity->isSuperAdmin()
                                ],
                                [
                                    'label' => 'Кред. продукты',
                                    'url' => ['/product-type'],
                                    'visible' => Yii::$app->user->identity->isSuperAdmin()
                                ],
                                [
                                    'label' => 'Статусы должника',
                                    'url' => ['/debtor-status'],
                                    'visible' => Yii::$app->user->identity->isSuperAdmin()
                                ],
                                [
                                    'label' => 'Документация АПИ',
                                    'url' => ['/api/doc'],
                                    'visible' => Yii::$app->user->identity->company->api_key ?? false
                                ],
                            ],
                        ],
                    ],
                ]
            );
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'error');
            echo $e->getMessage();
        }
        ?>
    <?php endif; ?>
</div>
