<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AutoRateLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Auto Rate Logs';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<style>
    table thead tr th {
        padding: 7px 6px !important;
    }
</style>

<div class="auto-rate-log-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-log-datatable',
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'rowOptions' => ['style' => 'font-size: 11px;'],
            'headerRowOptions' => ['style' => 'font-size: 11px;'],
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
                    ['role'=>'modal-remote','title'=> 'Create new Auto Rate Logs','class'=>'btn btn-default']).
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'.
                    '{export}'
                ],
            ],          
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
