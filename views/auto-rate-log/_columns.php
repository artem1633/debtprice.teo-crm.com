<?php

use yii\helpers\ArrayHelper;
use app\models\AutoRateLog;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '20px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'action',
        'value' => function($model){
            return ArrayHelper::getValue(AutoRateLog::actionLabels(), $model->action);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'value' => 'company.name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'value' => 'user.email'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'amount',
        'value' => function($model){
            return number_format($model->amount, 0, 0, ' ').' ₽';
        }
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'od',
         'label' => '% ОД',
         'content' => function($model){
//            return number_format($model->od, 2, 0, ' ').' %';
            return $model->osz.' %';
         }
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'osz',
         'label' => '% ОСЗ',
         'content' => function($model){
//             return number_format(12.34, 2, 0, ' ').' %';
             return $model->osz.' %';
         }
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'datetime',
         'content' => function($model){
             $date = Yii::$app->formatter->asDate($model->datetime, 'php:d M Y');
             $time = Yii::$app->formatter->asDate($model->datetime, 'php:H:i:s');

             return $date.'<br/><span class="time-s">'.$time.'</span>';
         },
     ],

];   