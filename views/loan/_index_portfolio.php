<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LoanSearch */
/* @var $portfolio app\models\Portfolio */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Дела портфеля ' . $portfolio->name;
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="loan-portfolio-index">
    <div id="ajaxCrudDatatable">
        <?php
        try {
            echo GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'columns' => require(__DIR__ . '/_columns.php'),
                'panelBeforeTemplate' =>
//                    Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
//                        [
//                            'role' => 'modal-remote',
//                            'title' => 'Добавить дело',
//                            'class' => 'btn btn-success'
//                        ]) . '&nbsp;' .
                    Html::a('<i class="fa fa-repeat"></i>', [''],
                        ['data-pjax' => 1, 'class' => 'btn btn-white', 'title' => 'Обновить']),
//                'toolbar' => [
//                    [
//                        'content' =>
//                            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
//                                [
//                                    'role' => 'modal-remote',
//                                    'title' => 'Create new Loans',
//                                    'class' => 'btn btn-default'
//                                ]) .
//                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
//                                ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset Grid']) .
//                            '{toggleData}' .
//                            '{export}'
//                    ],
//                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'inverse',
                    'heading' => '<i class="glyphicon glyphicon-list"></i> Реестр дел портфеля "' . $portfolio->name . '"',
//                    'after' => BulkButtonWidget::widget([
//                            'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
//                                ["bulk-delete"],
//                                [
//                                    "class" => "btn btn-danger btn-xs",
//                                    'role' => 'modal-remote-bulk',
//                                    'data-confirm' => false,
//                                    'data-method' => false,// for overide yii data api
//                                    'data-request-method' => 'post',
//                                    'data-confirm-title' => 'Are you sure?',
//                                    'data-confirm-message' => 'Are you sure want to delete this item'
//                                ]) .
//                            Html::a('<i class="glyphicon glyphicon-tasks"></i>&nbsp; Сформировать портфель',
//                                ["bulk-create-portfolio"],
//                                [
//                                    "class" => "btn btn-success btn-xs",
//                                    'role' => 'modal-remote-bulk',
//                                    'data-confirm' => false,
//                                    'data-method' => false,// for overide yii data api
//                                    'data-request-method' => 'post',
//                                    'data-confirm-title' => 'Вы уверены?',
//                                    'data-confirm-message' => 'Подтвердите создание портфеля',
//                                    'title' => 'Сформировать новый потрфель из выбранных дел',
//                                    'style' => 'margin-left: 10px;',
//                                ]) .
//                            Html::a('<i class="glyphicon glyphicon-play"></i>&nbsp; Добавить в портфель',
//                                ["bulk-add-to-portfolio"],
//                                [
//                                    "class" => "btn btn-info btn-xs",
//                                    'role' => 'modal-remote-bulk',
//                                    'data-confirm' => false,
//                                    'data-method' => false,// for overide yii data api
//                                    'data-request-method' => 'post',
//                                    'data-confirm-title' => 'Вы уверены?',
//                                    'data-confirm-message' => 'Подтвердите добавление в портфель',
//                                    'title' => 'Добавить выбранные дела в портфель',
//                                    'style' => 'margin-left: 10px;',
//                                ]),
//                        ]) .
//                        '<div class="clearfix"></div>',
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'error');
            echo $e->getMessage();
        } ?>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
