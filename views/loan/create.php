<?php

/* @var $this yii\web\View */
/* @var $loan_model app\models\Loan */
/* @var $debtor_model app\models\Debtor */

$this->title = 'Добавление дела';

?>
<div class="loan-create container  panel panel-default">
    <?= $this->render('_form', [
        'loan_model' => $loan_model,
        'debtor_model' => $debtor_model,
    ]) ?>
</div>
