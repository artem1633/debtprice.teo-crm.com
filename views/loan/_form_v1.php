<?php

use app\models\Currency;
use app\models\Debtor;
use app\models\Portfolio;
use app\models\ProductType;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Loan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'portfolio_id')->widget(Select2::class, [
                'data' => (new Portfolio())->getListForUser(),
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'options' => [
                    'prompt' => 'Выберите значение...'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'debtor_id')->widget(Select2::class, [
                'data' => (new Debtor())->getList(),
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'options' => [
                    'prompt' => 'Выберите значение...'
                ]
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'date')->input('date') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'type')->dropDownList((new ProductType())->getList(), [
                'prompt' => 'Выберите значение...'
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'currency_id')->dropDownList((new Currency())->getList(), [
                'prompt' => 'Выберите значение...'
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name_collateral')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'assessment_collateral')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'primary_amount_debt')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'percent_amount_debt')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'other_amount_debt')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'total_amount_debt')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'period')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'expired_days')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'amount_last_payment')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'total_payment')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'last_payment_date')->input('date') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'last_contact_date')->input('date') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'total_number_payment')->textInput() ?>
        </div>
    </div>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
