<?php

use app\models\Portfolio;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Loan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'portfolio_id')->widget(Select2::class, [
        'data' => (new Portfolio())->getListForUser(),
        'options' => [
            'prompt' => 'Выберите значение...'
        ]
    ]) ?>

    <?= $form->field($model, 'pks')->hiddenInput()->label(false) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
