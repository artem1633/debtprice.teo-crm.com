<?php

use app\models\Loan;
use app\models\Portfolio;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign' => 'middle',
//        'template' => '{view} {update} {exclude} {delete}',
//        'urlCreator' => function ($action, $model, $key, $index) {
//            return Url::to([$action, 'id' => $key]);
//        },
//
//        'buttons' => [
//            'exclude' => function ($action, Loan $model, $key) {
//                return Html::a('<span class="glyphicon glyphicon-remove"></span>', ['exclude-loan', 'id' => $model->id],
//                    [
//                        'role' => 'modal-remote',
//                        'title' => 'Исключить из портфеля',
//                        'data-confirm' => false,
//                        'data-method' => false,// for overide yii data api
//                        'data-request-method' => 'post',
//                        'data-toggle' => 'tooltip',
//                        'data-confirm-title' => 'Исключение дела',
//                        'data-confirm-message' => 'Подтвердите исключение дела из портфеля.',
//                        'hidden' => !$model->portfolio_id,
//                    ]);
//            }
//        ],
//
//        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Просмотр', 'data-toggle' => 'tooltip'],
//        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Редактировать', 'data-toggle' => 'tooltip'],
//        'deleteOptions' => [
//            'role' => 'modal-remote',
//            'title' => 'Удалить',
//            'data-confirm' => false,
//            'data-method' => false,// for overide yii data api
//            'data-request-method' => 'post',
//            'data-toggle' => 'tooltip',
//            'data-confirm-title' => 'Удаление записи',
//            'data-confirm-message' => 'Подтвердите удаление дела из базы.'
//        ],
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'vAlign' => 'middle',
        'label' => '',
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'content' => function (Loan $model) {
            $output = Html::a('<i class="fa fa-eye text-success" style="font-size: 16px;"></i>',
                ['/loan/view', 'id' => $model->id], [
                    'title' => 'Просмотр',
                    'data-toggle' => 'tooltip',
                ]);

            $portfolio_status = $model->portfolio->status ?? null;
            if ($portfolio_status == Portfolio::STATUS_DRAFT || !$model->portfolio_id) {
                $output .= '&nbsp;' . Html::a('<i class="fa fa-edit text-success" style="font-size: 16px;"></i>',
                        ['/loan/update', 'id' => $model->id], [
//                                'role' => 'modal-remote',
                            'title' => 'Редактировать',
                            'data-toggle' => 'tooltip',
                        ]);
                if ($model->portfolio_id){
                    $output .= '&nbsp;' . Html::a('<i class="fa fa-remove text-success" style="font-size: 16px;"></i>',
                        ['/loan/exclude-loan', 'id' => $model->id], [
                            'role' => 'modal-remote',
                            'title' => 'Исключить из портфеля',
                            'data-confirm' => false,
                            'data-method' => false,// for overide yii data api
                            'data-request-method' => 'post',
                            'data-toggle' => 'tooltip',
                            'data-confirm-title' => 'Исключение дела',
                            'data-confirm-message' => 'Подтвердите исключение дела из портфеля.',
                        ]);
                }

                $output .= '&nbsp;' . Html::a('<i class="fa fa-trash text-success" style="font-size: 16px;"></i>',
                        ['/loan/delete', 'id' => $model->id], [
                            'role' => 'modal-remote',
                            'title' => 'Удалить',
                            'data-confirm' => false,
                            'data-method' => false,// for overide yii data api
                            'data-request-method' => 'post',
                            'data-toggle' => 'tooltip',
                            'data-confirm-title' => 'Удаление дела',
                            'data-confirm-message' => 'Данная операция необратима! Подтвердите удаление дела.',
                        ]);
            }

            return $output;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'portfolio_id',
        'content' => function (Loan $model) {
            return $model->portfolio->name ?? 'Без портфеля';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'debtor_id',
        'content' => function (Loan $model) {
            return $model->debtor->outer_id ?? null;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'date',
        'format' => 'date'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'type',
        'content' => function (Loan $model) {
            return $model->loanType->outer_id ?? null;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'currency_id',
        'content' => function (Loan $model) {
            return $model->currency->name ?? null;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name_collateral',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'assessment_collateral',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'primary_amount_debt',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'percent_amount_debt',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'other_amount_debt',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'total_amount_debt',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'period',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'expired_days',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'amount_last_payment',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'total_payment',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'last_payment_date',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'last_contact_date',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'total_number_payment',
    // ],

];   