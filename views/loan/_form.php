<?php

use app\models\Currency;
use app\models\DebtorStatus;
use app\models\Portfolio;
use app\models\ProductType;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $loan_model app\models\Loan */
/* @var $debtor_model app\models\Debtor */
/* @var $form yii\widgets\ActiveForm */

?>


<div class="loan-form">
    <h2><?= Html::encode($this->title) ?></h2>
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($loan_model, 'portfolio_id')->widget(Select2::class, [
                'data' => (new Portfolio())->getListForUser(),
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'options' => [
                    'prompt' => 'Выберите значение...'
                ]
            ]) ?>
        </div>
    </div>
<!--    Должник-->
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($debtor_model, 'outer_id')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($debtor_model, 'birthday_date')->input('date') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($debtor_model, 'age')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($debtor_model, 'gender')->dropDownList($debtor_model->getGenderList(), [
                'prompt' => 'Выберите значение...'
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($debtor_model, 'marital_status')->dropDownList($debtor_model->getMaritalStatuses(), [
                'prompt' => 'Выберите значение...'
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($debtor_model, 'birthplace')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($debtor_model, 'education')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($debtor_model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($debtor_model, 'number_minor_children')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($debtor_model, 'owner_property')->textarea(['rows' => 6]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($debtor_model, 'fact_residence_postcode')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($debtor_model, 'fact_residence_region')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($debtor_model, 'fact_residence_city')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($debtor_model, 'fact_residence_address')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($debtor_model, 'register_region')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($debtor_model, 'register_city')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($debtor_model, 'register_address')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($debtor_model, 'status')->dropDownList((new DebtorStatus())->getList(), [
                'prompt' => 'Выберите значение...'
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($debtor_model, 'workplace')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($debtor_model, 'position')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($debtor_model, 'period_last_job')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($debtor_model, 'employer_city')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($debtor_model, 'employer_address')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
<!--    Кредит-->
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($loan_model, 'date')->input('date') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($loan_model, 'type')->dropDownList((new ProductType())->getList(), [
                'prompt' => 'Выберите значение...'
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($loan_model, 'currency_id')->dropDownList((new Currency())->getList(), [
                'prompt' => 'Выберите значение...'
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($loan_model, 'name_collateral')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($loan_model, 'assessment_collateral')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($loan_model, 'primary_amount_debt')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($loan_model, 'percent_amount_debt')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($loan_model, 'other_amount_debt')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($loan_model, 'total_amount_debt')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($loan_model, 'period')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($loan_model, 'expired_days')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($loan_model, 'amount_last_payment')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($loan_model, 'total_payment')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($loan_model, 'last_payment_date')->input('date') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($loan_model, 'last_contact_date')->input('date') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($loan_model, 'total_number_payment')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
                <?= Html::submitButton($loan_model->isNewRecord ? 'Добавить' : 'Сохранить',
                    ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($loan_model, 'debtor_id')->hiddenInput()->label(false) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
