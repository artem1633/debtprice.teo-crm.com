<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $loan_model app\models\Loan */
/* @var $debtor_model app\models\Debtor */
?>
<div class="loan-view">
    <div class="debtor-info">
        <?php
        try {
            echo DetailView::widget([
                'model' => $debtor_model,
                'attributes' => [
                    'id',
                    'outer_id',
                    'birthday_date',
                    'age',
                    [
                        'attribute' => 'gender',
                        'value' => $debtor_model->genderLabel
                    ],
                    [
                        'attribute' => 'marital_status',
                        'value' => $debtor_model->maritalLabel
                    ],
                    'birthplace:ntext',
                    'education',
                    'email:email',
                    'number_minor_children',
                    'owner_property:ntext',
                    'fact_residence_postcode',
                    'fact_residence_region',
                    'fact_residence_city',
                    'fact_residence_address',
                    'register_region',
                    'register_city',
                    'register_address',
                    [
                        'attribute' => 'status',
                        'value' => $debtor_model->debtorStatus->name ?? null
                    ],
                    'workplace:ntext',
                    'position:ntext',
                    'employer_city',
                    'employer_address',
                    'period_last_job',
                ],
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'error');
            echo $e->getMessage();
        } ?>
    </div>
    <div class="loan-info">
        <?php
        try {
            echo DetailView::widget([
                'model' => $loan_model,
                'attributes' => [
                    'id',
                    'date',
                    [
                        'attribute' => 'type',
                        'value' => $loan_model->loanType->name ?? null
                    ],
                    [
                        'attribute' => 'currency_id',
                        'value' => $loan_model->currency->name ?? null
                    ],
                    'name_collateral',
                    'assessment_collateral',
                    'primary_amount_debt',
                    'percent_amount_debt',
                    'other_amount_debt',
                    'total_amount_debt',
                    'period',
                    'expired_days',
                    'amount_last_payment',
                    'total_payment',
                    'last_payment_date',
                    'last_contact_date',
                    'total_number_payment',
                ],
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'error');
            echo $e->getMessage();
        } ?>
    </div>


</div>
