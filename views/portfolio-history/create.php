<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Portfolio */

$this->title = 'Создать портфель';

?>
<?= $this->render('_form', [
	'model' => $model,
]) ?>
