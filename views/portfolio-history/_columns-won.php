<?php
use app\models\PortfolioFileSearch;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Portfolio;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
        'hAlign' => GridView::ALIGN_CENTER,
        'headerOptions' => ['style' => 'text-align: center;'],
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class'=>'kartik\grid\ExpandRowColumn',
        'width'=>'50px',
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detailUrl' => function($model){
//            return Url::toRoute(['view', 'id' => $model->id]);
        },
        'detail'=>function ($model, $key, $index, $column) {
            $filesSearchModel = new PortfolioFileSearch();
            $filesDataProvider = $filesSearchModel->search([]);
            $filesDataProvider->query->andWhere(['portfolio_id' => $model->id]);

            return \Yii::$app->controller->renderPartial('view', [
                'model' => $model,
                'filesSearchModel' => $filesSearchModel,
                'filesDataProvider' => $filesDataProvider,
            ]);
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'hAlign' => GridView::ALIGN_RIGHT,
        'headerOptions' => ['style' => 'text-align: center;'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'start_rate',
        'hAlign' => GridView::ALIGN_RIGHT,
        'format' => 'raw',
        'headerOptions' => ['style' => 'text-align: center;'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Текущая ставка',
        'hAlign' => GridView::ALIGN_RIGHT,
        'format' => 'raw',
        'headerOptions' => ['style' => 'text-align: center;'],
        'value' => function($model) {
            //if($model->company_id !== Yii::$app->user->identity->company_id) {
            $od = $model->sum_od;
            $gsc = $model->general_sum_credit;
            $st = $model->currentRateAmount;
            if((!empty($st))&&(!empty($od))&&(!empty($gsc))) {
                $res = $st/$od*100;
                $res = round($res, 2);
                $st = round($st, 0);
                $st1 = number_format(intval($st), 0, '', ' ');
                $res = number_format(floatval($res), 2, ',', ' ');
                $resOZ = $st/$gsc*100;
                $resOZ = round($resOZ, 2);
                $resOZ = number_format(floatval($resOZ), 2, ',', ' ');
                //$st = number_format(floatval($st), 2, ',', ' ');

                return $st1.' ₽ <br/><span class="od-oz">'.$res. '% ОД <br/>'.$resOZ.'% ОСЗ </span>';
            } else {
                $res = '';
                return $res;
            }
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'stage',
        'hAlign' => GridView::ALIGN_RIGHT,
        'headerOptions' => ['style' => 'text-align: center;'],
        'content' => function($model){
            return ArrayHelper::getValue(Portfolio::stageLabels(), $model->stage);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'count_actions',
        'hAlign' => GridView::ALIGN_RIGHT,
        'headerOptions' => ['style' => 'text-align: center;'],
        'format' => 'integer',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sum_od',
        'hAlign' => GridView::ALIGN_RIGHT,
        'headerOptions' => ['style' => 'text-align: center;'],
        'format' => ['currency', 'rub'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sum_percents',
        'hAlign' => GridView::ALIGN_RIGHT,
        'headerOptions' => ['style' => 'text-align: center;'],
        'format' => 'decimal',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'datetime_start',
//        'hAlign' => GridView::ALIGN_RIGHT,
        'format' => ['date', 'php:d M Y H:i:s'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'datetime_end',
        'format' => ['date', 'php:d M Y H:i:s'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'format' => ['date', 'php:d M Y H:i:s'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'value' => 'company.name',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'count_ip',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'count_il',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'is_pledge',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'general_payments',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'count_action_before_90_days',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'count_action_90_180_days',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'count_action_181_365_days',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'count_action_366_730_days',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'count_action_731_1095_days',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'count_action_over_1096_days',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'product',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'location',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'pledge_name',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'count_placements_ka',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'status',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'datetime_start',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'datetime_end',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'winner_id',
    // ],
];