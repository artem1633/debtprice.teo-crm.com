<style type="text/css">
	#crud-datatable-filters {
		display: none;	
	}
	#crud-datatable-container .table-condensed > thead > tr > th:nth-child(2) {
		padding: 7px;
		padding-left: 10px;
		padding-right: 1px;
	}
	#crud-datatable-container .table-condensed > tbody > tr > td:nth-child(2) {
		padding: 7px;
		padding-left: 10px;
		padding-right: 1px;
	}
</style>

<script type="text/javascript"> 

$(function(){

    function initFormatting()
    {
        $('#crud-datatable-container tbody tr td[data-col-seq="4"]').hide();
        $('#crud-datatable-container thead tr th[data-col-seq="4"]').hide();
        $('#crud-datatable-container tbody tr td[data-col-seq="5"]').addClass('name-glyph');
        setInterval(function() {

            $('#crud-datatable-container tbody tr td').each(function() {
                var ht = $(this).text();
                if(ht.indexOf('<br/>') + 1) {
                    $(this).html(ht);
                }
            });
            $('#crud-datatable-container thead tr th a').each(function() {
                if($(this).text()=='ID' || $(this).text()=='#') {
                    $(this).parent().html('#');
                    $(this).parent().css('width', '2%');

                }
            });
        }, 500);
        $('#crud-datatable-container tbody tr td[data-col-seq="5"]').click(function() {
            $(this).siblings().children().children().children('.glyphicon').trigger("click");
        });
    }

    initFormatting();

    $(document).on('pjax:complete', function(event) {
        initFormatting();
    });

});
	
</script>

<?php

use app\models\PortfolioFileSearch;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Portfolio;
	
	return [
		[
			'class' => 'kartik\grid\CheckboxColumn',
			'width' => '20px',
			'checkboxOptions' => function ($model) {
				if ($model->company_id != Yii::$app->user->identity->company_id && Yii::$app->user->identity->isSuperAdmin() == false) {
					return ['hidden' => 'disabled'];
				}
			}
		],
		/*[
			'class' => 'kartik\grid\SerialColumn',
			'width' => '30px',
		],*/
		[
			'class'=>'\kartik\grid\DataColumn',
			'attribute'=>'id',
            'hAlign' => GridView::ALIGN_CENTER,
            'vAlign' => GridView::ALIGN_MIDDLE,
		],
		[
			'class' => '\kartik\grid\DataColumn',
			'vAlign' => 'middle',
            'format' => 'raw',
            'label' => 'Торги',
			'contentOptions' => ['style' => 'white-space: nowrap;'],
			'content' => function (Portfolio $model) {
				$output = '';
				// if(Yii::$app->user->identity->isSuperAdmin() || $model->company_id == Yii::$app->user->identity->company_id){
				$output .= "&nbsp;" . Html::a('<i class="fa fa-tasks text-info" style="font-size: 16px;"></i>',
						['rate-history', 'portfolio_id' => $model->id], [
							'role' => 'modal-remote',
							'title' => 'История ставок',
							'data-toggle' => 'tooltip',
						]) . "&nbsp;";
				// }

				if (Yii::$app->user->identity->isSuperAdmin() == false && $model->company_id != Yii::$app->user->identity->company_id) {
					if ($model->status == Portfolio::STATUS_RATING) {
						$output .= "&nbsp;" . Html::a('<i class="fa fa-hammer text-warning" style="font-size: 16px;"></i>',
								['set-rate', 'portfolio_id' => $model->id], [
									'role' => 'modal-remote',
									'title' => 'Поставить ставку',
									'data-toggle' => 'tooltip',
									'data-confirm' => false,
									'data-method' => false,// for overide yii data api
									'data-request-method' => 'post',
								]) . "&nbsp;";
					} else {
						$output .= "&nbsp;" . Html::a('<i class="fa fa-hammer" style="font-size: 16px; color: #808080;"></i>',
								'#', [
									'title' => 'Аукцион еще не начался или не прошел модерацию',
									'data-toggle' => 'tooltip',
									'data-pjax' => '0',
									'onclick' => "event.preventDefault(); alert('Аукцион еще не начался или не прошел модерацию!');"
								]) . "&nbsp;";
					}
				}

				if (Yii::$app->user->identity->isSuperAdmin() && $model->status == Portfolio::STATUS_MODERATION && $model->company->moderated == 1) {
					$output .= "&nbsp;" . Html::a('<i class="fa fa-thumbs-o-up text-success" style="font-size: 18px;"></i>',
							['approve', 'portfolio_id' => $model->id], [
								'role' => 'modal-remote',
								'title' => 'Одобрить',
								'data-toggle' => 'tooltip',
								'data-confirm' => false,
								'data-method' => false,// for overide yii data api
								'data-request-method' => 'post',
								'data-confirm-title' => 'Вы уверены?',
								'data-confirm-message' => 'Вы действительно хотите одобрить данную запись?'
							]) . "&nbsp;";
				}


				if ($model->status == $model::STATUS_DRAFT && ($model->company_id == Yii::$app->user->identity->company_id || Yii::$app->user->identity->isSuperAdmin()) && $model->company->moderated == 1) {
					$output .= "&nbsp;" . Html::a('<i class="fa fa-check-circle text-success" style="font-size: 18px;"></i>',
							['to-moderation', 'id' => $model->id], [
								'role' => 'modal-remote',
								'title' => 'Опубликовать',
								'data-toggle' => 'tooltip',
								'data-confirm' => false,
								'data-method' => false,// for overide yii data api
								'data-request-method' => 'post',
								'data-confirm-title' => 'Вы уверены?',
								'data-confirm-message' => 'Вы действительно хотите опубликовать портфель?'
							]) . "&nbsp;";
					if($model->disapprove_text != null){
                        $output .= "&nbsp;" . Html::a('<i class="fa fa-exclamation-triangle text-danger" style="font-size: 18px;"></i>',
                                ['disapprove-text', 'id' => $model->id], [
                                    'role' => 'modal-remote',
                                    'title' => 'Модерация не пройдена',
                                ]) . "&nbsp;";
                    }
				}

                if($model->company->moderated != 1){
                    $output .= "&nbsp;" . Html::a('<i class="fa fa-exclamation text-danger" style="font-size: 18px;"></i>',
                            '#', [
                                'title' => 'Компания не одобрена к торгам',
                            ]) . "&nbsp;";
                }



				$apiKey = $model->getApiKey();
				if(($apiKey['api_key'])||(Yii::$app->user->identity->isSuperAdmin())) {
					$output .= "&nbsp;" . Html::a('<i class="fa fa-eye text-success" style="font-size: 18px;"></i>',
							['view-loans', 'id' => $model->id], [
								'title' => 'Просмотреть дела',
								'data-toggle' => 'tooltip',
								'data-pjax' => 0,
							]) . "&nbsp;";
				}

				return $output;
			},
		],
		[
			'class' => 'kartik\grid\ActionColumn',
			'dropdown' => false,
			'vAlign' => 'middle',
            'urlCreator' => function ($action, $model, $key, $index) {
				return Url::to([$action, 'id' => $key]);
			},
			'contentOptions' => ['style' => 'white-space: nowrap;'],
	//        'visible' => Yii::$app->user->identity->isSuperAdmin(),
			'template' => '{update}{delete}',
			'buttons' => [
				'delete' => function ($url, Portfolio $model) {
					if (Yii::$app->user->identity->isSuperAdmin()) {
						//|| ($model->status == $model::STATUS_DRAFT && $model->company_id == Yii::$app->user->identity->company_id)) {
						return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
							'role' => 'modal-remote',
							'title' => 'Удалить',
							'data-confirm' => false,
							'data-method' => false,// for overide yii data api
							'data-request-method' => 'post',
							'data-confirm-title' => 'Вы уверены?',
							'data-confirm-message' => 'Вы действительно хотите удалить данную запись?'
						]);
					}
				},
				'update' => function ($url, $model) {
					if (Yii::$app->user->identity->isSuperAdmin()
						|| ($model->status == $model::STATUS_DRAFT && $model->company_id == Yii::$app->user->identity->company_id)) {
						return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
								'role' => 'modal-remote',
								'title' => 'Изменить',
								'data-confirm' => false,
								'data-method' => false,// for overide yii data api
								'data-request-method' => 'post',
							]) . "&nbsp;";
					}
				},
			],
		],

		[
			'class' => 'kartik\grid\ExpandRowColumn',
			'width' => '50px',
            'format' => 'raw',
            'value' => function ($model, $key, $index, $column) {
				return GridView::ROW_COLLAPSED;
			},
			'detailUrl' => function ($model) {
	//            return Url::toRoute(['view', 'id' => $model->id]);
			},
			'detail' => function ($model, $key, $index, $column) {
				$filesSearchModel = new PortfolioFileSearch();
				$filesDataProvider = $filesSearchModel->search([]);
				$filesDataProvider->query->andWhere(['portfolio_id' => $model->id]);

				return \Yii::$app->controller->renderPartial('view', [
					'model' => $model,
					'filesSearchModel' => $filesSearchModel,
					'filesDataProvider' => $filesDataProvider,
				]);
			},
			'headerOptions' => ['class' => 'kartik-sheet-style'],
			'expandOneOnly' => true
		],
		[
			'class' => '\kartik\grid\DataColumn',
			'attribute' => 'name',
		],
		/*[
			'class' => '\kartik\grid\DataColumn',
			'attribute' => 'start_rate',
			'format' => ['currency', 'rub'],
		],*/
		[
			'class' => '\kartik\grid\DataColumn',
			'label' => 'Текущая ставка',
            'hAlign' => GridView::ALIGN_RIGHT,
            'format' => 'raw',
            'headerOptions' => ['style' => 'text-align: center;'],
            'value' => function($model) {
                //if($model->company_id !== Yii::$app->user->identity->company_id) {
                $od = $model->sum_od;
                $gsc = $model->general_sum_credit;
                $st = $model->currentRateAmount;
                if((!empty($st))&&(!empty($od))&&(!empty($gsc))) {
                    $res = $st/$od*100;
                    $res = round($res, 2);
                    $st = round($st, 0);
                    $st1 = number_format(intval($st), 0, '', ' ');
                    $res = number_format(floatval($res), 2, ',', ' ');
                    $resOZ = $st/$gsc*100;
                    $resOZ = round($resOZ, 2);
                    $resOZ = number_format(floatval($resOZ), 2, ',', ' ');
                    //$st = number_format(floatval($st), 2, ',', ' ');

                    return $st1.' ₽ <br/><span class="od-oz">'.$res. '% ОД <br/>'.$resOZ.'% ОСЗ </span>';
                } else {
                    $res = '';
                    return $res;
                }
            }
		],
		/*[
			'class' => '\kartik\grid\DataColumn',
			'attribute' => 'company_id',
			'label' => 'Компания',
			'value' => 'company.name'
		],*/
		
		[
			'class' => '\kartik\grid\DataColumn',
			'attribute' => 'count_actions',
            'format' => 'raw',
            'hAlign' => GridView::ALIGN_RIGHT,
            'headerOptions' => ['style' => 'text-align: center;'],
			'value' => function($model) {
				$od = $model->count_actions;
				$od = round($od, 0);
				$od1 = number_format(intval($od), 0, '', ' ');
				
				return '<span class="gsc">'.$od1. '</span><br/>';
			}
		],
		[
			'class' => '\kartik\grid\DataColumn',
			'attribute' => 'sum_od',
            'format' => 'raw',
            'hAlign' => GridView::ALIGN_RIGHT,
            'headerOptions' => ['style' => 'text-align: center;'],
			'value' => function($model) {
				$od = $model->sum_od;
				$od = round($od, 0);
				$od1 = number_format(intval($od), 0, '', ' ');
				
				return '<span class="gsc">'.$od1. ' ₽</span><br/>';
			}
		],
		[
			'class' => '\kartik\grid\DataColumn',
			'attribute' => 'general_sum_credit',
            'format' => 'raw',
            'hAlign' => GridView::ALIGN_RIGHT,
            'headerOptions' => ['style' => 'text-align: center;'],
			'value' => function($model) {
				$od = $model->general_sum_credit;
				$od = round($od, 0);
				$od1 = number_format(intval($od), 0, '', ' ');
				
				return '<span class="gsc">'.$od1. ' ₽</span><br/>';
			}
		],
		[
			'class' => '\kartik\grid\DataColumn',
			'attribute' => 'sum_percents',
            'format' => 'raw',
            'hAlign' => GridView::ALIGN_RIGHT,
            'headerOptions' => ['style' => 'text-align: center;'],
			'value' => function($model) {
				$od = $model->sum_percents;
				$od = round($od, 0);
				$od1 = number_format(intval($od), 0, '', ' ');
				
				return '<span class="gsc">'.$od1. ' ₽</span><br/>';
			}
		],
		[
			'class' => '\kartik\grid\DataColumn',
			'label' => 'Время торгов',
			'attribute' => 'datetime_end',
            'format' => 'raw',
            'hAlign' => GridView::ALIGN_RIGHT,
            'headerOptions' => ['style' => 'text-align: center;'],
			'value' => function($model) {
				$od = $model->datetime_start;
				$od1 = $model->datetime_end;
				$_monthsList = array("Jan" => "янв.", "Feb" => "фев.", 
					"Mar" => "мар.", "Apr" => "апр.", "May" => "мая", "Jun" => "июн.", 
					"Jul" => "июл.", "Aug" => "авг.", "Sep" => "сен.",
					"Oct" => "окт.", "Nov" => "ноя.", "Dec" => "дек.");
				$currentDate = date("d M Y H:i", strtotime($od));
				$currentDate1 = date("d M Y H:i", strtotime($od1));
				
				$_mD = date("M", strtotime($od));
				$_mD1 = date("M", strtotime($od1));
				$date = date("d M Y", strtotime($od));
				$date1 = date("d M Y", strtotime($od1));
				$time = date("H:i", strtotime($od));
				$time1 = date("H:i", strtotime($od1));
				
				if($date == $date1) {
					$date = str_replace($_mD, " ".$_monthsList[$_mD]." ", $date);
					$currentDate = $date.'<br/><span class="time-s">'.$time.'-'.$time1.'</span>';
					
					return $currentDate;		
				} else {
					$date = str_replace($_mD, " ".$_monthsList[$_mD]." ", $date);
					$date1 = str_replace($_mD, " ".$_monthsList[$_mD]." ", $date1);
					//$currentDate1 = str_replace($_mD1, " ".$_monthsList[$_mD1]." ", $currentDate1);
					
					return $date.'<br/><span class="time-s">'.$time.'-'.$time1.'</span><br/>'.$date1;
				}
			}
		],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'created_at',
            'hAlign' => GridView::ALIGN_RIGHT,
            'headerOptions' => ['style' => 'text-align: center;'],
            'format' => 'raw',
            'content' => function($model){
                $date = Yii::$app->formatter->asDate($model->created_at, 'php:d M Y');
                $time = Yii::$app->formatter->asDate($model->created_at, 'php:H:i:s');

                return $date.'<br/><span class="time-s">'.$time.'</span>';
            }
        ],
	];
