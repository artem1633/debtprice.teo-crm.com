<style type="text/css">
	#crud-datatable-filters {
		display: none;	
	}
</style>

<script type="text/javascript"> 

$(function(){ 
	/*$('#crud-datatable-container thead tr th').each(function() {
		
		$('#crud-datatable-container tbody tr td[data-col-seq="1"]').hide();
		$('#crud-datatable-container thead tr th[data-col-seq="1"]').hide();
		$('#crud-datatable-container tbody tr td[data-col-seq="2"]').addClass('name-glyph');
		
		$('#crud-datatable-container tbody tr .name-glyph').click(function() {
			$(this).siblings().children().children().children('.glyphicon').trigger("click");
			$('.kv-expand-detail-row td .kv-expanded-row').css("height", "100%");
		});
	});*/

	function initFormatting()
    {
        $('#crud-datatable-container thead tr th').each(function() {
            if($('div').hasClass('admin-true')) {
                $('#crud-datatable-container tbody tr td[data-col-seq="3"]').hide();
                $('#crud-datatable-container thead tr th[data-col-seq="3"]').hide();
                $('#crud-datatable-container tbody tr td[data-col-seq="4"]').addClass('name-glyph');
            } else {
                $('#crud-datatable-container tbody tr td[data-col-seq="1"]').hide();
                $('#crud-datatable-container thead tr th[data-col-seq="1"]').hide();
                $('#crud-datatable-container tbody tr td[data-col-seq="2"]').addClass('name-glyph');
            }

        });
        setInterval(function() {

            $('#crud-datatable-container tbody tr td').each(function() {
                var ht = $(this).text();
                if(ht.indexOf('<br/>') + 1) {
                    $(this).html(ht);
                }
            });
            $('#crud-datatable-container thead tr th a').each(function() {
                if($(this).text()=='ID' || $(this).text()=='#') {
                    $(this).parent().html('#');
                    $(this).parent().css('width', '2%');

                }
            });
        }, 500);

        $('#crud-datatable-container tbody tr .name-glyph').click(function() {
            $(this).siblings().children().children().children('.glyphicon').trigger("click");
        });
    }

    initFormatting();

    $(document).on('pjax:complete', function(event) {
        initFormatting();
    });

});
	
</script>

<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\Pjax;
use app\models\PortfolioFileSearch;
use yii\helpers\ArrayHelper;
use app\models\Portfolio;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PortfolioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $adminFilesModel \app\models\AdminFiles */

$this->title = "Мои сделки";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
    <div class="panel panel-inverse portfolio-index">
        <div class="panel-heading">
            <!--        <div class="panel-heading-btn">-->
            <!--        </div>-->
            <h4 class="panel-title">Мои сделки</h4>

        </div>
        <div class="panel-body">
            <?php Pjax::begin(['id' => 'rate-io-pjax', 'enablePushState' => false]) ?>
            <div id="ajaxCrudDatatable">
                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'pjax'=>true,
                    'columns' => [
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'id',
                            'hAlign' => GridView::ALIGN_CENTER,
                            'vAlign' => GridView::ALIGN_MIDDLE,
                        ],
                        // [
                        // 'class'=>'\kartik\grid\DataColumn',
                        // 'attribute'=>'id',
                        // ],
//                        [
//                            'class'=>'kartik\grid\ExpandRowColumn',
//                            'width'=>'50px',
//                            'value'=>function ($model, $key, $index, $column) {
//                                return GridView::ROW_COLLAPSED;
//                            },
//                            'detailUrl' => function($model){
////            return Url::toRoute(['view', 'id' => $model->id]);
//                            },
//                            'detail'=>function ($model, $key, $index, $column) use($adminFilesModel) {
//                                $filesSearchModel = new PortfolioFileSearch();
//                                $filesDataProvider = $filesSearchModel->search([]);
//                                $filesDataProvider->query->andWhere(['portfolio_id' => $model->id]);
//                                $filesDataProvider->query->orderBy('id desc');
//
//                                return \Yii::$app->controller->renderPartial('view', [
//                                    'model' => $model,
//                                    'filesSearchModel' => $filesSearchModel,
//                                    'filesDataProvider' => $filesDataProvider,
//                                    'adminFilesModel' => $adminFilesModel
//                                ]);
//                            },
//                            'headerOptions'=>['class'=>'kartik-sheet-style'],
//                            'expandOneOnly'=>true
//                        ],
                        [
                            'class' => 'kartik\grid\ExpandRowColumn',
                            'width' => '50px',
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'value' => function ($model, $key, $index, $column) {
                                return GridView::ROW_COLLAPSED;
                            },
                            'detailUrl' => function ($model) {
                                //            return Url::toRoute(['view', 'id' => $model->id]);
                            },
                            'detail' => function ($model, $key, $index, $column) {
                                $filesSearchModel = new PortfolioFileSearch();
                                $filesDataProvider = $filesSearchModel->search([]);
                                $filesDataProvider->query->andWhere(['portfolio_id' => $model->id]);

                                return \Yii::$app->controller->renderPartial('view', [
                                    'model' => $model,
                                    'filesSearchModel' => $filesSearchModel,
                                    'filesDataProvider' => $filesDataProvider,
                                ]);
                            },
                            'headerOptions' => ['class' => 'kartik-sheet-style'],
                            'expandOneOnly' => true
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'name',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'hAlign' => GridView::ALIGN_CENTER,
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'start_rate',
                            'hAlign' => GridView::ALIGN_RIGHT,
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'content' => function($model) {
//								$od = $model->start_rate;
//								$od = round($od, 0);
//								$od1 = number_format(intval($od), 0, '', ' ');
//
//								return '<span class="gsc">'.$od1. ' ₽</span><br/>';


                                $od = $model->sum_od;
                                $gsc = $model->general_sum_credit;
                                $st = $model->start_rate;
                                if((!empty($st))&&(!empty($od))&&(!empty($gsc))) {
                                    $res = $st/$od*100;
                                    $res = round($res, 2);
                                    $st = round($st, 0);
                                    $st1 = number_format(intval($st), 0, '', ' ');
                                    $res = number_format(floatval($res), 2, ',', ' ');
                                    $resOZ = $st/$gsc*100;
                                    $resOZ = round($resOZ, 2);
                                    $resOZ = number_format(floatval($resOZ), 2, ',', ' ');
                                    //$st = number_format(floatval($st), 2, ',', ' ');

                                    return $st1.' ₽ <br/><span class="od-oz">'.$res. '% ОД <br/>'.$resOZ.'% ОСЗ </span>';
                                } else {
                                    $res = '';
                                    return $res;
                                }
							}
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            //'format' => ['currency', 'rub'],
                            'label' => 'Текущая ставка',
                            'hAlign' => GridView::ALIGN_RIGHT,
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'value' => function($model) {
                                //if($model->company_id !== Yii::$app->user->identity->company_id) {
                                $od = $model->sum_od;
                                $gsc = $model->general_sum_credit;
                                $st = $model->currentRateAmount;
                                if((!empty($st))&&(!empty($od))&&(!empty($gsc))) {
                                    $res = $st/$od*100;
                                    $res = round($res, 2);
                                    $st = round($st, 0);
                                    $st1 = number_format(intval($st), 0, '', ' ');
                                    $res = number_format(floatval($res), 2, ',', ' ');
                                    $resOZ = $st/$gsc*100;
                                    $resOZ = round($resOZ, 2);
                                    $resOZ = number_format(floatval($resOZ), 2, ',', ' ');
                                    //$st = number_format(floatval($st), 2, ',', ' ');

                                    return $st1.' ₽ <br/><span class="od-oz">'.$res. '% ОД <br/>'.$resOZ.'% ОСЗ </span>';
                                } else {
                                    $res = '';
                                    return $res;
                                }
                            }
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'stage',
                            'content' => function($model){
                                return ArrayHelper::getValue(Portfolio::stageLabels(), $model->stage);
                            },
                        ],
                        [
							'class' => '\kartik\grid\DataColumn',
							'attribute' => 'count_actions',
                            'format' => 'raw',
                            'hAlign' => GridView::ALIGN_RIGHT,
							'headerOptions' => ['style' => 'text-align: center;'],
							'value' => function($model) {
								$od = $model->count_actions;
								$od = round($od, 0);
								$od1 = number_format(intval($od), 0, '', ' ');

								return '<span class="gsc">'.$od1. '</span><br/>';
							}
						],
						[
							'class' => '\kartik\grid\DataColumn',
							'attribute' => 'sum_od',
                            'format' => 'raw',
                            'hAlign' => GridView::ALIGN_RIGHT,
							'headerOptions' => ['style' => 'text-align: center;'],
							'value' => function($model) {
								$od = $model->sum_od;
								$od = round($od, 0);
								$od1 = number_format(intval($od), 0, '', ' ');

								return '<span class="gsc">'.$od1. ' ₽</span><br/>';
							}
						],
                        [
							'class' => '\kartik\grid\DataColumn',
							'attribute' => 'sum_percents',
                            'format' => 'raw',
                            'hAlign' => GridView::ALIGN_RIGHT,
							'headerOptions' => ['style' => 'text-align: center;'],
							'value' => function($model) {
								$od = $model->sum_percents;
								$od = round($od, 0);
								$od1 = number_format(intval($od), 0, '', ' ');

								return '<span class="gsc">'.$od1. ' ₽</span><br/>';
							}
						],
                       /* [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'datetime_start',
                            'format' => ['date', 'php:d M Y H:i:s'],
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'datetime_end',
                            'format' => ['date', 'php:d M Y H:i:s'],
                        ],*/
						[
							'class' => '\kartik\grid\DataColumn',
							'label' => 'Время торгов',
                            'hAlign' => GridView::ALIGN_RIGHT,
							'headerOptions' => ['style' => 'text-align: center;'],
							'format' => 'raw',
							'value' => function($model) {
								$od = $model->datetime_start;
								$od1 = $model->datetime_end;
								$_monthsList = array("Jan" => "янв.", "Feb" => "фев.", 
									"Mar" => "мар.", "Apr" => "апр.", "May" => "мая", "Jun" => "июн.", 
									"Jul" => "июл.", "Aug" => "авг.", "Sep" => "сен.",
									"Oct" => "окт.", "Nov" => "ноя.", "Dec" => "дек.");
								$currentDate = date("d M Y H:i", strtotime($od));
								$currentDate1 = date("d M Y H:i", strtotime($od1));

								$_mD = date("M", strtotime($od));
								$_mD1 = date("M", strtotime($od1));
								$date = date("d M Y", strtotime($od));
								$date1 = date("d M Y", strtotime($od1));
								$time = date("H:i", strtotime($od));
								$time1 = date("H:i", strtotime($od1));

								if($date == $date1) {
									$date = str_replace($_mD, " ".$_monthsList[$_mD]." ", $date);
									$currentDate = $date.'<br/><span class="time-s">'.$time.'-'.$time1.'</span>';

									return $currentDate;		
								} else {
									$date = str_replace($_mD, " ".$_monthsList[$_mD]." ", $date);
									$date1 = str_replace($_mD1, " ".$_monthsList[$_mD1]." ", $date1);
									//$currentDate1 = str_replace($_mD1, " ".$_monthsList[$_mD1]." ", $currentDate1);

									return $date.'<br/><span class="time-s">'.$time.'-'.$time1.'</span><br/>'.$date1;
								}
							}
						],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'company_id',
                            'value' => 'company.name',
                            'visible' => Yii::$app->user->identity->isSuperAdmin(),
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'created_at',
                            'hAlign' => GridView::ALIGN_RIGHT,
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'format' => 'raw',
                            'content' => function($model){
						        $date = Yii::$app->formatter->asDate($model->created_at, 'php:d M Y');
                                $time = Yii::$app->formatter->asDate($model->created_at, 'php:H:i:s');

						        return $date.'<br/><span class="time-s">'.$time.'</span>';
                            }
                        ],
                    ],
                    'panelBeforeTemplate' => '',
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'headingOptions' => ['style' => 'display: none;'],
                        'after'=>'',
                    ]
                ])?>
            </div>
            <?php Pjax::end() ?>
        </div>
    </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>


<?php

$script = <<< JS
$('[data-file-input]').click(function(e){
    e.preventDefault();

    var dataFileInput = $(this).data('file-input');
    var id = $(this).data('id');

    $('#portfolio-file-form-'+id+' input[name="Portfolio['+dataFileInput+']"]').trigger('click');
});

$('[data-attribute-input]').click(function(e){
    e.preventDefault();

    var attribute = $(this).data('attribute-input');
    var id = $(this).data('id');

    $.get('/portfolio/update-attribute-boolean?id='+id+'&attribute='+attribute, function(response){
        $.pjax.reload('#crud-datatable-pjax');
    });
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>