<?php
use yii\widgets\ActiveForm;
use app\widgets\NumericInput;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\PortfolioRate */
/* @var $portfolio app\models\Portfolio */
/* @var $form yii\widgets\ActiveForm */
?>
<script type="text/javascript"> 

$(function(){ 
	$("#w1").keyup(function() { 
		var setRate = $(this).val();
		setRate = setRate.replace(/\s+/g,'');
		var od = $('.sum-od-h').val();
		var oz = $('.sum-od-g').val();
		var resOd = parseInt(setRate)/parseFloat(od)*100;
		var resOz = parseInt(setRate)/parseFloat(oz)*100;
		resOd = resOd.toFixed(2);
		resOz = resOz.toFixed(2);
		$('.perc-od span').text('Процент от ОД: '+resOd+' %');
		$('.perc-oz span').text('Процент от ОЗ: '+resOz+' %');
	});
	$("#auto-perc").keyup(function() { 
		var ap = $(this).val();
		$("#w1").val(ap);
	});
	$("#auto-fix").keyup(function() { 
		var ap = $(this).val();
		$("#portfoliorate-count_fix").val(ap);
	});
	$('.setrate').click(function() {
		$('.modal-footer .btn-primary').trigger("click");
	});
	$('.auto-set-rate').click(function() {
		var asp = $('#step-percent-auto').val();
		var asf = $('#step-fix-auto').val();
		var lsp = $('#limit-percent-auto').val();
		var lsf = $('#limit-fix-auto').val();
		$('#portfoliorate-amounta').val(asp);
		$('#portfoliorate-amount_fixa').val(asf);
		$('#portfoliorate-limit_percent').val(lsp);
		$('#portfoliorate-limit_fix').val(lsf);
		$("#w1").val(asp);
		$("#portfoliorate-count_fix").val(asf);
		$('.setrate').trigger("click");
	});
	if($('#limit-percent-auto').val().length > 0) {
		var lA = $('#step-percent-auto').val();	
		$('#auto-perc').val(lA);
	}
	if($('#limit-fix-auto').val().length > 0) {
		var lF = $('#step-fix-auto').val();	
		$('#auto-fix').val(lF);
	}
	$('.auto-rate-set-table thead').each(function() {
		if($(this).children().hasClass('soft-legal-head')) {
				
		}
	});
	
});

</script>
<div class="portfolio-form portfolio-form-set">
	<?php
		if(!empty($auctPerc)) {
			foreach($auctPerc as $auctPercItem => $auctPercValue) {
				if($auctPercValue == 1) {
					$keyPerc = $auctPercItem;
					$keyPerc = substr($auctPercItem, 3);
					if($keyPerc !== 'legal_percent') {
						$keyPerc = $keyPerc.'_percent';
					}
				}
			}
			if($portfolio->is_legal_fix == 1) {
				$fixPerc = $portfolio->legal_fix;
			}
	    }
	?>
	<?php if(!empty($keyPerc) || $portfolio->is_legal_fix == 1) {?>
	<div class="row">
		<div class="col-md-4">
			
			<table class="auto-rate soft_p" style=" width: 100%;">
				<thead>
					<tr>
						<td style="width: 33%;"></td><td style="width: 33%; text-align: center;">Шаг</td><td style="width: 33%; text-align: center;">Лимит</td>
					</tr>
				</thead>
				<?php 
					if($getAutoRate['amount']) {
						$step = $getAutoRate['amount'];
					} else {
						if($rHistoryItemL['amount']) {
							$step = $rHistoryItemL['amount'];
						} else {
							$step = $per['min_step_percent'];	
						}
						//if(isset($per['min_step_percent'])) { $step = $per['min_step_percent']; } else { $step = 0; }
					} 
					if($getAutoRate['amount_fix']) {
						$stepFix = $getAutoRate['amount_fix'];
					} else {
						if($rHistoryItemL['count_fix']) {
							$stepFix = $rHistoryItemL['count_fix'];
						} else {
							$stepFix = $per['min_step_fix'];
						}
						//if(isset($per['min_step_fix'])) { $stepFix = $per['min_step_fix']; } else { $stepFix = 0; }	
					}
					if($getAutoRate['limit_percent']) {
						$limitPercent = $getAutoRate['limit_percent'];
					} else {
						$limitPercent = '';
					}
					if($getAutoRate['limit_fix']) {
						$limitFix = $getAutoRate['limit_fix'];
					} else {
						$limitFix = '';
					}
					
				?>
	
				<tbody>
					<tr>
						<td>%</td><td><input id="step-percent-auto" type="text" name="auto-soft" value="<?php echo $step; ?>"></td><td><input id="limit-percent-auto" type="text" name="auto-soft" value="<?php echo $limitPercent; ?>"></td>
					</tr>
					<?php if(isset($per['min_step_fix']) && ($portfolio->is_legal_fix == 1)) { ?>
					<tr>
						<td>FIX</td><td><input id="step-fix-auto" type="text" name="auto-soft" value="<?php echo $stepFix; ?>"></td><td><input id="limit-fix-auto" type="text" name="auto-soft" value="<?php echo $limitFix; ?>"></td>
					</tr>
					<?php } ?>
					<tr>
						<td colspan="3"><span class='btn btn-default btn-block auto-set-rate'>Автоставка</span></td>
					</tr>	
				</tbody>
			</table>
			
		</div>
		<div class="col-md-4">
			<table class="auto-rate soft_p" style=" width: 100%;">
				<?php 
					if($rHistoryItemL['amount']) {
						$rStep = $rHistoryItemL['amount'];
					} else {
						$rStep = $per['min_step_percent'];	
					}
					$labelStep = '% -';
					if(isset($per['min_step_fix']) && ($portfolio->is_legal_fix == 1)) { 
						$labelStepFix = 'FIX - ';
						if($rHistoryItemL['count_fix']) {
							$rStepFix = $rHistoryItemL['count_fix'];
						} else {
							$rStepFix = $per['min_step_fix'];
						}
					}
						
				?>
				<tbody>
					<tr>
						<td>Шаг ставки (min):</td>
					</tr>
					<tr>
						<td><?php echo $labelStep.$rStep.' %'; ?></td>
					</tr>
					<?php if(isset($per['min_step_fix']) && ($portfolio->is_legal_fix == 1)) { ?>
					<tr>
						<td><?php echo $labelStepFix.''.$rStepFix.' ₽'; ?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<div class="col-md-4">
			<table class="auto-rate-set soft_p" style=" width: 100%;">
				<tbody>
					<tr><td colspan="2">Новая ставка</td></tr>
					
					
					<?php if(!empty($keyPerc) || $portfolio->is_legal_fix == 1) {?>
					<tr><td style=" width: 30%;">%</td>
						<td>
						<?php
							if($rHistoryItemL['amount']) {
								$rStep = $rHistoryItemL['amount'];
							} else {
								$rStep = $per['min_step_percent'];	
							}
						?>
						<input id="auto-perc" type="text" name="auto-perc" value="<?php echo $rStep; ?>">
						</td>
					</tr>
					<?php if($portfolio->is_legal_fix == 1) { ?>
						<?php
							if($rHistoryItemL['count_fix']) {
								$rStepFix = $rHistoryItemL['count_fix'];
							} else {
								$rStepFix = $per['min_step_fix'];	
							}
						?>
					<tr><td>FIX</td>
						<td>
						<input id="auto-fix"  type="text" name="auto-fix" value="<?php echo $rStepFix; ?>">	
						</td>
					</tr>
					<?php } ?>
				<?php } ?>
					<tr><td colspan="2"><span class='btn btn-default btn-block setrate'>Сделать ставку</span></td></tr>
    			
				</tbody>
			</table>
		</div>
	</div>
	<br/>
	<?php } ?>
	
    <?php $form = ActiveForm::begin(); ?>
	
	<?php if(empty($keyPerc) && $portfolio->is_legal_fix !== 1) {?>
		<div class="row">
			<div class="col-md-12">
				<?php if(isset($per['min_step'])) { $step = $per['min_step']; } else { $step = 1; }?>
				<?= $form->field($model, 'amount')->widget(NumericInput::class, [
					'pluginOptions' => [
						'data-a-dec' => ',',
						'data-a-sep' => ' ',
						'value' => $portfolio->currentRateAmount+$per['min_step'],
					],
				]) ?>
			</div>
		</div>
	<?php } else { ?>
		<div class="hidden-form-rate" style="display: none;">
			<p>Подсказка (Вычитается из текущего вознаграждения)</p>
			<div class="row">
				<div class="col-md-12">
					<?= $form->field($model, 'amount')->widget(NumericInput::class, [
						'pluginOptions' => [
							'data-a-dec' => ',',
							'data-a-sep' => ' ',
							'value' => $step,
						],
					]) ?>
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					
					<?= $form->field($model, 'amountA')->textInput();
					 ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<?= $form->field($model, 'amount_fixA')->textInput();
					 ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<?= $form->field($model, 'limit_percent')->textInput();
					 ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<?= $form->field($model, 'limit_fix')->textInput();
					 ?>
				</div>
			</div>
			<?php if($portfolio->is_legal_fix == 1) { ?>
					<div class="row">
						<div class="col-md-12">
							
							<?= $form->field($model, 'count_fix')->textInput(['value' => $stepFix])

							 ?>
						</div>
					</div>
			<?php } ?>
		<?php } ?>
	</div>
	<?php if(empty($keyPerc) && $portfolio->is_legal_fix !== 1) {?>
		<div class="row">
			<div class="col-md-12 perc-od">
				<input class="sum-od-h" hidden="hidden" value="<?php echo $per['sum_od']; ?>">
				<?php 
					$st = $portfolio->currentRateAmount+$per['min_step'];
					$od = $per['sum_od']; ?>
                    <?php if((isset($st))&&(isset($od))): ?>
                        <?php if($od > 0): ?>
                            <?php $res = $st/$od*100; ?>
                            <span><?= 'Процент от ОД: '.round($res, 2).' %'; ?></span>
                        <?php endif; ?>

                    <?php endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 perc-oz">
				<input class="sum-od-g" hidden="hidden" value="<?php echo $per['general_sum_credit']; ?>">
				<?php 
					$st = $portfolio->currentRateAmount+$per['min_step'];
					$od = $per['general_sum_credit'];
					if((isset($st))&&(isset($od))) {
						$res = $st/$od*100; ?>
						<span><?php echo 'Процент от ОЗ: '.round($res, 2).' %'; ?></span>
					<?php }
				?>
			</div>
		</div>
	<?php } ?>
	<div class="row">
		<div class="col-md-12">
			<?php 
				if(isset($per['min_step_percent'])) { 
					//$step = $per['min_step_percent']; 
					//$labelStep = 'Минимальный шаг процент: ';
				} else {
					$stepP = $per['min_step']; 
					$labelStep = 'Шаг ставки: ';
					echo $labelStep.$stepP;
					echo '<br>';
				}
				/*if(isset($per['min_step_fix']) && ($portfolio->is_legal_fix == 1)) { 
					$stepFix = $per['min_step_fix']; 
					$labelStepFix = 'Минимальный шаг фикс: ';
				}*/
			?>
			<?php 
				
				/*if(isset($per['min_step_fix']) && ($portfolio->is_legal_fix == 1)) { 
					echo $labelStepFix.''.$stepFix;	
				}*/
			?>
		</div>
	</div>
    <?php ActiveForm::end(); ?>
	<?php if(!empty($keyPerc) || $portfolio->is_legal_fix == 1) {?>
	<?php 
	$cntSoft = 0;
	$cntLegal = 0;
	?>
	<table class="auto-rate-set soft_p auto-rate-set-table" style=" width: 100%;">
		<thead>
			<tr class="soft-legal-head">
				<td rowspan="2" style="width: 30px;">#</td>
				<td style="width: 25%;" rowspan="2">Время</td>
				
				<?php if($portfolio->soft_30_days) { 
					$cntSoft = $cntSoft+1; ?>

				<?php }?>
				<?php if($portfolio->soft_90_days) { 
					$cntSoft = $cntSoft+1;?>
				
				<?php }?>
				<?php if($portfolio->soft_180_days) { 
				$cntSoft = $cntSoft+1; ?>
				
				<?php }?>
				<?php if($portfolio->soft_365_days) { 
				$cntSoft = $cntSoft+1; ?>
				
				<?php }?>
				<?php if($portfolio->soft_730_days) { 
				$cntSoft = $cntSoft+1; ?>
				
				<?php }?>
				<?php if($cntSoft > 0) { ?>
					<td colspan="<?php echo $cntSoft; ?>">Просрочка более, дней</td>
				<?php }?>
				<?php if($portfolio->legal_percent) { 
				$cntLegal = $cntLegal+1;?>
				
				<?php }?>
				<?php if($portfolio->legal_fix) { 
				$cntLegal = $cntLegal+1;?>
				
				<?php }?>
				<?php if($cntLegal > 0) { ?>
					<td colspan="<?php echo $cntLegal; ?>">legal collection</td>
				<?php }?>
				<td></td>
			</tr>
			<tr>
				
				<?php if($portfolio->soft_30_days) { ?>
				<td><?php echo $portfolio->soft_30_days; ?></td>
				<?php }?>
				<?php if($portfolio->soft_90_days) { ?>
				<td><?php echo $portfolio->soft_90_days; ?></td>
				<?php }?>
				<?php if($portfolio->soft_180_days) { ?>
				<td><?php echo $portfolio->soft_180_days; ?></td>
				<?php }?>
				<?php if($portfolio->soft_365_days) { ?>
				<td><?php echo $portfolio->soft_365_days; ?></td>
				<?php }?>
				<?php if($portfolio->soft_730_days) { ?>
				<td><?php echo $portfolio->soft_730_days; ?></td>
				<?php }?>
				<?php if($portfolio->legal_percent) { ?>
				<td>%</td>
				<?php }?>
				<?php if($portfolio->legal_fix) { ?>
				<td>FIX</td>
				<?php }?>
				<td style="width: 25%;">Ставка</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td></td>
				<td></td>
				<?php if($portfolio->soft_30_days) { ?>
				<td><?php if(isset($rHistoryItemL['count_soft_30'])) {
						echo $rHistoryItemL['count_soft_30'] - $rHistoryItemL['amount']." %"; 
					} else {
						echo $portfolio->soft_30_days_percent - $rHistoryItemL['amount']." %";
					}?>
				</td>
				<?php } ?>
				<?php if($portfolio->soft_90_days) { ?>
				<td>
				<?php if(isset($rHistoryItemL['count_soft_90'])) {
						echo $rHistoryItemL['count_soft_90'] - $rHistoryItemL['amount']." %";
					} else {
						echo $portfolio->soft_90_days_percent - $rHistoryItemL['amount']." %";
					}?>
				</td>
				
				<?php }?>
				<?php if($portfolio->soft_180_days) { ?>
				<td>
					<?php if(isset($rHistoryItemL['count_soft_180'])) {
						echo $rHistoryItemL['count_soft_180'] - $rHistoryItemL['amount']." %"; 
					} else {
						echo $portfolio->soft_180_days_percent - $rHistoryItemL['amount']." %";
					}?>
				</td>
				
				<?php }?>
				<?php if($portfolio->soft_365_days) { ?>
				<td>
				<?php if(isset($rHistoryItemL['count_soft_365'])) {
						echo $rHistoryItemL['count_soft_365'] - $rHistoryItemL['amount']." %";
					} else {
						echo $portfolio->soft_365_days_percent - $rHistoryItemL['amount']." %";
					}?>
				</td>
				
				<?php }?>
				<?php if($portfolio->soft_730_days) { ?>
				<td>
				<?php if(isset($rHistoryItemL['count_soft_730'])) {
						echo $rHistoryItemL['count_soft_730'] - $rHistoryItemL['amount']." %"; 
					} else {
						echo $portfolio->soft_730_days_percent - $rHistoryItemL['amount']." %";
					}?>
				</td>
				
				<?php }?>
				<?php if($portfolio->legal_percent) { ?>
				<td>
					<?php if(isset($rHistoryItemL['count_legal'])) {
						echo $rHistoryItemL['count_legal'] - $rHistoryItemL['amount']." %";
					} else {
						echo $portfolio->legal_percent - $rHistoryItemL['amount']." %";
					}?>
				</td>
				
				<?php }?>
				<?php if($portfolio->legal_fix) { ?>
				<td>
					<?php if($rHistoryItemL['count_legal_fix']) {
						echo $rHistoryItemL['count_legal_fix'] - $rHistoryItemL['count_fix']." ₽";
					} else {
						echo $portfolio->legal_fix - $stepFix." ₽";
					}?>
				</td>
				
				<?php }?>
				<td><span class='btn btn-default btn-block setrate'>Сделать ставку</span></td>
			</tr>
			</tbody>
			</table>
			<h4 style="text-align: center; width: 100%; z-index:1001; position:relative;">История</h4>
			
			<table class="auto-rate-set soft_p auto-rate-set-table auto-rate-set-table-h" style=" width: 100%; top:-30px; position:relative;">
				<thead style="opacity: 0;">
					<tr>
						<td style="width: 30px;">#</td>
						<td style="width: 25%;">Время</td>
						<?php if($portfolio->soft_30_days) { ?>
						<td><?php echo $portfolio->soft_30_days; ?></td>
						<?php }?>
						<?php if($portfolio->soft_90_days) { ?>
						<td><?php echo $portfolio->soft_90_days; ?></td>
						<?php }?>
						<?php if($portfolio->soft_180_days) { ?>
						<td><?php echo $portfolio->soft_180_days; ?></td>
						<?php }?>
						<?php if($portfolio->soft_365_days) { ?>
						<td><?php echo $portfolio->soft_365_days; ?></td>
						<?php }?>
						<?php if($portfolio->soft_730_days) { ?>
						<td><?php echo $portfolio->soft_730_days; ?></td>
						<?php }?>
						<?php if($portfolio->legal_percent) { ?>
						<td>%</td>
						<?php }?>
						<?php if($portfolio->legal_fix) { ?>
						<td>FIX</td>
						<?php }?>
						<td style="width: 25%;">Ставка</td>
					</tr>
				</thead>
				<tbody>
			<?php $userId = \Yii::$app->user->identity->id;
			$count = count($rHistory);
			$count = $count+1;
			$countMax = $count;								 
			foreach($rHistory as $rHistoryItem) { 
				if($rHistoryItem['count_soft_30'] || $rHistoryItem['count_soft_90'] || $rHistoryItem['count_soft_180'] || $rHistoryItem['count_soft_365'] || $rHistoryItem['count_soft_730'] || $rHistoryItem['count_legal'] || $rHistoryItem['count_legal_fix']) {
					if($count == $countMax) {?>
						<tr>
							<td><?php echo $count = $count-1; ?></td>
							<td><?php echo date("H:i:s", strtotime($rHistoryItem['created_at'])); ?></td>
							<?php if($portfolio->soft_30_days) { ?>
							<td><?php echo $rHistoryItem['count_soft_30']." %"; ?></td>

							<?php }?>
							<?php if($portfolio->soft_90_days) { ?>
							<td><?php echo $rHistoryItem['count_soft_90']." %"; ?></td>

							<?php }?>
							<?php if($portfolio->soft_180_days) { ?>
							<td><?php echo $rHistoryItem['count_soft_180']." %"; ?></td>

							<?php }?>
							<?php if($portfolio->soft_365_days) { ?>
							<td><?php echo $rHistoryItem['count_soft_365']." %"; ?></td>

							<?php }?>
							<?php if($portfolio->soft_730_days) { ?>
							<td><?php echo $rHistoryItem['count_soft_730']." %"; ?></td>

							<?php }?>
							<?php if($portfolio->legal_percent) { ?>
							<td><?php echo $rHistoryItem['count_legal']." %"; ?></td>

							<?php }?>
							<?php if($portfolio->legal_fix) { ?>
							<td><?php echo $rHistoryItem['count_legal_fix']." ₽"; ?></td>

							<?php }?>
							<?php if($rHistoryItem['user_id'] == $userId) { ?>
							<td style="color: green;">Актуальная, Моя</td>
							<?php } else { ?>
							<td>Актуальная</td>
							<?php } ?>
						</tr>
					<?php } else {?>
						<tr>
							<td><?php echo $count = $count-1; ?></td>
							<td><?php echo date("H:i:s", strtotime($rHistoryItem['created_at'])); ?></td>
							<?php if($portfolio->soft_30_days) { ?>
							<td><?php echo $rHistoryItem['count_soft_30']." %"; ?></td>

							<?php }?>
							<?php if($portfolio->soft_90_days) { ?>
							<td><?php echo $rHistoryItem['count_soft_90']." %"; ?></td>

							<?php }?>
							<?php if($portfolio->soft_180_days) { ?>
							<td><?php echo $rHistoryItem['count_soft_180']." %"; ?></td>

							<?php }?>
							<?php if($portfolio->soft_365_days) { ?>
							<td><?php echo $rHistoryItem['count_soft_365']." %"; ?></td>

							<?php }?>
							<?php if($portfolio->soft_730_days) { ?>
							<td><?php echo $rHistoryItem['count_soft_730']." %"; ?></td>

							<?php }?>
							<?php if($portfolio->legal_percent) { ?>
							<td><?php echo $rHistoryItem['count_legal']." %"; ?></td>

							<?php }?>
							<?php if($portfolio->legal_fix) { ?>
							<td><?php echo $rHistoryItem['count_legal_fix']." ₽"; ?></td>

							<?php }?>
							<?php if($rHistoryItem['user_id'] == $userId) { ?>
							<td style="color: green;">Моя</td>
							<?php } else { ?>
							<td></td>
							<?php } ?>
						</tr>
				<?php } ?>	
			<?php } ?>
			<?php } ?>
		</tbody>
	
	
	</table>
	<div class="text-rate">
		<br>
		<p>КАК ДЕЛАТЬ СТАВКИ:</p>
<p>При нажатии кнопки АВТОСТАВКА аукцион автоматически сделает за вас ставку, если ваша ставка не актуальна и не будет совершать ставок пока ваша ставка не будет перебита другим участником аукциона.
АВТОСТАВКА позволяет выставить максимально допустимые для вас значения ставок и шаг,
с которым они будут делаться, если ваша ставка перебита. По умолчанию аукцион предлагает вам минимальное значение ставки для этого лота.</p>

<p>ЛИМИТ это максимально допустимая для вас цена покупки портфеля. </p>
<p>По достижении лимита, система перестанет делать ставки и отправит Вам СМС о том, что достигнут лимит автоставки. 
Шаг - минимально допустимое изменение цены</p>
	</div>
		<?php }?>
</div>

