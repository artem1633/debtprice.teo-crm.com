<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DebtorStatus */
?>
<div class="debtor-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
        ],
    ]) ?>

</div>
