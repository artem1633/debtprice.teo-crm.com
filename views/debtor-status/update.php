<?php

/* @var $this yii\web\View */
/* @var $model app\models\DebtorStatus */
?>
<div class="debtor-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
