<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Кредитные продукты';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="product-type-index">
    <div id="ajaxCrudDatatable">
        <?php
        try {
            echo GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsiveWrap' => false,
                'pjax' => true,
                'columns' => require(__DIR__ . '/_columns.php'),
                'panelBeforeTemplate' => Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                        [
                            'role' => 'modal-remote',
                            'title' => 'Добавить кредит',
                            'class' => 'btn btn-success'
                        ]) . '&nbsp;' .
                    Html::a('<i class="fa fa-repeat"></i>', [''],
                        ['data-pjax' => 1, 'class' => 'btn btn-white', 'title' => 'Обновить']),
                //            'toolbar'=> [
                //                ['content'=>
                //                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
                //                    ['role'=>'modal-remote','title'=> 'Create new Product Types','class'=>'btn btn-default']).
                //                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                //                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                //                    '{toggleData}'.
                //                    '{export}'
                //                ],
                //            ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i> Список кредитных прдуктов',
//                    'before' => '<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                    'after' => BulkButtonWidget::widget([
                            'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete All',
                                ["bulk-delete"],
                                [
                                    "class" => "btn btn-danger btn-xs",
                                    'role' => 'modal-remote-bulk',
                                    'data-confirm' => false,
                                    'data-method' => false,// for overide yii data api
                                    'data-request-method' => 'post',
                                    'data-confirm-title' => 'Are you sure?',
                                    'data-confirm-message' => 'Are you sure want to delete this item'
                                ]),
                        ]) .
                        '<div class="clearfix"></div>',
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'error');
            echo $e->getMessage();
        } ?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
