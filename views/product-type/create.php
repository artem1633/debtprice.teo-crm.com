<?php

/* @var $this yii\web\View */
/* @var $model app\models\ProductType */

?>
<div class="product-type-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
