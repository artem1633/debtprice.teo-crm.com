<?php

/* @var $this yii\web\View */
/* @var $model app\models\ProductType */
?>
<div class="product-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
